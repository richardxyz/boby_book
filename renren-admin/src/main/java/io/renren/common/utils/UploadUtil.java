//package io.renren.common.utils;
//
//
//import org.csource.common.MyException;
//import org.csource.fastdfs.ClientGlobal;
//import org.csource.fastdfs.StorageClient;
//import org.csource.fastdfs.TrackerClient;
//import org.csource.fastdfs.TrackerServer;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.IOException;
//
//public class UploadUtil {
//    public static String uploadImage(MultipartFile multipartFile) {
//        String imgUrl = "http://192.168.198.129";
//        String tracker = UploadUtil.class.getResource("/tracker.conf").getPath();//获取配置文件目录
//        try {
//
//            ClientGlobal.init(tracker);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (MyException e) {
//            e.printStackTrace();
//        }
//        TrackerClient trackerClient = new TrackerClient();
//
//        //获得一个trackerServer实例
//        TrackerServer trackerServer = null;
//        try {
//            trackerServer = trackerClient.getConnection();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        //通过tracker获得一个storage连接客户端
//        StorageClient storageClient = new StorageClient(trackerServer, null);
//
//        try {
//            byte[] bytes = multipartFile.getBytes();
//            //获取全名
//            String originalFilename = multipartFile.getOriginalFilename();
//            System.out.println(originalFilename);
//            // 获取最后一个点
//            int i = originalFilename.lastIndexOf(".");
//            //截取点后面的后缀名
//            String nextName = originalFilename.substring(i + 1);
//
//            String[] uploadInfos = storageClient.upload_file(bytes, "png", null);
//            for (String uploadInfo : uploadInfos) {
//                imgUrl += "/" + uploadInfo;
//            }
//            System.out.println(imgUrl);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return imgUrl;
//
//
//    }
//}
