package io.renren.common.utils;

import io.renren.common.constant.WeiXinData;
import net.sf.json.JSONObject;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.HashMap;
import java.util.Map;

public class WeChatUtils {

    /**
     * Unicode转中文方法
     *
     * @param unicode Unicode数据
     * @return 转换后的结果
     */
    public static String unicodeToCn(String unicode) {
        /** 以 \ u 分割，因为java注释也能识别unicode，因此中间加了一个空格*/
        String[] strs = unicode.split("\\\\u");
        String returnStr = "";
        // 由于unicode字符串以 \ u 开头，因此分割出的第一个字符是""。
        for (int i = 1; i < strs.length; i++) {
            returnStr += (char) Integer.valueOf(strs[i], 16).intValue();
        }
        return returnStr;
    }

    /**
     * 中文转Unicode 中文数据
     *
     * @param cn
     * @return 转换后的结果
     */
    public static String cnToUnicode(String cn) {
        char[] chars = cn.toCharArray();
        String returnStr = "";
        for (int i = 0; i < chars.length; i++) {
            returnStr += "\\u" + Integer.toString(chars[i], 16);
        }
        return returnStr;
    }

    /**
     * 装换字符集编码
     *
     * @param content 需要转换的数据
     * @return 转换后的结果
     */
    public static String changeCharacter(String content) {
        String unicodeAa = cnToUnicode(content);//转Unicode
        //System.out.println("中文转Unicode结果： " + unicodeAa);
        String cnAa = unicodeToCn(unicodeAa);//转中文
        //System.out.println("Unicode转中文结果： " + cnAa);
        return cnAa;
    }

    /**
     * 通过临时登录凭证code 获取用户openid和session_key
     *
     * @param code 临时登录凭证
     * @return
     */
    @PostMapping("/selectOpenIdByCode")
    public static Map<String, Object> selectOpenIdByCode(String code) {
        Map<String, Object> map = new HashMap<>();
        String url = "https://api.weixin.qq.com/sns/jscode2session?"
                + "appid=" + WeiXinData.APP_ID
                + "&secret=" + WeiXinData.APP_SECRET
                + "&js_code=" + code
                + "&grant_type=" + WeiXinData.AUTHORIZATION_CODE;
        String result = AesCbcUtil.GETlogin(url);
        //将这个拼接出来的url打印出来看一下
//        System.err.println("URL:" + url);
        JSONObject json = JSONObject.fromObject(result);
        // System.out.println("这里是openid和session_key" + json);
        String openid = (String) json.get("openid");
        String session_key = (String) json.get("session_key");
//        System.err.println("openid：" + openid + "session_key：" + session_key);
        map.put("openid", openid);
        map.put("session_key", session_key);
        return map;
    }
}
