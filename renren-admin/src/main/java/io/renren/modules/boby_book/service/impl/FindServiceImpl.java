package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.FindDao;
import io.renren.modules.boby_book.dto.FindDTO;
import io.renren.modules.boby_book.entity.FindEntity;
import io.renren.modules.boby_book.service.FindService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 发现数据
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class FindServiceImpl extends CrudServiceImpl<FindDao, FindEntity, FindDTO> implements FindService {

    @Override
    public QueryWrapper<FindEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<FindEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private FindDao findDao;

    @Override
    public List<FindEntity> selectFindAll() {
        return findDao.selectFindAll();
    }
    @Override
    public List<FindEntity> selectFindByUserId(Long userId) {

        return findDao.selectFindByUserId(userId);
    }

}