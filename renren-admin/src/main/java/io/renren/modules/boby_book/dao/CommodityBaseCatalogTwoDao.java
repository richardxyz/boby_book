package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogTwoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 商品分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface CommodityBaseCatalogTwoDao extends BaseDao<CommodityBaseCatalogTwoEntity> {

    /**
     * 获取商品二级分类
     *
     * @param catalog1Id 一级分类id
     * @return
     */
    @Select("SELECT * FROM e_commodity_base_catalog2 WHERE catalog1_id = #{catalog1_id}")
    List<CommodityBaseCatalogTwoEntity> selectListByCatalog1Id(@Param("catalog1_id") Long catalog1Id);
}