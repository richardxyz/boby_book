package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 属性值表(用作于查询)
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xux_commodity_base_attr_value")
public class CommodityBaseAttrValueEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 属性值名称
     */
	private String valueName;
    /**
     * 分类id
     */
	private Long attrId;
    /**
     * 启用:1 停用:0
     */
	private String isEnabled;
    /**
     * 更新者
     */
	private Long updater;
    /**
     * 更新时间
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;
}