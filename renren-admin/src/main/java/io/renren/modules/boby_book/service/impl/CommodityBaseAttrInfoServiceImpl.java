package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommodityBaseAttrInfoDao;
import io.renren.modules.boby_book.dto.CommodityBaseAttrInfoDTO;
import io.renren.modules.boby_book.entity.CommodityBaseAttrInfoEntity;
import io.renren.modules.boby_book.service.CommodityBaseAttrInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 属性表(用作于查询)
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Service
@Transactional
public class CommodityBaseAttrInfoServiceImpl extends CrudServiceImpl<CommodityBaseAttrInfoDao, CommodityBaseAttrInfoEntity, CommodityBaseAttrInfoDTO> implements CommodityBaseAttrInfoService {

    @Override
    public QueryWrapper<CommodityBaseAttrInfoEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<CommodityBaseAttrInfoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}