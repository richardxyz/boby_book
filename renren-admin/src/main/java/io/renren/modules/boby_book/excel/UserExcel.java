package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 用户基本信息表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class UserExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "微信(用户唯一标识 OpenID)")
    private String openId;
    @Excel(name = "昵称")
    private String username;
    @Excel(name = "密码")
    private String password;
    @Excel(name = "手机号")
    private String phone;
    @Excel(name = "性别")
    private String gender;
    @Excel(name = "头像")
    private String headPortrait;
    @Excel(name = "地区")
    private String regional;
    @Excel(name = "详细地址")
    private String detailedAddress;
    @Excel(name = "语录")
    private String quotations;
    @Excel(name = "本周签到状态")
    private String signIn;
    @Excel(name = "签到总天数")
    private Integer signInNum;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}