package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.FindFabulousDTO;
import io.renren.modules.boby_book.entity.FindFabulousEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * 发现点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-20
 */
public interface FindFabulousService extends CrudService<FindFabulousEntity, FindFabulousDTO> {

    /**
     * 根据发现id判断点赞状态
     *
     * @param userId 用户id
     * @param findId 发现id
     * @return
     */
    Integer judgeFindFabulousStatus(Long userId, Long findId);

    /**
     * 取消发现点赞数
     *
     * @param userId 用户id
     * @param findId 发现id
     */
    void cancelFindFabulous(Long userId, Long findId);

    /**
     * 添加发现点赞数
     *
     * @param userId  用户id
     * @param findId  发现id
     * @param request
     */
    void addFindFabulous(Long userId, Long findId, HttpServletRequest request);
}