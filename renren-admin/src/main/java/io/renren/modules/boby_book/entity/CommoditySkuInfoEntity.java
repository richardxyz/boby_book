package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 商品详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xux_commodity_sku_info")
public class CommoditySkuInfoEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 商品id
     */
	private Long commodityId;
    /**
     * sku名称
     */
	private String skuName;
	/**
     * 商品详情
     */
	private String skuDetails;
    /**
     * 积分
     */
	private Integer integral;
    /**
     * 市场参考价
     */
	private Double price;
    /**
     * 品牌id(冗余)
     */
	private Long brandId;
    /**
     * 分类id（冗余)
     */
	private Long catalogId;
    /**
     * 默认显示图片(冗余)
     */
	private String skuDefaultImg;
	/**
     * 已兑换个数
     */
	private Integer exchangeNum;
	/**
     * 库存个数
     */
	private Integer stockNum;
    /**
     * 更新者
     */
	private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;

	/**
	 * 商品图片
	 */
	@TableField(exist = false)
	List<CommoditySkuImageEntity> skuImageList;

	/**
	 * sku销售属性值
	 */
	@TableField(exist = false)
	List<CommoditySkuSaleAttrValueEntity> skuSaleAttrValueList;

	/**
	 * sku平台属性值关联表
	 */
	@TableField(exist = false)
	List<CommoditySkuAttrValueEntity> skuAttrValueList;
}