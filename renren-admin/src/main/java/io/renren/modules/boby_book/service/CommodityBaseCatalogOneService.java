package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommodityBaseCatalogOneDTO;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogOneEntity;

/**
 * 商品分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommodityBaseCatalogOneService extends CrudService<CommodityBaseCatalogOneEntity, CommodityBaseCatalogOneDTO> {

}