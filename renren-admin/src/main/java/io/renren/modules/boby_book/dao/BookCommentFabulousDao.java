package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.BookCommentFabulousEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 图书评论点赞
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface BookCommentFabulousDao extends BaseDao<BookCommentFabulousEntity> {

    /**
     * 根据评论id判断点赞状态
     *
     * @param userId    用户id
     * @param commentId 评论id
     * @return
     */
    @Select("SELECT COUNT(*) FROM e_book_comment_fabulous  WHERE  from_uid = #{from_uid} AND comment_id = #{comment_id}")
    Integer judgeCommentFabulousStatus(@Param("from_uid") Long userId, @Param("comment_id") Long commentId);

    /**
     * 根据userId和commentId查询点赞数据
     * @param userId 用户id
     * @param commentId 评论id
     * @return
     */
    @Select("SELECT * FROM e_book_comment_fabulous  WHERE  from_uid = #{from_uid} AND comment_id = #{comment_id}")
    BookCommentFabulousEntity selectFabulousByUserIdAndCommentId(@Param("from_uid")Long userId, @Param("comment_id")Long commentId);
}