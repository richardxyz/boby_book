package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 商品信息表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_commodity_spu_info")
public class CommoditySpuInfoEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 商品名称
     */
    private String commodityName;
    /**
     * 商品描述
     */
    private String commodityDescribe;
    /**
     * 品牌id
     */
    private Long brandId;
    /**
     * 商品分类id
     */
    private Long catalogId;
    /**
     * 默认积分
     */
    private Integer integral;
    /**
     * 默认价格
     */
    private Double price;
    /**
     * 已兑换总个数
     */
    private Integer exchangeNum;
    /**
     * 评论数
     */
    private Integer commentNum;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;

    /**
     * 销售属性
     */
    @TableField(exist = false)
    private List<CommoditySalesAttrEntity> spuSaleAttrList;
    /**
     * 商品图片
     */
    @TableField(exist = false)
    private List<CommoditySpuImageEntity> spuImageList;
}