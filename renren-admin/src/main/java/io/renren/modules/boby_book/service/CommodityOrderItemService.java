package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommodityOrderItemDTO;
import io.renren.modules.boby_book.entity.CommodityOrderItemEntity;

/**
 * 订单详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface CommodityOrderItemService extends CrudService<CommodityOrderItemEntity, CommodityOrderItemDTO> {

}