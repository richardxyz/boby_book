package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.UserFansDao;
import io.renren.modules.boby_book.dto.UserFansDTO;
import io.renren.modules.boby_book.entity.UserFansEntity;
import io.renren.modules.boby_book.service.UserFansService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 用户粉丝
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class UserFansServiceImpl extends CrudServiceImpl<UserFansDao, UserFansEntity, UserFansDTO> implements UserFansService {

    @Override
    public QueryWrapper<UserFansEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<UserFansEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private UserFansDao fansDao;

    @Override
    public List<UserFansEntity> selectFans(Long userId) {
        return fansDao.selectFans(userId);
    }
}