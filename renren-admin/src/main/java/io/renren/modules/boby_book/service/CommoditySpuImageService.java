package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommoditySpuImageDTO;
import io.renren.modules.boby_book.entity.CommoditySpuImageEntity;

/**
 * 商品图片
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommoditySpuImageService extends CrudService<CommoditySpuImageEntity, CommoditySpuImageDTO> {

}