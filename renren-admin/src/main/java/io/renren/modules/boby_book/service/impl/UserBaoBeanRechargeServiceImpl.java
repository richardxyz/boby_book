package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.UserBaoBeanRechargeDao;
import io.renren.modules.boby_book.dto.UserBaoBeanRechargeDTO;
import io.renren.modules.boby_book.entity.UserBaoBeanRechargeEntity;
import io.renren.modules.boby_book.service.UserBaoBeanRechargeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 宝豆充值
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class UserBaoBeanRechargeServiceImpl extends CrudServiceImpl<UserBaoBeanRechargeDao, UserBaoBeanRechargeEntity, UserBaoBeanRechargeDTO> implements UserBaoBeanRechargeService {

    @Override
    public QueryWrapper<UserBaoBeanRechargeEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<UserBaoBeanRechargeEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}