package io.renren.modules.boby_book.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Data
@ApiModel(value = "商品详情表")
public class CommoditySkuInfoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "商品id")
    private Long commodityId;

    @ApiModelProperty(value = "sku名称")
    private String skuName;

    @ApiModelProperty(value = "商品详情")
    private String skuDetails;

    @ApiModelProperty(value = "积分")
    private Integer integral;

    @ApiModelProperty(value = "市场参考价")
    private Double price;

    @ApiModelProperty(value = "品牌id(冗余)")
    private Long brandId;

    @ApiModelProperty(value = "分类id（冗余)")
    private Long catalogId;

    @ApiModelProperty(value = "默认显示图片(冗余)")
    private String skuDefaultImg;

    @ApiModelProperty(value = "已兑换个数")
    private Integer exchangeNum;

    @ApiModelProperty(value = "库存个数")
    private Integer stockNum;

    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;
}
