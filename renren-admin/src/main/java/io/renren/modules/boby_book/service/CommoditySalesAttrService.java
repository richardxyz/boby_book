package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommoditySalesAttrDTO;
import io.renren.modules.boby_book.entity.CommoditySalesAttrEntity;

import java.util.List;

/**
 * 销售属性
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommoditySalesAttrService extends CrudService<CommoditySalesAttrEntity, CommoditySalesAttrDTO> {

    /**
     * 销售属性列表
     *
     * @param commodityId 商品id
     * @param skuId       商品库存id
     * @return
     */
    List<CommoditySalesAttrEntity> spuSaleAttrListCheckBySku(Long commodityId, Long skuId);

}