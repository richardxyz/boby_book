package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommodityBaseCatalogOneDao;
import io.renren.modules.boby_book.dto.CommodityBaseCatalogOneDTO;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogOneEntity;
import io.renren.modules.boby_book.service.CommodityBaseCatalogOneService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 商品分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-11-11
 */
@Service
@Transactional
public class CommodityBaseCatalogOneServiceImpl extends CrudServiceImpl<CommodityBaseCatalogOneDao, CommodityBaseCatalogOneEntity, CommodityBaseCatalogOneDTO> implements CommodityBaseCatalogOneService {

    @Override
    public QueryWrapper<CommodityBaseCatalogOneEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<CommodityBaseCatalogOneEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}