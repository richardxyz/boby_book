package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommodityBaseAttrValueDTO;
import io.renren.modules.boby_book.entity.CommodityBaseAttrValueEntity;

/**
 * 属性值表(用作于查询)
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommodityBaseAttrValueService extends CrudService<CommodityBaseAttrValueEntity, CommodityBaseAttrValueDTO> {

}