package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 评论回复
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xux_commodity_comment_reply")
public class CommodityCommentReplyEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 目标评论id
     */
	private Long commentId;
    /**
     * 回复内容
     */
	private String content;
    /**
     * 回复时间
     */
	private Date replyTime;
	/**
	 * 回复用户id
	 */
	private Long fromUid;
	/**
	 * 目标用户id
	 */
	private Long toUid;
    /**
     * 更新者
     */
	private Long updater;
    /**
     * 更新时间
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;
}