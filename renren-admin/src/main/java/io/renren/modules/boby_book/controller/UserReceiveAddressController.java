package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.UserReceiveAddressDTO;
import io.renren.modules.boby_book.entity.UserReceiveAddressEntity;
import io.renren.modules.boby_book.excel.UserReceiveAddressExcel;
import io.renren.modules.boby_book.service.UserReceiveAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 用户信息详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/userreceiveaddress")
@Api(tags = "用户信息详情表")
public class UserReceiveAddressController {
    @Autowired
    private UserReceiveAddressService receiveAddressService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:userreceiveaddress:page")
    public Result<PageData<UserReceiveAddressDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<UserReceiveAddressDTO> page = receiveAddressService.page(params);

        return new Result<PageData<UserReceiveAddressDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:userreceiveaddress:info")
    public Result<UserReceiveAddressDTO> get(@PathVariable("id") Long id) {
        UserReceiveAddressDTO data = receiveAddressService.get(id);

        return new Result<UserReceiveAddressDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:userreceiveaddress:save")
    public Result save(@RequestBody UserReceiveAddressDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        receiveAddressService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:userreceiveaddress:update")
    public Result update(@RequestBody UserReceiveAddressDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        receiveAddressService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:userreceiveaddress:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        receiveAddressService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:userreceiveaddress:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<UserReceiveAddressDTO> list = receiveAddressService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, UserReceiveAddressExcel.class);
    }

    /**
     * 查询用户默认收货地址信息
     *
     * @param userid 用户id
     * @return
     */
    @PostMapping("/getReceiveAddress")
    public Result getReceiveAddress(Long userid) {
        System.out.println("userid：" + userid);
        UserReceiveAddressEntity userReceiveAddress2Entity = receiveAddressService.selectReceiveAddressById(userid);//查询默认地址
        return new Result().ok(userReceiveAddress2Entity);
    }

    /**
     * 查询用户所有收货地址信息
     *
     * @param userid 用户id
     * @return
     */
    @PostMapping("/getReceiveAddressList")
    public Result getReceiveAddressList(Long userid) {
        System.out.println("userid：" + userid);
        List<UserReceiveAddressEntity> entityList = receiveAddressService.selectReceiveAddressListById(userid);//查询所有收货地址
        return new Result().ok(entityList);
    }

    /**
     * 新增收货地址
     *
     * @param userid        用户id
     * @param name          收货人昵称
     * @param phone         收货人手机号
     * @param postCode      邮政编码
     * @param province      省份/直辖市
     * @param city          城市
     * @param region        区
     * @param detailAddress 详细地址(街道)
     * @param defaultStatus 是否为默认
     * @return
     */
    @PostMapping("/addNewAddress")
    public Result addNewAddress(Long userid, String name, String phone, String postCode, String province, String city, String region, String detailAddress, Boolean defaultStatus) {
        receiveAddressService.addNewAddress(userid, name, phone, postCode, province, city, region, detailAddress, defaultStatus);//新增收货地址
        return new Result().ok("新增成功");
    }


    /**
     * 根据id查询收货地址信息
     *
     * @param addressid 收货地址id
     * @return
     */
    @PostMapping("/selectReceiveAddress")
    public Result selectReceiveAddress(Long addressid) {
        System.out.println("addressid：" + addressid);
        UserReceiveAddressEntity userReceiveAddress2Entity = receiveAddressService.selectById(addressid);//查询收货地址
        return new Result().ok(userReceiveAddress2Entity);
    }


    /**
     * 修改收货地址
     *
     * @param addressid     收货地址id
     * @param userid        用户id
     * @param name          收货人昵称
     * @param phone         收货人手机号
     * @param postCode      邮政编码
     * @param province      省份/直辖市
     * @param city          城市
     * @param region        区
     * @param detailaddress 详细地址(街道)
     * @param defaultstatus 是否为默认
     * @return
     */
    @PostMapping("/updateAddress")
    public Result updateAddress(Long addressid, Long userid, String name, String phone, String postCode, String province, String city, String region, String detailaddress, Boolean defaultstatus) {
        receiveAddressService.updateAddress(addressid, userid, name, phone, postCode, province, city, region, detailaddress, defaultstatus);//修改收货地址

        return new Result().ok("修改成功");
    }

}