package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户关注
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_user_follow")
public class UserFollowEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private Long userId;
    /**
     * 关注时间
     */
    private Date followTime;
    /**
     * 关注id
     */
    private Long followUserId;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;


    /**
     * 用户信息
     */
    @TableField(exist = false)
    UserEntity user;

    /**
     * 用户详细信息
     */
    @TableField(exist = false)
    UserSpecificInformationEntity userSpecificInformation;
}