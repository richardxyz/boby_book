package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.BookClassificationCategoryDTO;
import io.renren.modules.boby_book.entity.BookClassificationCategoryEntity;

/**
 * 书籍分类中间表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
public interface BookClassificationCategoryService extends CrudService<BookClassificationCategoryEntity, BookClassificationCategoryDTO> {

}