package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogThreeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 商品分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface CommodityBaseCatalogThreeDao extends BaseDao<CommodityBaseCatalogThreeEntity> {

    /**
     * 获取商品三级分类
     *
     * @param catalog2Id 二级分类id
     * @return
     */
    @Select("SELECT * FROM e_commodity_base_catalog3 WHERE catalog2_id = #{catalog2_id}")
    List<CommodityBaseCatalogThreeEntity> selectListByCatalog2Id(@Param("catalog2_id") Long catalog2Id);
}