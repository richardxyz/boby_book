package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommodityBaseCatalogThreeDTO;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogOneEntity;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogThreeEntity;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogTwoEntity;

import java.util.List;

/**
 * 商品分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommodityBaseCatalogThreeService extends CrudService<CommodityBaseCatalogThreeEntity, CommodityBaseCatalogThreeDTO> {

    /**
     * 获取商品一级分类
     *
     * @return
     */
    List<CommodityBaseCatalogOneEntity> getCatalog1();

    /**
     * 获取商品二级分类
     *
     * @param catalog1Id
     * @return
     */
    List<CommodityBaseCatalogTwoEntity> getCatalog2(Long catalog1Id);

    /**
     * 获取商品三级分类
     *
     * @param catalog2Id
     * @return
     */
    List<CommodityBaseCatalogThreeEntity> getCatalog3(Long catalog2Id);
}