package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 发现点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_find_fabulous")
public class FindFabulousEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 评论id
     */
    private Long findId;
    /**
     * 评论用户IP
     */
    private String fromUip;
    /**
     * 用户id
     */
    private Long fromUid;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;
}