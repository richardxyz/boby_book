package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.common.utils.Result;
import io.renren.modules.boby_book.dto.UserDTO;
import io.renren.modules.boby_book.entity.UserEntity;
import org.springframework.web.multipart.MultipartFile;

/**
 * 用户基本信息表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserService extends CrudService<UserEntity, UserDTO> {
    /**
     * 用户手机号注册
     *
     * @param phone    手机号
     * @param password 密码
     */
    void Register(String phone, String password);

    /**
     * 根据手机号查询用户id
     *
     * @param phone 手机号
     * @return
     */
    Long selectIdByPhone(String phone);

    /**
     * 根据手机号查询用户
     *
     * @param phone 手机号
     * @return
     */
    UserEntity selectUserByPhone(String phone);

    /**
     * 修改昵称
     *
     * @param username 用户名
     * @param id       用户id
     */
    void changeUserName(String username, Long id);

    /**
     * 修改地区
     *
     * @param regional 地区
     * @param id       用户id
     */
    void changeRegional(String regional, Long id);

    /**
     * 修改详细地址
     *
     * @param detailedAddress 详细地址
     * @param id              用户id
     */
    void changedeTailedAddress(String detailedAddress, Long id);

    /**
     * 查询当前手机号是否注册
     *
     * @param phone 手机号
     * @return
     */
    Integer selectByPhone(String phone);

    /**
     * 修改头像
     *
     * @param filePath 头像信息
     * @param id       用户id
     */
    void changeHeadPortrait(String filePath, Long id);

    /**
     * 修改签到状态
     *
     * @param signInState 签到状态
     * @param id          用户ID
     */
    void updateSignInState(String signInState, Long id);

    /**
     * 修改签到天数
     *
     * @param num 天数
     * @param id  用户id
     */
    void updateSignInNum(Integer num, Long id);

    /**
     * 清空签到状态
     */
    void updateSignIn();

    /**
     * 微信登录
     *
     * @param openid 微信用户唯一标识
     * @return
     */
    Integer wxLogin(String openid);

    /**
     * 微信注册
     *
     * @param openid    微信用户唯一标识
     * @param avatarUrl 头像
     * @param nickName  昵称
     * @param province  地区
     */
    void wxEmpower(String openid, String avatarUrl, String nickName, String province);

    /**
     * 根据openid查询用户
     *
     * @param openid 微信用户唯一标识
     * @return
     */
    UserEntity selectUserByOpenid(String openid);

    /**
     * 修改性别
     *
     * @param gender 性别
     * @param userid 用户id
     */
    void changedeGender(String gender, Long userid);

    /**
     * 修改密码
     *
     * @param newPassword 新密码
     * @param userid      用户id
     */
    void changedePwd(String newPassword, Long userid);

    /**
     * 用户修改语录
     *
     * @param quotations 语录
     * @param userid     用户id
     */
    void changeQuotations(String quotations, Long userid);

    /**
     * 查询当前用户是否关注
     *
     * @param fuId 用户id
     * @param tuId 所关注用户id
     * @return
     */
    Integer selectFollowStyle(Long fuId, Long tuId);

    /**
     * 关注用户
     *
     * @param fuId 用户id
     * @param tuId 所关注用户id
     */
    void followUser(Long fuId, Long tuId);

    /**
     * 取消关注
     *
     * @param fuId 用户id
     * @param tuId 所关注用户id
     */
    void cancelFollowUser(Long fuId, Long tuId);

    /**
     * 绑定手机号
     *
     * @param phone  手机号
     * @param userid 用户id
     */
    void bindPhone(String phone, Long userid);

    /**
     * 判断手机号是否已绑定注册
     *
     * @param phone 手机号
     * @return
     */
    Integer judgePhineExist(String phone);

    Result updUserProfile(MultipartFile profile, Long userid);
}