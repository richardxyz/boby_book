package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 商品图片表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xux_commodity_sku_image")
public class CommoditySkuImageEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 商品skuid
     */
	private Long skuId;
    /**
     * 图片名称（冗余）
     */
	private String imgName;
    /**
     * 图片路径(冗余)
     */
	private String imgUrl;
    /**
     * 商品图片id
     */
	private Long productImgId;
	/**
     * 是否默认
     */
	private Integer isDefault;
    /**
     * 更新者
     */
	private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;
}