package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * sku销售属性值(用户选择的属性)
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommoditySkuSaleAttrValueExcel {
    @Excel(name = "id")
    private Long id;
    @Excel(name = "库存单元id")
    private Long skuId;
    @Excel(name = "销售属性id（冗余)")
    private Long saleAttrId;
    @Excel(name = "销售属性值id")
    private Long saleAttrValueId;
    @Excel(name = "销售属性名称(冗余)")
    private String saleAttrName;
    @Excel(name = "销售属性值名称(冗余)")
    private String saleAttrValueName;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}