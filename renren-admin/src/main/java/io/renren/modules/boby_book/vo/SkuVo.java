package io.renren.modules.boby_book.vo;

import io.renren.modules.boby_book.entity.CommoditySkuInfoEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SkuVo extends CommoditySkuInfoEntity {
    /**
     * 品牌名称
     */
    private String brandName;
    /**
     * 评论数
     */
    private Integer commentNum;

}
