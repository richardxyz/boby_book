package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommodityCommentReplyEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 评价回复
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface CommodityCommentReplyDao extends BaseDao<CommodityCommentReplyEntity> {

    /**
     * 根据id查询商品评价回复信息
     *
     * @param id 商品id
     * @return
     */
    @Select("SELECT * FROM e_commodity_comment_reply WHERE comment_id = #{comment_id}")
    List<CommodityCommentReplyEntity> selectCommentReplyById(@Param("comment_id") Long id);
}