package io.renren.modules.boby_book.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 用户信息详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@ApiModel(value = "用户信息详情表")
public class UserSpecificInformationDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "金币积分数量")
    private Integer integral;

    @ApiModelProperty(value = "宝豆数量")
    private Double baoBean;

    @ApiModelProperty(value = "下载图书数量")
    private Integer downloadNum;

    @ApiModelProperty(value = "购买图书数量")
    private Integer purchaseNum;

    @ApiModelProperty(value = "收藏图书数量")
    private Integer collectionNum;

    @ApiModelProperty(value = "订阅图书数量")
    private Integer subscribeNum;

    @ApiModelProperty(value = "视频播放数量")
    private Integer playVideoNum;

    @ApiModelProperty(value = "视频数量")
    private Integer videoNum;

    @ApiModelProperty(value = "粉丝(人)")
    private Integer fansNum;

    @ApiModelProperty(value = "关注(人)")
    private Integer followNum;

    @ApiModelProperty(value = "用户id（外键）")
    private Long userId;

    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;
}