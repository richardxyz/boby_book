package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.UserCollectionDao;
import io.renren.modules.boby_book.dto.UserCollectionDTO;
import io.renren.modules.boby_book.entity.UserCollectionEntity;
import io.renren.modules.boby_book.service.UserCollectionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 用户收藏
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class UserCollectionServiceImpl extends CrudServiceImpl<UserCollectionDao, UserCollectionEntity, UserCollectionDTO> implements UserCollectionService {

    @Override
    public QueryWrapper<UserCollectionEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<UserCollectionEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private UserCollectionDao eCollectionDao;


    @Override
    public List<UserCollectionEntity> selectCollectionBook(Long id) {
        return eCollectionDao.selectCollectionBook(id);
    }
}