package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户信息详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_user_specific_information")
public class UserSpecificInformationEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 金币积分数量
     */
    private Integer integral;
    /**
     * 宝豆数量
     */
    private Double baoBean;
    /**
     * 下载图书数量
     */
    private Integer downloadNum;
    /**
     * 购买图书数量
     */
    private Integer purchaseNum;
    /**
     * 收藏图书数量
     */
    private Integer collectionNum;
    /**
     * 订阅图书数量
     */
    private Integer subscribeNum;
    /**
     * 视频播放数量
     */
    private Integer playVideoNum;
    /**
     * 视频数量
     */
    private Integer videoNum;
    /**
     * 粉丝(人)
     */
    private Integer fansNum;
    /**
     * 关注(人)
     */
    private Integer followNum;
    /**
     * 用户id（外键）
     */
    private Long userId;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;
}