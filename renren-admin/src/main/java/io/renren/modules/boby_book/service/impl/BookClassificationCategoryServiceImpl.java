package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.BookClassificationCategoryDao;
import io.renren.modules.boby_book.dto.BookClassificationCategoryDTO;
import io.renren.modules.boby_book.entity.BookClassificationCategoryEntity;
import io.renren.modules.boby_book.service.BookClassificationCategoryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 书籍分类中间表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Service
public class BookClassificationCategoryServiceImpl extends CrudServiceImpl<BookClassificationCategoryDao, BookClassificationCategoryEntity, BookClassificationCategoryDTO> implements BookClassificationCategoryService {

    @Override
    public QueryWrapper<BookClassificationCategoryEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<BookClassificationCategoryEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}