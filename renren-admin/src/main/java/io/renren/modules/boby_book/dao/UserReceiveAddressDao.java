package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.UserReceiveAddressEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 用户信息详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserReceiveAddressDao extends BaseDao<UserReceiveAddressEntity> {
    /**
     * 查询用户默认收货地址信息
     *
     * @param userid
     * @return
     */
    @Select("SELECT * FROM e_user_receive_address WHERE user_id = #{user_id} AND default_status = 1")
    UserReceiveAddressEntity selectReceiveAddressById(@Param("user_id") Long userid);

    /**
     * 查询用户所有收货地址信息
     *
     * @param userid
     * @return
     */
    @Select("SELECT * FROM e_user_receive_address WHERE user_id = #{user_id}")
    List<UserReceiveAddressEntity> selectReceiveAddressListById(@Param("user_id") Long userid);

    /**
     * 修改默认收货地址
     *
     * @param addressid
     * @param userid
     */
    @Update("UPDATE e_user_receive_address SET default_status = 0 WHERE id IN (SELECT a.id FROM(SELECT id FROM e_user_receive_address WHERE user_id = #{user_id} AND id != #{id} ) AS a);")
    void upDefaultStatusListById(@Param("id") Long addressid, @Param("user_id") Long userid);
}