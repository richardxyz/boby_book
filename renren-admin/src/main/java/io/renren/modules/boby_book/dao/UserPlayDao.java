package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.UserPlayEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户最近播放
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserPlayDao extends BaseDao<UserPlayEntity> {
    /**
     * 查询今日播放
     *
     * @param id 用户id
     * @return
     */
    List<UserPlayEntity> selectTodayBook(@Param("user_id") Long id);

    /**
     * 查询昨日播放
     *
     * @param id 用户id
     * @return
     */
    List<UserPlayEntity> selectYesterdayBook(@Param("user_id") Long id);

    /**
     * 查询更早播放
     *
     * @param id 用户id
     * @return
     */
    List<UserPlayEntity> selecEarlierBook(@Param("user_id") Long id);

    /**
     * 查询当前图书是否已加入播放列表
     *
     * @param userId 用户id
     * @param bookId 图书id
     * @return
     */
    @Select("SELECT COUNT(*) FROM e_user_play  WHERE TO_DAYS(play_time) = TO_DAYS(NOW()) AND user_id = #{user_id} AND book_id = #{book_id}")
    Integer lastPlay(@Param("user_id") Long userId, @Param("book_id") Long bookId);
}