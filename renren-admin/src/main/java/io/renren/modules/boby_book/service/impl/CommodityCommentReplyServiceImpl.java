package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommodityCommentReplyDao;
import io.renren.modules.boby_book.dto.CommodityCommentReplyDTO;
import io.renren.modules.boby_book.entity.CommodityCommentReplyEntity;
import io.renren.modules.boby_book.service.CommodityCommentReplyService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 评价回复
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-11-11
 */
@Service
@Transactional
public class CommodityCommentReplyServiceImpl extends CrudServiceImpl<CommodityCommentReplyDao, CommodityCommentReplyEntity, CommodityCommentReplyDTO> implements CommodityCommentReplyService {

    @Override
    public QueryWrapper<CommodityCommentReplyEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<CommodityCommentReplyEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Autowired
    private CommodityCommentReplyDao commentReplyDao;

    @Override
    public void saveCommentReply(Long cid, Long fuid, Long tuid, String content, String ip) {
        CommodityCommentReplyEntity entity = new CommodityCommentReplyEntity();
        entity.setCommentId(cid);
        entity.setContent(content);
        entity.setReplyTime(new Date());
        entity.setFromUid(fuid);
        entity.setToUid(tuid);
        commentReplyDao.insert(entity);
    }

    @Override
    public List<CommodityCommentReplyEntity> selectCommentReplyById(Long id) {
        return commentReplyDao.selectCommentReplyById(id);
    }
}