package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.FindCommentReplyDTO;
import io.renren.modules.boby_book.excel.FindCommentReplyExcel;
import io.renren.modules.boby_book.service.FindCommentReplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 发现评论回复表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-19
 */
@RestController
@RequestMapping("boby_book/findcommentreply")
@Api(tags = "发现评论回复表")
public class FindCommentReplyController {
    @Autowired
    private FindCommentReplyService findCommentReplyService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:findcommentreply:page")
    public Result<PageData<FindCommentReplyDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<FindCommentReplyDTO> page = findCommentReplyService.page(params);

        return new Result<PageData<FindCommentReplyDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:findcommentreply:info")
    public Result<FindCommentReplyDTO> get(@PathVariable("id") Long id) {
        FindCommentReplyDTO data = findCommentReplyService.get(id);

        return new Result<FindCommentReplyDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:findcommentreply:save")
    public Result save(@RequestBody FindCommentReplyDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        findCommentReplyService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:findcommentreply:update")
    public Result update(@RequestBody FindCommentReplyDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        findCommentReplyService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:findcommentreply:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        findCommentReplyService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:findcommentreply:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<FindCommentReplyDTO> list = findCommentReplyService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, FindCommentReplyExcel.class);
    }

}