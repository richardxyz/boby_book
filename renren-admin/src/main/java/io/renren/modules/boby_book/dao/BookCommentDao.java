package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.BookCommentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 图书章节评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface BookCommentDao extends BaseDao<BookCommentEntity> {
    /**
     * 根据id查询图书评论信息
     *
     * @param bookId 图书id
     * @return
     */
    List<BookCommentEntity> selectCommentByBookId(@Param("book_id") Long bookId);

    /**
     * 根据评论id查询评论信息
     *
     * @param id 评论id
     * @return
     */
    BookCommentEntity selectCommentById(@Param("id") Long id);
}