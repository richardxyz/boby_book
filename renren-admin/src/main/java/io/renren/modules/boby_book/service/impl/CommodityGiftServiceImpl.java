package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommodityGiftDao;
import io.renren.modules.boby_book.dto.CommodityGiftDTO;
import io.renren.modules.boby_book.entity.CommodityGiftEntity;
import io.renren.modules.boby_book.service.CommodityGiftService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 兑换礼品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-10-30
 */
@Service
@Transactional
public class CommodityGiftServiceImpl extends CrudServiceImpl<CommodityGiftDao, CommodityGiftEntity, CommodityGiftDTO> implements CommodityGiftService {

    @Override
    public QueryWrapper<CommodityGiftEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<CommodityGiftEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private CommodityGiftDao giftDao;

    @Override
    public List<CommodityGiftEntity> selectGiftByUserId(Long userId, Long commodityId) {

        return giftDao.selectGiftByUserId(userId,commodityId);
    }

    @Override
    public void saveGift(Long skuid, Integer purchaseQuantity, Long userid) {
        CommodityGiftEntity giftEntity = new CommodityGiftEntity();
        giftEntity.setSkuId(skuid);
        giftEntity.setUserId(userid);
        giftEntity.setExchangeNum(purchaseQuantity);
        giftEntity.setExchangeTime(new Date());
        giftDao.insert(giftEntity);
    }
}