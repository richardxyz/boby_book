package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserBaoBeanRechargeDTO;
import io.renren.modules.boby_book.entity.UserBaoBeanRechargeEntity;

/**
 * 宝豆充值
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserBaoBeanRechargeService extends CrudService<UserBaoBeanRechargeEntity, UserBaoBeanRechargeDTO> {

}