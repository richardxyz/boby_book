package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.BookChapterEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 图书章节信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface BookChapterDao extends BaseDao<BookChapterEntity> {
    /**
     * 查询所有书籍章节信息（正序）
     *
     * @param bookId 图书id
     * @return
     */
    @Select("SELECT * FROM e_book_chapter WHERE book_id = #{book_id} ORDER BY id ASC")
    List<BookChapterEntity> selectBookChapterOrderByASC(@Param("book_id")Long bookId);

    /**
     * 查询所有书籍章节信息（倒序）
     *
     * @param bookId 图书id
     * @return
     */
    @Select("SELECT * FROM e_book_chapter WHERE book_id = #{book_id} ORDER BY id DESC")
    List<BookChapterEntity> selectBookChapterOrderByDESC(@Param("book_id")Long bookId);
}