package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 发现点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-20
 */
@Data
public class FindFabulousExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "评论id")
    private Long findId;
    @Excel(name = "评论用户IP")
    private String fromUip;
    @Excel(name = "用户id")
    private Long fromUid;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}