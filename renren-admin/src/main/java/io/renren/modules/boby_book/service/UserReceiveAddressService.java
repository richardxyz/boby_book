package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserReceiveAddressDTO;
import io.renren.modules.boby_book.entity.UserReceiveAddressEntity;

import java.util.List;

/**
 * 用户收货地址表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserReceiveAddressService extends CrudService<UserReceiveAddressEntity, UserReceiveAddressDTO> {
    /**
     * 查询用户默认收货地址信息
     *
     * @param userid 用户id
     * @return
     */
    UserReceiveAddressEntity selectReceiveAddressById(Long userid);

    /**
     * 查询用户所有收货地址信息
     *
     * @param userid 用户id
     * @return
     */
    List<UserReceiveAddressEntity> selectReceiveAddressListById(Long userid);

    /**
     * 新增收货地址
     *
     * @param userid        用户id
     * @param name          收货人昵称
     * @param phone         收货人手机号
     * @param postCode      邮政编码
     * @param province      省份/直辖市
     * @param city          城市
     * @param region        区
     * @param detailAddress 详细地址(街道)
     * @param defaultStatus 是否为默认
     * @return
     */
    void addNewAddress(Long userid, String name, String phone, String postCode, String province, String city, String region, String detailAddress, Boolean defaultStatus);

    /**
     * 修改收货地址
     *
     * @param addressid     收货地址id
     * @param userid        用户id
     * @param name          收货人昵称
     * @param phone         收货人手机号
     * @param postCode      邮政编码
     * @param province      省份/直辖市
     * @param city          城市
     * @param region        区
     * @param detailAddress 详细地址(街道)
     * @param defaultStatus 是否为默认
     * @return
     */
    void updateAddress(Long addressid, Long userid, String name, String phone, String postCode, String province, String city, String region, String detailAddress, Boolean defaultStatus);


}
