package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.UserPurchaseDTO;
import io.renren.modules.boby_book.entity.BookChapterEntity;
import io.renren.modules.boby_book.entity.BookEntity;
import io.renren.modules.boby_book.excel.UserPurchaseExcel;
import io.renren.modules.boby_book.service.UserPurchaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 用户购买
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/userpurchase")
@Api(tags = "用户购买")
public class UserPurchaseController {
    @Autowired
    private UserPurchaseService purchaseService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:userpurchase:page")
    public Result<PageData<UserPurchaseDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<UserPurchaseDTO> page = purchaseService.page(params);

        return new Result<PageData<UserPurchaseDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:userpurchase:info")
    public Result<UserPurchaseDTO> get(@PathVariable("id") Long id) {
        UserPurchaseDTO data = purchaseService.get(id);

        return new Result<UserPurchaseDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:userpurchase:save")
    public Result save(@RequestBody UserPurchaseDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        purchaseService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:userpurchase:update")
    public Result update(@RequestBody UserPurchaseDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        purchaseService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:userpurchase:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        purchaseService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:userpurchase:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<UserPurchaseDTO> list = purchaseService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, UserPurchaseExcel.class);
    }

    /**
     * 查询我已购买的书籍
     *
     * @param id 用户id
     * @return
     */
    @PostMapping("/selectPurchaseBook")
    public Result selectPurchaseBook(Long id) {
        List<BookEntity> entity = purchaseService.selectPurchaseBook(id);

        return new Result().ok(entity);
    }

    /**
     * 查询我已购买的书籍的章节
     *
     * @param bookid 图书id
     * @param userid 用户id
     * @return
     */
    @PostMapping("/selectPurchaseBookChapter")
    public Result selectPurchaseBookChapter(Long bookid, Long userid) {
        List<BookChapterEntity> entity = purchaseService.selectPurchaseBookChapter(bookid, userid);
        return new Result().ok(entity);
    }

}