package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommodityOrderItemEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 订单详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface CommodityOrderItemDao extends BaseDao<CommodityOrderItemEntity> {


    /**
     * 取消/删除订单
     *
     * @param orderId 订单id
     */
    @Delete("DELETE FROM e_commodity_order_item WHERE order_id = #{order_id}")
    void cancelOrder(@Param("order_id") Long orderId);
}