package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.WeChatUtils;
import io.renren.modules.boby_book.dao.BookDao;
import io.renren.modules.boby_book.dao.BookCommentDao;
import io.renren.modules.boby_book.dto.BookCommentDTO;
import io.renren.modules.boby_book.entity.BookEntity;
import io.renren.modules.boby_book.entity.BookCommentEntity;
import io.renren.modules.boby_book.service.BookCommentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 图书章节评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-10-30
 */
@Service
@Transactional
public class BookCommentServiceImpl extends CrudServiceImpl<BookCommentDao, BookCommentEntity, BookCommentDTO> implements BookCommentService {

    @Override
    public QueryWrapper<BookCommentEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<BookCommentEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private BookCommentDao commentDao;

    @Autowired
    private BookDao bookDao;


    @Override
    public void addComment(Long bookId, Long fuid, String content, String ip) {
        String cnAa = WeChatUtils.changeCharacter(content);
        BookCommentEntity entity = new BookCommentEntity();
        entity.setBookId(bookId);
        entity.setUserIp(ip);
        entity.setCommentTime(new Date());
        entity.setCommentContent(cnAa);
        entity.setFromUid(fuid);
        entity.setReplayCount(0);
        commentDao.insert(entity);
        BookEntity bookEntity = bookDao.selectById(bookId);
        Integer bookCommentNum = bookEntity.getBookCommentNum();
        bookCommentNum++;
        bookEntity.setBookCommentNum(bookCommentNum);
        bookDao.updateById(bookEntity);


    }


    @Override
    public List<BookCommentEntity> selectCommentByBookId(Long bookId) {
        return commentDao.selectCommentByBookId(bookId);
    }

    @Override
    public BookCommentEntity selectCommentById(Long id) {
        return commentDao.selectCommentById(id);
    }
}