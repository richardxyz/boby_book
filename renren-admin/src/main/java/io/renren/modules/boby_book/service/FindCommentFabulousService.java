package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.FindCommentFabulousDTO;
import io.renren.modules.boby_book.entity.FindCommentFabulousEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * 发现评论点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-20
 */
public interface FindCommentFabulousService extends CrudService<FindCommentFabulousEntity, FindCommentFabulousDTO> {
    /**
     * 根据发现评论id判断点赞状态
     *
     * @param userId        用户id
     * @param findCommentId 发现评论id
     * @return
     */
    Integer judgeFindCommentFabulousStatus(Long userId, Long findCommentId);

    /**
     * 取消发现评论点赞数
     *
     * @param userId        用户id
     * @param findCommentId 发现评论id
     */
    void cancelFindCommentFabulous(Long userId, Long findCommentId);

    /**
     * 添加发现评论点赞数
     *
     * @param userId        用户id
     * @param findCommentId 发现评论id
     * @param request
     */
    void addFindCommentFabulous(Long userId, Long findCommentId, HttpServletRequest request);
}