package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 用户收货地址表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class UserReceiveAddressExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "用户ID")
    private Long userId;
    @Excel(name = "收货人名称")
    private String name;
    @Excel(name = "收货人手机号")
    private String phoneNumber;
    @Excel(name = "是否为默认")
    private Integer defaultStatus;
    @Excel(name = "邮政编码")
    private String postCode;
    @Excel(name = "省份/直辖市")
    private String province;
    @Excel(name = "城市")
    private String city;
    @Excel(name = "区")
    private String region;
    @Excel(name = "详细地址(街道)")
    private String detailAddress;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}