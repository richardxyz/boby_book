package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.CommodityBaseAttrInfoDTO;
import io.renren.modules.boby_book.excel.CommodityBaseAttrInfoExcel;
import io.renren.modules.boby_book.service.CommodityBaseAttrInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 属性表(用作于查询)
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@RestController
@RequestMapping("boby_book/commoditybaseattrinfo")
@Api(tags = "属性表(用作于查询)")
public class CommodityBaseAttrInfoController {

    @Autowired
    private CommodityBaseAttrInfoService baseAttrInfoService;


    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:commoditybaseattrinfo:page")
    public Result<PageData<CommodityBaseAttrInfoDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<CommodityBaseAttrInfoDTO> page = baseAttrInfoService.page(params);

        return new Result<PageData<CommodityBaseAttrInfoDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:commoditybaseattrinfo:info")
    public Result<CommodityBaseAttrInfoDTO> get(@PathVariable("id") Long id) {
        CommodityBaseAttrInfoDTO data = baseAttrInfoService.get(id);

        return new Result<CommodityBaseAttrInfoDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:commoditybaseattrinfo:save")
    public Result save(@RequestBody CommodityBaseAttrInfoDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        baseAttrInfoService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:commoditybaseattrinfo:update")
    public Result update(@RequestBody CommodityBaseAttrInfoDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        baseAttrInfoService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:commoditybaseattrinfo:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        baseAttrInfoService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:commoditybaseattrinfo:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<CommodityBaseAttrInfoDTO> list = baseAttrInfoService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, CommodityBaseAttrInfoExcel.class);
    }

}