package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.renren.modules.boby_book.dao.BookDao;
import io.renren.modules.boby_book.entity.BookEntity;
import io.renren.modules.boby_book.service.BookService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 图书信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-10-30
 */
@Service
@Transactional
public class BookServiceImpl extends ServiceImpl<BookDao, BookEntity> implements BookService {


}