package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.FindCommentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 发现数据评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface FindCommentDao extends BaseDao<FindCommentEntity> {

    /**
     * 根据id查询发现评论信息
     *
     * @param findId 发现数据id
     * @return
     */
    List<FindCommentEntity> selectCommentByFindId(@Param("find_id") Long findId);

    /**
     * 根据评论id查询评论信息
     *
     * @param id 评论id
     * @return
     */
    FindCommentEntity selectFindCommentById(Long id);
}