package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommodityCommentReplyDTO;
import io.renren.modules.boby_book.entity.CommodityCommentReplyEntity;

import java.util.List;

/**
 * 评价回复
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommodityCommentReplyService extends CrudService<CommodityCommentReplyEntity, CommodityCommentReplyDTO> {

    /**
     * 添加评论回复
     *
     * @param cid     目标评论id
     * @param fuid    回复用户id
     * @param tuid    目标用户id
     * @param content 评论内容
     * @param ip      回复用户ip
     */
    void saveCommentReply(Long cid, Long fuid, Long tuid, String content, String ip);

    /**
     * 根据id查询商品评价回复信息
     *
     * @param id 商品id
     * @return
     */
    List<CommodityCommentReplyEntity> selectCommentReplyById(Long id);
}