package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserDownloadDTO;
import io.renren.modules.boby_book.entity.BookChapterEntity;
import io.renren.modules.boby_book.entity.BookEntity;
import io.renren.modules.boby_book.entity.UserDownloadEntity;

import java.util.List;

/**
 * 用户下载
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserDownloadService extends CrudService<UserDownloadEntity, UserDownloadDTO> {
    /**
     * 查询我已下载的书籍
     *
     * @param id 用户id
     * @return
     */
    List<BookEntity> selectDownloadBook(Long id);

    /**
     * 查询我已下载的书籍的章节
     *
     * @param bookid 图书id
     * @param userid 用户id
     * @return
     */
    List<BookChapterEntity> selectDownloadBookChapter(Long bookid, Long userid);
}