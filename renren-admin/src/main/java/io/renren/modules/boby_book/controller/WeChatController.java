package io.renren.modules.boby_book.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.renren.common.constant.WeiXinData;
import io.renren.common.utils.CommonUtil;
import io.renren.common.utils.HttpUtil;
import io.renren.common.utils.Result;
import io.renren.modules.boby_book.vo.Template;
import io.renren.modules.boby_book.vo.TemplateParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/boby_book/weChat")
public class WeChatController {

    /**
     * 测试发送订单消息
     *
     * @param templateId 所需下发的订阅模板id
     * @param openid     接收者（用户）的 openid
     */
    @PostMapping("/sendSubscribeMsg")
    public Result sendSubscribeMsg(String templateId, String openid) {
        System.out.println("订阅模板id：" + templateId);
        if (openid == "" || openid.equals("") || openid == null) {
            return new Result().error("当前用户未绑定微信");
        }
        String url = "https://api.weixin.qq.com/cgi-bin/token?"
                + "grant_type=" + WeiXinData.CLIENT_CREDENTIAL
                + "&appid=" + WeiXinData.APP_ID
                + "&secret=" + WeiXinData.APP_SECRET;
        String result = HttpUtil.sendGet(url);
        JSONObject object = JSON.parseObject(result);
        String Access_Token = object.getString("access_token");
        System.out.println(Access_Token);
        Template template = new Template();
        template.setTemplate_id(templateId);
        template.setTouser(openid);
        template.setPage("pages/index/index");
        List<TemplateParam> paras = new ArrayList<TemplateParam>();
        paras.add(new TemplateParam("number1", "123456"));
        //创建SimpleDateFormat对象实例并定义好转换格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = sdf.format(new Date());
        paras.add(new TemplateParam("date2", date));
        paras.add(new TemplateParam("thing3", "订单2天内配送"));
        paras.add(new TemplateParam("thing4", "急件"));
        paras.add(new TemplateParam("thing5", "玩具熊，泰迪熊"));
        template.setTemplateParamList(paras);
        String requestUrl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=ACCESS_TOKEN";
        requestUrl = requestUrl.replace("ACCESS_TOKEN", Access_Token);

        System.out.println(template.toJSON());
        net.sf.json.JSONObject jsonResult = CommonUtil.httpsRequest(requestUrl, "POST", template.toJSON());
        if (jsonResult != null) {
            System.out.println(jsonResult);
            int errorCode = jsonResult.getInt("errcode");
            String errorMessage = jsonResult.getString("errmsg");
            if (errorCode == 0) {
                System.out.println("订阅消息发送成功");
            } else {
                System.out.println("订阅消息发送失败:" + errorCode + "," + errorMessage);
            }
        }
        return new Result().ok("订阅消息发送完成");
    }

    /**
     * 发送签到消息
     *
     * @param templateId 所需下发的订阅模板id
     * @param openid     接收者（用户）的 openid
     */
    @PostMapping("/sendSignInMsg")
    public Result sendSignInMsg(String templateId, String openid) {
        System.out.println("订阅模板id：" + templateId);
        if (openid == "" || openid.equals("") || openid == null) {
            return new Result().error("当前用户未绑定微信");
        }
        String url = "https://api.weixin.qq.com/cgi-bin/token?"
                + "grant_type=" + WeiXinData.CLIENT_CREDENTIAL
                + "&appid=" + WeiXinData.APP_ID
                + "&secret=" + WeiXinData.APP_SECRET;
        String result = HttpUtil.sendGet(url);
        JSONObject object = JSON.parseObject(result);
        String Access_Token = object.getString("access_token");
        System.out.println(Access_Token);
        Template template = new Template();
        template.setTemplate_id(templateId);
        template.setTouser(openid);
        template.setPage("pages/index/index");
        List<TemplateParam> paras = new ArrayList<TemplateParam>();
        paras.add(new TemplateParam("thing1", "每日签到"));
        paras.add(new TemplateParam("thing2", "点击立即签到"));
        template.setTemplateParamList(paras);
        String requestUrl = "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=ACCESS_TOKEN";
        requestUrl = requestUrl.replace("ACCESS_TOKEN", Access_Token);

        System.out.println(template.toJSON());
        net.sf.json.JSONObject jsonResult = CommonUtil.httpsRequest(requestUrl, "POST", template.toJSON());
        if (jsonResult != null) {
            System.out.println(jsonResult);
            int errorCode = jsonResult.getInt("errcode");
            String errorMessage = jsonResult.getString("errmsg");
            if (errorCode == 0) {
                System.out.println("签到消息发送成功");
            } else {
                System.out.println("签到消息发送失败:" + errorCode + "," + errorMessage);
            }
        }
        return new Result().ok("签到消息发送完成");
    }
}
