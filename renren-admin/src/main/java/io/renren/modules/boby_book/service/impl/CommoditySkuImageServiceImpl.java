package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommoditySkuImageDao;
import io.renren.modules.boby_book.dto.CommoditySkuImageDTO;
import io.renren.modules.boby_book.entity.CommoditySkuImageEntity;
import io.renren.modules.boby_book.service.CommoditySkuImageService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 商品图片表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-11-11
 */
@Service
@Transactional
public class CommoditySkuImageServiceImpl extends CrudServiceImpl<CommoditySkuImageDao, CommoditySkuImageEntity, CommoditySkuImageDTO> implements CommoditySkuImageService {

    @Override
    public QueryWrapper<CommoditySkuImageEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<CommoditySkuImageEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}