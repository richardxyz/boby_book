package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 图书章节信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_book_chapter")
public class BookChapterEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 图书名称
     */
    private String bookName;
    /**
     * 图书章节名
     */
    private String chapterName;

    /**
     * 图书章节内容
     */
    private String chapterContent;
    /**
     * 图书章节封面
     */
    private String chapterImg;
    /**
     * 图书播放数量
     */
    private Integer bookPlayNums;
    /**
     * 图书收藏数量
     */
    private Integer bookCollectionNum;
    /**
     * 图书评论数量
     */
    private Integer bookCommentNum;
    /**
     * 书籍id
     */
    private Long bookId;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;


}