
package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.exception.ErrorCode;
import io.renren.common.page.PageData;
import io.renren.common.redis.RedisUtils;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.UserDTO;
import io.renren.modules.boby_book.entity.UserEntity;
import io.renren.modules.boby_book.entity.UserSpecificInformationEntity;
import io.renren.modules.boby_book.excel.UserExcel;
import io.renren.modules.boby_book.service.UserService;
import io.renren.modules.boby_book.service.UserSignInService;
import io.renren.modules.boby_book.service.UserSpecificInformationService;
import io.renren.modules.security.password.PasswordUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户基本信息表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/user")
@Api(tags = "用户基本信息表")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserSignInService signInService;

    @Autowired
    private UserSpecificInformationService specificInformationService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:user:page")
    public Result<PageData<UserDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<UserDTO> page = userService.page(params);

        return new Result<PageData<UserDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:user:info")
    public Result<UserDTO> get(@PathVariable("id") Long id) {
        UserDTO data = userService.get(id);

        return new Result<UserDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:user:save")
    public Result save(@RequestBody UserDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        userService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:user:update")
    public Result update(@RequestBody UserDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        userService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:user:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        userService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:user:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<UserDTO> list = userService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, UserExcel.class);
    }

    /**
     * 获取用户信息
     *
     * @param userid 用户id
     * @return
     */
    @PostMapping("/getUserInformation")
    public Result getUserInformation(Long userid) {
        System.err.println("getUserInformation用户id：" + userid);
        UserEntity entity1 = userService.selectById(userid);
        UserSpecificInformationEntity entity2 = specificInformationService.selectUserSpecificInformationById(entity1.getId());
        Date date = signInService.lastSignIn(userid);
        Map<String, Object> map = new HashMap<>();
        if (date == null) {
            map.put("entity1", entity1);
            map.put("entity2", entity2);
            map.put("status", 0);
            return new Result().ok(map);
        } else {
            Integer status = signInService.checkAllotSigin(date);
            map.put("entity1", entity1);
            map.put("entity2", entity2);
            map.put("status", status);
            return new Result().ok(map);
        }
    }

    /**
     * 修改头像(手机可查看)
     *
     * @param response
     * @param request
     * @param userid   用户id
     * @return
     */
    @RequestMapping(value = "/changeHeadPortrait", method = RequestMethod.POST)
    @ResponseBody
    public String changeHeadPortrait(HttpServletResponse response, HttpServletRequest request, Long userid) {
        MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = req.getFile("file");
        String filePath = "";
//        String filePath = UploadUtil.uploadImage(multipartFile);
        System.err.println("用户id：" + userid);
        userService.changeHeadPortrait(filePath, userid);
        return filePath;

    }

    /**
     * 修改头像(模拟器预览)
     *
     * @param response
     * @param request
     * @param userid   用户id
     * @return
     */
    @RequestMapping(value = "/setHeadPortrait", method = RequestMethod.POST)
    @ResponseBody
    public Result setHeadPortrait(HttpServletResponse response, HttpServletRequest request, Long userid) {
        MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = req.getFile("file");
        return userService.updUserProfile(multipartFile, userid);
    }


    /**
     * 用户修改昵称
     *
     * @param username 用户昵称
     * @param userid   用户id
     * @return
     */
    @PostMapping("/changeUserName")
    public Result changeUserName(String username, Long userid) {
        System.err.print("昵称：" + username);
        System.err.println("用户id：" + userid);
        userService.changeUserName(username, userid);
        return new Result();
    }

    /**
     * 用户修改语录
     *
     * @param quotations 语录
     * @param userid     用户ID
     * @return
     */
    @PostMapping("/changeQuotations")
    public Result changeQuotations(String quotations, Long userid) {
        System.err.print("语录：" + quotations);
        System.err.println("用户id：" + userid);
        userService.changeQuotations(quotations, userid);
        return new Result();
    }

    /**
     * 用户修改地区
     *
     * @param regional 地区
     * @param userid   用户id
     * @return
     */
    @PostMapping("/changeRegional")
    public Result changeRegional(String regional, Long userid) {
        System.err.print("地区：" + regional);
        System.err.println("用户id：" + userid);
        userService.changeRegional(regional, userid);
        return new Result();
    }

    /**
     * 用户修改详细地址
     *
     * @param detailedAddress 详细地址
     * @param userid          用户ID
     * @return
     */
    @PostMapping("/changedeTailedAddress")
    public Result changedeTailedAddress(String detailedAddress, Long userid) {
        System.err.print("地区：" + detailedAddress);
        System.err.println("用户id：：" + userid);
        userService.changedeTailedAddress(detailedAddress, userid);
        return new Result();
    }


    /**
     * 用户修改性别
     *
     * @param gender 性别
     * @param userid 用户id
     * @return
     */
    @PostMapping("/changedeGender")
    public Result changedeGender(String gender, Long userid) {
        System.err.print("性别：" + gender);
        System.err.println("用户id：：" + userid);
        userService.changedeGender(gender, userid);
        return new Result();
    }

    @Autowired
    private RedisUtils redisUtils;

    /**
     * 绑定手机号
     *
     * @param phone  手机号
     * @param vcode  验证码
     * @param userid 用户id
     * @return
     */
    @PostMapping("/bindPhone")
    public Result bindPhone(String phone, String vcode, Long userid) {
        System.err.print("手机号：" + phone);
        System.err.println("验证码：：" + vcode);
        Integer count = userService.judgePhineExist(phone);
        Map<String, Object> map = new HashMap<>();
        if (count >= 1) {
            map.put("code", 606);
            map.put("msg", "此手机号已被绑定");
            return new Result().ok(map);
        } else {
            System.out.println("*****绑定手机号*****");
            //.获得前台传递过来的验证码
            String verificationCode = vcode;
            System.out.println("前台传递过来的验证码:" + verificationCode);
            //.获得Redis数据库中存储的数据
//        String redisVerificationCode = redisTemplate.opsForValue().get(phone);
            String redisVerificationCode = (String) redisUtils.get(phone);
            System.out.println("Redis数据库中存储的验证码:" + redisVerificationCode);
            if (verificationCode.equalsIgnoreCase(redisVerificationCode)) {
                userService.bindPhone(phone, userid);
                map.put("code", 0);
                map.put("msg", "绑定成功");
                return new Result().ok(map);
            } else {
                map.put("code", 500);
                map.put("msg", "绑定失败");
                return new Result().ok(map);
            }
        }
    }


    /**
     * 用户修改密码
     *
     * @param password    用户输入密码
     * @param userId      用户id
     * @param newPassword 新密码
     * @return
     */
    @PostMapping("/changedePwd")
    public Result changedePwd(String password, Long userId, String newPassword) {
        System.err.println("密码：" + password);
        System.err.println("用户id：" + userId);
        System.err.println("新密码：" + newPassword);
        UserEntity entity = userService.selectById(userId);
        //原密码不正确
        if (!PasswordUtils.matches(password, entity.getPassword())) {
            return new Result().error(ErrorCode.PASSWORD_ERROR);
        }
        userService.changedePwd(newPassword, userId);
        return new Result();
    }

    /**
     * 查询当前用户是否关注
     *
     * @param fuId 用户id
     * @param tuId 所关注用户id
     * @return
     */
    @PostMapping("/selectFollowStyle")
    public Result selectFollowStyle(Long fuId, Long tuId) {
        Map<String, Object> map = new HashMap<>();
        System.err.println("用户id：" + fuId);
        System.err.println("所关注用户id：" + tuId);
        Integer count = userService.selectFollowStyle(fuId, tuId);
        if (count == 1) {
            map.put("code", "0");
            map.put("msg", "已关注");
            return new Result().ok(map);
        } else {
            map.put("code", "500");
            map.put("msg", "未关注");
            return new Result().ok(map);
        }
    }

    /**
     * 关注用户
     *
     * @param fuId 用户id
     * @param tuId 所关注用户id
     * @return
     */
    @PostMapping("/followUser")
    public Result followUser(Long fuId, Long tuId) {
        System.err.println("用户id：" + fuId);
        System.err.println("所关注用户id：" + tuId);
        try {
            userService.followUser(fuId, tuId);
            return new Result().ok("关注成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result().error("关注失败");
        }
    }

    /**
     * 取消关注
     *
     * @param fuId 用户id
     * @param tuId 所关注用户id
     * @return
     */
    @PostMapping("/cancelFollowUser")
    public Result cancelFollowUser(Long fuId, Long tuId) {
        System.err.println("用户id：" + fuId);
        System.err.println("所关注用户id：" + tuId);
        try {
            userService.cancelFollowUser(fuId, tuId);
            return new Result().ok("取消关注成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result().error("取消关注失败");
        }
    }

}