package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommoditySpuInfoDTO;
import io.renren.modules.boby_book.entity.CommoditySpuInfoEntity;

import java.util.List;

/**
 * 兑换商品
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommoditySpuInfoService extends CrudService<CommoditySpuInfoEntity, CommoditySpuInfoDTO> {

    /**
     * 查询所有商品数据
     *
     * @return
     */
    List<CommoditySpuInfoEntity> selectSpuInfoList();
    /**
     * 根据id查询商品数据
     * @param id 商品id
     * @return
     */
    CommoditySpuInfoEntity selectSpuInfoById(Long id);
}