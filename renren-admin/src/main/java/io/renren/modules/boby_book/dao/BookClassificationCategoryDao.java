package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.BookClassificationCategoryEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 书籍分类中间表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Mapper
public interface BookClassificationCategoryDao extends BaseDao<BookClassificationCategoryEntity> {
	
}