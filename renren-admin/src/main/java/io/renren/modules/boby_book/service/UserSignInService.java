package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserSignInDTO;
import io.renren.modules.boby_book.entity.UserSignInEntity;

import java.util.Date;

/**
 * 用户签到表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserSignInService extends CrudService<UserSignInEntity, UserSignInDTO> {
    /**
     * 获取最后一次签到的日期
     *
     * @param userid 用户id
     * @return
     */
    Date lastSignIn(Long userid);

    /**
     * 获取用户签到状态
     *
     * @param date 最后一次签到的日期
     * @return
     */
    Integer checkAllotSigin(Date date);

}