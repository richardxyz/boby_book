package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.BookChapterDao;
import io.renren.modules.boby_book.dto.BookChapterDTO;
import io.renren.modules.boby_book.entity.BookChapterEntity;
import io.renren.modules.boby_book.service.BookChapterService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 图书章节信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class BookChapterServiceImpl extends CrudServiceImpl<BookChapterDao, BookChapterEntity, BookChapterDTO> implements BookChapterService {

    @Override
    public QueryWrapper<BookChapterEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<BookChapterEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private BookChapterDao chapterDao;

    @Override
    public List<BookChapterEntity> selectBookChapterOrderByASC(Long bookId) {
        return chapterDao.selectBookChapterOrderByASC(bookId);
    }

    @Override
    public List<BookChapterEntity> selectBookChapterOrderByDESC(Long bookId) {
        return chapterDao.selectBookChapterOrderByDESC(bookId);
    }
}