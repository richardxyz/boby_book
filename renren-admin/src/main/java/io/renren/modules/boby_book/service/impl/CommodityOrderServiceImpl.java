package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.redis.RedisUtil;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommodityOrderDao;
import io.renren.modules.boby_book.dao.CommodityOrderItemDao;
import io.renren.modules.boby_book.dto.CommodityOrderDTO;
import io.renren.modules.boby_book.entity.CommodityOrderEntity;
import io.renren.modules.boby_book.entity.CommodityOrderItemEntity;
import io.renren.modules.boby_book.service.CommodityOrderService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.Jedis;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 订单
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-10-30
 */
@Service
@Transactional
public class CommodityOrderServiceImpl extends CrudServiceImpl<CommodityOrderDao, CommodityOrderEntity, CommodityOrderDTO> implements CommodityOrderService {

    @Override
    public QueryWrapper<CommodityOrderEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<CommodityOrderEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private CommodityOrderDao orderDao;
    @Autowired
    private CommodityOrderItemDao orderItemDao;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public String genTradeCode(Long userid) {
        Jedis jedis = null;
        try {
            jedis = redisUtil.getJedis();
            String tradeKey = "user:" + userid + ":tradeCode";
            String tradeCode = UUID.randomUUID().toString();
            jedis.setex(tradeKey, 60 * 15, tradeCode);//交易码维持15分钟
            return tradeCode;
        } finally {
            jedis.close();
        }

    }

    @Override
    public String checkTradeCode(Long userid, String tradeCode) {
        Jedis jedis = null;
        try {
            jedis = redisUtil.getJedis();
            String tradeKey = "user:" + userid + ":tradeCode";
            //String tradeCodeFromCache = jedis.get(tradeKey);// 使用lua脚本在发现key的同时将key删除，防止并发订单攻击
            //对比防重删令牌
            String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
            Long eval = (Long) jedis.eval(script, Collections.singletonList(tradeKey), Collections.singletonList(tradeCode));
            if (eval != null && eval != 0) {
                jedis.del(tradeKey);
                return "success";
            } else {
                return "fail";
            }
        } finally {
            jedis.close();
        }
    }

    @Override
    public void saveOrder(CommodityOrderEntity order) {
        // 保存订单表
        orderDao.insert(order);
        Long orderId = order.getId();
        // 保存订单详情
        List<CommodityOrderItemEntity> orderItemList = order.getOrderItems();
        for (CommodityOrderItemEntity orderItem : orderItemList) {
            orderItem.setOrderId(orderId);
            orderItemDao.insert(orderItem);
            // 删除购物车数据
            // cartService.delCart();
        }

    }

    @Override
    public List<CommodityOrderEntity> findAllOrder(Long userId) {
        return orderDao.findAllOrder(userId);
    }

    @Override
    public void cancelOrder(Long orderId) {
        orderDao.deleteById(orderId);
        orderItemDao.cancelOrder(orderId);
    }
}