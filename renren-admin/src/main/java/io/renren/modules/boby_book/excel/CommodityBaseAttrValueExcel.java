package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 销售属性值表(用作于用户选择)
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommodityBaseAttrValueExcel {
    @Excel(name = "编号")
    private Long id;
    @Excel(name = "属性值名称")
    private String valueName;
    @Excel(name = "属性id")
    private Long attrId;
    @Excel(name = "启用：1 停用：0 1")
    private String isEnabled;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}