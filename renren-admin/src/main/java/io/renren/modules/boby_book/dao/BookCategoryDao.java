package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.BookCategoryEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 图书类别
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface BookCategoryDao extends BaseDao<BookCategoryEntity> {
	
}