package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.BookFabulousDTO;
import io.renren.modules.boby_book.entity.BookFabulousEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * 图书点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-20
 */
public interface BookFabulousService extends CrudService<BookFabulousEntity, BookFabulousDTO> {

    /**
     * 根据图书id判断点赞状态
     *
     * @param userId 用户id
     * @param bookId 图书id
     * @return
     */
    Integer judgeBookFabulousStatus(Long userId, Long bookId);

    /**
     * 给图书点赞
     *
     * @param userId 用户id
     * @param bookId 图书id
     */
    void cancelBookFabulous(Long userId, Long bookId);

    /**
     * 图书取消点赞
     *
     * @param userId  用户id
     * @param bookId  图书id
     * @param request
     */
    void addBookFabulous(Long userId, Long bookId, HttpServletRequest request);
}