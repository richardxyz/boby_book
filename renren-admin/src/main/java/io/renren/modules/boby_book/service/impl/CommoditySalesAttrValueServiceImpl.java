package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommoditySalesAttrValueDao;
import io.renren.modules.boby_book.dto.CommoditySalesAttrValueDTO;
import io.renren.modules.boby_book.entity.CommoditySalesAttrValueEntity;
import io.renren.modules.boby_book.service.CommoditySalesAttrValueService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 销售属性值
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Service
@Transactional
public class CommoditySalesAttrValueServiceImpl extends CrudServiceImpl<CommoditySalesAttrValueDao, CommoditySalesAttrValueEntity, CommoditySalesAttrValueDTO> implements CommoditySalesAttrValueService {

    @Override
    public QueryWrapper<CommoditySalesAttrValueEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<CommoditySalesAttrValueEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}