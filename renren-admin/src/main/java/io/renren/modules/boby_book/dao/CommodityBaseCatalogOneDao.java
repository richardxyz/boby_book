package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogOneEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface CommodityBaseCatalogOneDao extends BaseDao<CommodityBaseCatalogOneEntity> {

}