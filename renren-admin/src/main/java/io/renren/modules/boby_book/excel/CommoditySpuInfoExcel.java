package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 兑换商品
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommoditySpuInfoExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "商品名称")
    private String commodityName;
    @Excel(name = "商品描述")
    private String commodityDescribe;
    @Excel(name = "品牌id")
    private Long brandId;
    @Excel(name = "商品分类id")
    private Long catalogId;
    @Excel(name = "默认积分")
    private Integer integral;
    @Excel(name = "默认价格")
    private Double price;
    @Excel(name = "已兑换总个数")
    private Integer exchangeNum;
    @Excel(name = "评论数")
    private Integer commentNum;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}