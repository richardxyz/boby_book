package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 销售属性
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommoditySalesAttrExcel {
    @Excel(name = "主键id")
    private Long id;
    @Excel(name = "商品id")
    private Long commodityId;
    @Excel(name = "销售属性id")
    private Long saleAttrId;
    @Excel(name = "销售属性名称(冗余)")
    private String saleAttrName;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}