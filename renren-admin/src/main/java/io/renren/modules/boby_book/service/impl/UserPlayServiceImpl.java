package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.BookDao;
import io.renren.modules.boby_book.dao.UserPlayDao;
import io.renren.modules.boby_book.dto.UserPlayDTO;
import io.renren.modules.boby_book.entity.BookEntity;
import io.renren.modules.boby_book.entity.UserPlayEntity;
import io.renren.modules.boby_book.service.UserPlayService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 用户最近播放
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class UserPlayServiceImpl extends CrudServiceImpl<UserPlayDao, UserPlayEntity, UserPlayDTO> implements UserPlayService {

    @Override
    public QueryWrapper<UserPlayEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<UserPlayEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Autowired
    private UserPlayDao play2Dao;
    @Autowired
    private BookDao book2Dao;

    @Override
    public List<UserPlayEntity> selectTodayBook(Long id) {
        return play2Dao.selectTodayBook(id);
    }

    @Override
    public List<UserPlayEntity> selectYesterdayBook(Long id) {
        return play2Dao.selectYesterdayBook(id);
    }

    @Override
    public List<UserPlayEntity> selecEarlierBook(Long id) {
        return play2Dao.selecEarlierBook(id);
    }

    @Override
    public void addToPlay(Long userId, Long bookId) {
        UserPlayEntity entity = new UserPlayEntity();
        entity.setBookId(bookId);
        entity.setPlayTime(new Date());
        entity.setUserId(userId);
        play2Dao.insert(entity);
        BookEntity book2Entity = book2Dao.selectById(bookId);
        Integer bookPlayNum = book2Entity.getBookPlayNum();
        bookPlayNum++;
        book2Entity.setBookPlayNum(bookPlayNum);
        book2Dao.updateById(book2Entity);
    }

    @Override
    public Integer lastPlay(Long userId, Long bookId) {
        return play2Dao.lastPlay(userId,bookId);
    }
}