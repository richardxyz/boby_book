package io.renren.modules.boby_book.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.renren.modules.boby_book.entity.BookEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 图书信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface BookDao extends BaseMapper<BookEntity> {

}