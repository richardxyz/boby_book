package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.WeChatUtils;
import io.renren.modules.boby_book.dao.FindCommentDao;
import io.renren.modules.boby_book.dao.FindDao;
import io.renren.modules.boby_book.dto.FindCommentDTO;
import io.renren.modules.boby_book.entity.FindCommentEntity;
import io.renren.modules.boby_book.entity.FindEntity;
import io.renren.modules.boby_book.service.FindCommentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 发现数据评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class FindCommentServiceImpl extends CrudServiceImpl<FindCommentDao, FindCommentEntity, FindCommentDTO> implements FindCommentService {

    @Override
    public QueryWrapper<FindCommentEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<FindCommentEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private FindDao findDao;
    @Autowired
    private FindCommentDao findCommentDao;

    @Override
    public List<FindCommentEntity> selectCommentByFindId(Long findId) {
        return findCommentDao.selectCommentByFindId(findId);
    }

    @Override
    public void addFindComment(Long findId, Long fuid, String content, String ip) {
        String cnAa = WeChatUtils.changeCharacter(content);
        FindCommentEntity entity = new FindCommentEntity();
        entity.setFindId(findId);
        entity.setUserId(fuid);
        entity.setFindComment(cnAa);
        entity.setFindCommentTime(new Date());
        entity.setUserIp(ip);
        findCommentDao.insert(entity);
        FindEntity find2Entity = findDao.selectById(findId);
        Integer commentNum = find2Entity.getCommentNum();
        commentNum++;
        find2Entity.setCommentNum(commentNum);
        findDao.updateById(find2Entity);
    }

    @Override
    public FindCommentEntity selectFindCommentById(Long id) {

        return findCommentDao.selectFindCommentById(id);
    }
}