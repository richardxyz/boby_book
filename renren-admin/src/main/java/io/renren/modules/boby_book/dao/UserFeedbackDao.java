package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.UserFeedbackEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * 意见反馈
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserFeedbackDao extends BaseDao<UserFeedbackEntity> {
    /**
     * 查询当前用户最后一条反馈数据
     *
     * @param userId 用户id
     * @return
     */
    @Select("SELECT * FROM e_user_feedback WHERE user_id = #{user_id} ORDER BY id DESC LIMIT 0,1;")
    UserFeedbackEntity findLastFeedBack(@Param("user_id") Long userId);

    /**
     * 添加意见反馈截图
     *
     * @param filePath 反馈图片
     * @param userId   用户id
     */
    @Update("UPDATE e_user_feedback SET feedback_image = #{feedback_image}  WHERE id IN (SELECT a.id FROM (SELECT * FROM e_user_feedback WHERE user_id = #{user_id}  ORDER BY id DESC LIMIT 1)a)")
    void uploadFeedbackImg(@Param("feedback_image") String filePath, @Param("user_id") Long userId);
}