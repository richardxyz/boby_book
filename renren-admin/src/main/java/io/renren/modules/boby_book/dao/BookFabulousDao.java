package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.BookFabulousEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 图书点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-20
 */
@Mapper
public interface BookFabulousDao extends BaseDao<BookFabulousEntity> {
    /**
     * 根据图书id判断点赞状态
     *
     * @param userId 用户id
     * @param bookId 图书id
     * @return
     */
    @Select("SELECT COUNT(*) FROM e_book_fabulous  WHERE  from_uid = #{from_uid} AND book_id = #{book_id}")
    Integer judgeBookFabulousStatus(@Param("from_uid")Long userId, @Param("book_id")Long bookId);

    /**
     * 根据userId和bookId查询点赞数据
     *
     * @param userId 用户id
     * @param bookId 图书id
     * @return
     */
    @Select("SELECT * FROM e_book_fabulous  WHERE  from_uid = #{from_uid} AND book_id = #{book_id}")
    BookFabulousEntity selectBookFabulousByUserIdAndBookId(@Param("from_uid")Long userId, @Param("book_id")Long bookId);
}