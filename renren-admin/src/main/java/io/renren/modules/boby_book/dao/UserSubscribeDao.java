package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.UserSubscribeEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 用户订阅
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserSubscribeDao extends BaseDao<UserSubscribeEntity> {
    /**
     * 查询当前图书是否订阅
     *
     * @param userId 用户id
     * @param bookId 图书id
     * @return
     */
    @Select("SELECT COUNT(*) FROM e_user_subscribe WHERE user_id = #{user_id} AND book_id = #{book_id}")
    Integer selectSubscribeStatus(@Param("user_id") Long userId, @Param("book_id") Long bookId);

    /**
     * 取消图书订阅
     *
     * @param userId 用户id
     * @param bookId 图书id
     */
    @Delete("DELETE FROM e_user_subscribe WHERE user_id = #{user_id} AND book_id = #{book_id}")
    void cancelSubscribeBook(@Param("user_id") Long userId, @Param("book_id") Long bookId);
}