package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 商品品牌
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommodityBrandExcel {
    @Excel(name = "品牌编号id")
    private Long id;
    @Excel(name = "品牌名称")
    private String name;
    @Excel(name = "首字母")
    private String firstLetter;
    @Excel(name = "分类")
    private Integer sort;
    @Excel(name = "是否为品牌制造商：0->不是；1->是")
    private Integer factoryStatus;
    @Excel(name = "显示状态")
    private Integer showStatus;
    @Excel(name = "产品数量")
    private Integer productCount;
    @Excel(name = "产品评论数量")
    private Integer productCommentCount;
    @Excel(name = "产品logo")
    private String logo;
    @Excel(name = "专区大图")
    private String bigPic;
    @Excel(name = "品牌故事")
    private String brandStory;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}