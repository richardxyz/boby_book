package io.renren.modules.boby_book.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.renren.modules.boby_book.entity.BookEntity;

/**
 * 图书信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface BookService extends IService<BookEntity> {

}