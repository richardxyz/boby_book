package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 意见反馈
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class UserFeedbackExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "反馈内容描述")
    private String feedbackContent;
    @Excel(name = "反馈图片")
    private String feedbackImage;
    @Excel(name = "反馈时间")
    private Date feedbackTime;
    @Excel(name = "用户id")
    private Long userId;
    @Excel(name = "用户ip")
    private String userIp;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}