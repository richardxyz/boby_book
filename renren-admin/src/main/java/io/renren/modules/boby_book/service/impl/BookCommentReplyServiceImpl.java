package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.WeChatUtils;
import io.renren.modules.boby_book.dao.BookCommentDao;
import io.renren.modules.boby_book.dao.BookCommentReplyDao;
import io.renren.modules.boby_book.dto.BookCommentReplyDTO;
import io.renren.modules.boby_book.entity.BookCommentEntity;
import io.renren.modules.boby_book.entity.BookCommentReplyEntity;
import io.renren.modules.boby_book.service.BookCommentReplyService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 图书评价回复
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Service
@Transactional
public class BookCommentReplyServiceImpl extends CrudServiceImpl<BookCommentReplyDao, BookCommentReplyEntity, BookCommentReplyDTO> implements BookCommentReplyService {

    @Override
    public QueryWrapper<BookCommentReplyEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<BookCommentReplyEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private BookCommentDao commentDao;
    @Autowired
    private BookCommentReplyDao commentReplyDao;


    @Override
    public void addCommentReply(Long cid, Long fuid, Long tuid, String content, String ip) {
        String cnAa = WeChatUtils.changeCharacter(content);
        BookCommentReplyEntity entity = new BookCommentReplyEntity();
        entity.setCommentId(cid);
        entity.setContent(cnAa);
        entity.setReplyTime(new Date());
        entity.setFromUid(fuid);
        entity.setToUid(tuid);
        entity.setFromUip(ip);
        commentReplyDao.insert(entity);
        BookCommentEntity bookComment2Entity = commentDao.selectById(cid);
        Integer replayCount = bookComment2Entity.getReplayCount();
        replayCount++;
        bookComment2Entity.setReplayCount(replayCount);
        commentDao.updateById(bookComment2Entity);

    }

    @Override
    public List<BookCommentReplyEntity> selectCommentReplyByCommentId(Long id) {
        return commentReplyDao.selectCommentReplyByCommentId(id);
    }
}