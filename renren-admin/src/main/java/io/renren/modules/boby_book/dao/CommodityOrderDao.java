package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommodityOrderEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface CommodityOrderDao extends BaseDao<CommodityOrderEntity> {
    /**
     * 查询登录用户所有订单
     *
     * @param userId 用户id
     * @return
     */
    List<CommodityOrderEntity> findAllOrder(@Param("user_id") Long userId);

}