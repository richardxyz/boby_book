package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.IpUtils;
import io.renren.modules.boby_book.dao.BookFabulousDao;
import io.renren.modules.boby_book.dto.BookFabulousDTO;
import io.renren.modules.boby_book.entity.BookEntity;
import io.renren.modules.boby_book.entity.BookFabulousEntity;
import io.renren.modules.boby_book.service.BookService;
import io.renren.modules.boby_book.service.BookFabulousService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 图书点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-1-0
 */
@Service
public class BookFabulousServiceImpl extends CrudServiceImpl<BookFabulousDao, BookFabulousEntity, BookFabulousDTO> implements BookFabulousService {

    @Override
    public QueryWrapper<BookFabulousEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<BookFabulousEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }
    @Autowired
    private BookFabulousDao bookFabulousDao;

    @Autowired
    private BookService bookService;

    @Autowired
    private IpUtils ipUtils;

    @Override
    public Integer judgeBookFabulousStatus(Long userId, Long bookId) {
        return bookFabulousDao.judgeBookFabulousStatus(userId,bookId);
    }

    @Override
    public void cancelBookFabulous(Long userId, Long bookId) {
        BookFabulousEntity bookFabulousEntity  = bookFabulousDao.selectBookFabulousByUserIdAndBookId(userId,bookId);
        bookFabulousDao.deleteById(bookFabulousEntity.getId());
        BookEntity entity = bookService.getById(bookId);
        Integer fabulousCount = entity.getBookFabulousNum();
        fabulousCount--;
        entity.setBookFabulousNum(fabulousCount);
        bookService.updateById(entity);
    }

    @Override
    public void addBookFabulous(Long userId, Long bookId, HttpServletRequest request) {
        String ip = ipUtils.getIpAddr(request);
        BookEntity entity = bookService.getById(bookId);
        Integer fabulousCount = entity.getBookFabulousNum();
        fabulousCount++;
        entity.setBookFabulousNum(fabulousCount);
        bookService.updateById(entity);
        BookFabulousEntity bookFabulousEntity = new BookFabulousEntity();
        bookFabulousEntity.setBookId(bookId);
        bookFabulousEntity.setFromUid(userId);
        bookFabulousEntity.setFromUip(ip);
        bookFabulousDao.insert(bookFabulousEntity);
    }

}