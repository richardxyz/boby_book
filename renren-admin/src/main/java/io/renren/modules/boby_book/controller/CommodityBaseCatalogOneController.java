package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.CommodityBaseCatalogOneDTO;
import io.renren.modules.boby_book.excel.CommodityBaseCatalogOneExcel;
import io.renren.modules.boby_book.service.CommodityBaseCatalogOneService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 一级商品分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since one.0.0 20one9-oneone-oneone
 */
@RestController
@RequestMapping("boby_book/commoditybasecatalogone")
@Api(tags = "商品分类")
public class CommodityBaseCatalogOneController {
    @Autowired
    private CommodityBaseCatalogOneService baseCatalogOneService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从one开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:commoditybasecatalogone:page")
    public Result<PageData<CommodityBaseCatalogOneDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<CommodityBaseCatalogOneDTO> page = baseCatalogOneService.page(params);

        return new Result<PageData<CommodityBaseCatalogOneDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:commoditybasecatalogone:info")
    public Result<CommodityBaseCatalogOneDTO> get(@PathVariable("id") Long id) {
        CommodityBaseCatalogOneDTO data = baseCatalogOneService.get(id);

        return new Result<CommodityBaseCatalogOneDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:commoditybasecatalogone:save")
    public Result save(@RequestBody CommodityBaseCatalogOneDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        baseCatalogOneService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:commoditybasecatalogone:update")
    public Result update(@RequestBody CommodityBaseCatalogOneDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        baseCatalogOneService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:commoditybasecatalogone:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        baseCatalogOneService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:commoditybasecatalogone:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<CommodityBaseCatalogOneDTO> list = baseCatalogOneService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, CommodityBaseCatalogOneExcel.class);
    }

}