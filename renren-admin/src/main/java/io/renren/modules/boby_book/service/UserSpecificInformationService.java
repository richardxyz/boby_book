package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserSpecificInformationDTO;
import io.renren.modules.boby_book.entity.UserSpecificInformationEntity;

/**
 * 用户信息详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserSpecificInformationService extends CrudService<UserSpecificInformationEntity, UserSpecificInformationDTO> {
    /**
     * 用户注册新增详细数据
     *
     * @param id 用户id
     */
    void Register(Long id);

    /**
     * 根据用户id查询详细数据
     *
     * @param userId 用户id
     * @return
     */
    UserSpecificInformationEntity selectUserSpecificInformationById(Long userId);

    /**
     * 修改积分
     *
     * @param integralNum 积分
     * @param userId      用户id
     */
    void updateIntegralNum(Integer integralNum, Long userId);

}