package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.FindCommentFabulousEntity;
import io.renren.modules.boby_book.entity.FindFabulousEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 发现评论点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-20
 */
@Mapper
public interface FindCommentFabulousDao extends BaseDao<FindCommentFabulousEntity> {
    /**
     * 根据评论id判断点赞状态
     *
     * @param userId
     * @param findCommentId
     * @return
     */
    @Select("SELECT COUNT(*) FROM e_find_comment_fabulous  WHERE  from_uid = #{from_uid} AND find_comment_id = #{find_comment_id}")
    Integer judgeFindCommentFabulousStatus(@Param("from_uid") Long userId, @Param("find_comment_id") Long findCommentId);

    /**
     * 根据userId和findCommentId查询点赞数据
     *
     * @param userId
     * @param findCommentId
     * @return
     */
    @Select("SELECT * FROM e_find_comment_fabulous  WHERE  from_uid = #{from_uid} AND find_comment_id = #{find_comment_id}")
    FindFabulousEntity selectFindCommentFabulousByUserIdAndCommentId(@Param("from_uid") Long userId, @Param("find_comment_id") Long findCommentId);
}