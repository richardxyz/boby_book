package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 销售属性
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xux_commodity_sales_attr")
public class CommoditySalesAttrEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 商品id
     */
	private Long commodityId;
    /**
     * 销售属性id
     */
	private Long saleAttrId;
    /**
     * 销售属性名称(冗余)
     */
	private String saleAttrName;
    /**
     * 更新者
     */
	private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;

	/**
	 * 销售属性值
	 */
	@TableField(exist = false)
	private List<CommoditySalesAttrValueEntity> spuSaleAttrValueList;

}