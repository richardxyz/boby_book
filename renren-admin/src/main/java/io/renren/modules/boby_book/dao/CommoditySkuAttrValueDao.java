package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommoditySkuAttrValueEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * sku平台属性值关联表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface CommoditySkuAttrValueDao extends BaseDao<CommoditySkuAttrValueEntity> {

    /**
     * 查询商品销售属性
     *
     * @param id 商品库存id
     * @return
     */
    @Select("SELECT * FROM e_commodity_sku_attr_value WHERE sku_id = #{sku_id}")
    List<CommoditySkuAttrValueEntity> selectSkuAttrValue(@Param("sku_id") Long id);
}