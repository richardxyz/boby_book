package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommodityBaseCatalogTwoDao;
import io.renren.modules.boby_book.dto.CommodityBaseCatalogTwoDTO;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogTwoEntity;
import io.renren.modules.boby_book.service.CommodityBaseCatalogTwoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 商品分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Service
@Transactional
public class CommodityBaseCatalogTwoServiceImpl extends CrudServiceImpl<CommodityBaseCatalogTwoDao, CommodityBaseCatalogTwoEntity, CommodityBaseCatalogTwoDTO> implements CommodityBaseCatalogTwoService {

    @Override
    public QueryWrapper<CommodityBaseCatalogTwoEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<CommodityBaseCatalogTwoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}