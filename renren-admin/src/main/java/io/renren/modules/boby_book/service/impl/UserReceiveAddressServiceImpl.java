package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.UserReceiveAddressDao;
import io.renren.modules.boby_book.dto.UserReceiveAddressDTO;
import io.renren.modules.boby_book.entity.UserReceiveAddressEntity;
import io.renren.modules.boby_book.service.UserReceiveAddressService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 用户信息详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class UserReceiveAddressServiceImpl extends CrudServiceImpl<UserReceiveAddressDao, UserReceiveAddressEntity, UserReceiveAddressDTO> implements UserReceiveAddressService {


    @Autowired
    private UserReceiveAddressDao userReceiveAddressDao;

    @Override
    public QueryWrapper<UserReceiveAddressEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<UserReceiveAddressEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;

    }


    @Override
    public UserReceiveAddressEntity selectReceiveAddressById(Long userid) {
        return userReceiveAddressDao.selectReceiveAddressById(userid);
    }

    @Override
    public List<UserReceiveAddressEntity> selectReceiveAddressListById(Long userid) {
        return userReceiveAddressDao.selectReceiveAddressListById(userid);
    }

    @Override
    public void addNewAddress(Long userid, String name, String phone, String postCode, String province, String city, String region, String detailAddress, Boolean defaultStatus) {
        UserReceiveAddressEntity userReceiveAddress2Entity = new UserReceiveAddressEntity();
        userReceiveAddress2Entity.setUserId(userid);
        userReceiveAddress2Entity.setName(name);
        userReceiveAddress2Entity.setPhoneNumber(phone);
        userReceiveAddress2Entity.setPostCode(postCode);
        userReceiveAddress2Entity.setProvince(province);
        userReceiveAddress2Entity.setCity(city);
        userReceiveAddress2Entity.setRegion(region);
        userReceiveAddress2Entity.setDetailAddress(detailAddress);
        Integer status;
        if (defaultStatus == true) {
            status = 1;
        } else {
            status = 0;
        }
        userReceiveAddress2Entity.setDefaultStatus(status);
        userReceiveAddressDao.insert(userReceiveAddress2Entity);
    }

    @Override
    public void updateAddress(Long addressid, Long userid, String name, String phone, String postCode, String province, String city, String region, String detailAddress, Boolean defaultStatus) {
        UserReceiveAddressEntity userReceiveAddress2Entity = userReceiveAddressDao.selectById(addressid);
        userReceiveAddress2Entity.setUserId(userid);
        userReceiveAddress2Entity.setName(name);
        userReceiveAddress2Entity.setPhoneNumber(phone);
        userReceiveAddress2Entity.setPostCode(postCode);
        userReceiveAddress2Entity.setProvince(province);
        userReceiveAddress2Entity.setCity(city);
        userReceiveAddress2Entity.setRegion(region);
        userReceiveAddress2Entity.setDetailAddress(detailAddress);
        Integer status;
        if (defaultStatus == true) {
            status = 1;
            userReceiveAddressDao.upDefaultStatusListById(addressid, userid);
        } else {
            status = 0;
        }
        userReceiveAddress2Entity.setDefaultStatus(status);
        userReceiveAddressDao.updateById(userReceiveAddress2Entity);
    }
}
