package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.BookChapterDTO;
import io.renren.modules.boby_book.entity.BookChapterEntity;

import java.util.List;

/**
 * 图书章节信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface BookChapterService extends CrudService<BookChapterEntity, BookChapterDTO> {
    /**
     * 查询所有书籍章节信息（正序）
     *
     * @param bookId 图书id
     * @return
     */
    List<BookChapterEntity> selectBookChapterOrderByASC(Long bookId);

    /**
     * 查询所有书籍章节信息（倒序）
     *
     * @param bookId 图书id
     * @return
     */
    List<BookChapterEntity> selectBookChapterOrderByDESC(Long bookId);
}