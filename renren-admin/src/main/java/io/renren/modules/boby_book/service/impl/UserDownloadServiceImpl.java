package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.UserDownloadDao;
import io.renren.modules.boby_book.dto.UserDownloadDTO;
import io.renren.modules.boby_book.entity.BookChapterEntity;
import io.renren.modules.boby_book.entity.BookEntity;
import io.renren.modules.boby_book.entity.UserDownloadEntity;
import io.renren.modules.boby_book.service.UserDownloadService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 用户下载
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class UserDownloadServiceImpl extends CrudServiceImpl<UserDownloadDao, UserDownloadEntity, UserDownloadDTO> implements UserDownloadService {

    @Override
    public QueryWrapper<UserDownloadEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<UserDownloadEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private UserDownloadDao eDownloadDao;

    @Override
    public List<BookEntity> selectDownloadBook(Long id) {
        return eDownloadDao.selectDownloadBook(id);
    }

    @Override
    public List<BookChapterEntity> selectDownloadBookChapter(Long bookid, Long userid) {
        return eDownloadDao.selectDownloadBookChapter(bookid,userid);
    }
}