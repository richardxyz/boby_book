package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.FindCommentReplyDTO;
import io.renren.modules.boby_book.entity.FindCommentReplyEntity;

import java.util.List;

/**
 * 发现评论回复
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface FindCommentReplyService extends CrudService<FindCommentReplyEntity, FindCommentReplyDTO> {

    /**
     * 添加评论回复
     *
     * @param cid     目标评论id
     * @param fuid    回复用户id
     * @param tuid    目标用户id
     * @param content 评论内容
     * @param ip      回复用户ip
     * @return
     */
    void addFindCommentReply(Long cid, Long fuid, Long tuid, String content, String ip);

    /**
     * 根据id查询评论回复信息
     *
     * @param id 评论id
     * @return
     */
    List<FindCommentReplyEntity> selectFindCommentReplyByCommentId(Long id);
}