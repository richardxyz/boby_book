package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 兑换礼品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_commodity_gift")
public class CommodityGiftEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 商品skuId
     */
    private Long skuId;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 兑换数量
     */
    private Integer exchangeNum;
    /**
     * 兑换时间
     */
    private Date exchangeTime;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;

    /**
     * 商品信息
     */
    @TableField(exist = false)
    CommoditySkuInfoEntity commoditySkuInfo;


}