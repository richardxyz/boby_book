package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.FindEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 发现数据
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface FindDao extends BaseDao<FindEntity> {

    /**
     * 查询当前用户视频
     *
     * @param userId 用户id
     * @return
     */
    @Select("SELECT * FROM e_find WHERE user_id = #{user_id}")
    List<FindEntity> selectFindByUserId(@Param("user_id") Long userId);

    List<FindEntity> selectFindAll();

}