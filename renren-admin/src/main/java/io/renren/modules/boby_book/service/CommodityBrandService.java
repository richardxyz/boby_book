package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommodityBrandDTO;
import io.renren.modules.boby_book.entity.CommodityBrandEntity;

/**
 * 商品品牌
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommodityBrandService extends CrudService<CommodityBrandEntity, CommodityBrandDTO> {

}