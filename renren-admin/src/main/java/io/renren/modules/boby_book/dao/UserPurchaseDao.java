package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.BookEntity;
import io.renren.modules.boby_book.entity.BookChapterEntity;
import io.renren.modules.boby_book.entity.UserPurchaseEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户购买
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserPurchaseDao extends BaseDao<UserPurchaseEntity> {
    /**
     * 查询我已购买的书籍
     *
     * @param id 用户id
     * @return
     */
    List<BookEntity> selectPurchaseBook(@Param("user_id") Long id);

    /**
     * 查询我已购买的书籍的章节
     *
     * @param bookid 图书id
     * @param userid 用户id
     * @return
     */
    @Select("SELECT * FROM e_book_chapter WHERE book_id = #{book_id} AND id IN(SELECT book_chapter_id FROM e_user_purchase WHERE user_id = #{user_id} AND book_id= #{book_id})")
    List<BookChapterEntity> selectPurchaseBookChapter(@Param("book_id") Long bookid, @Param("user_id") Long userid);

}