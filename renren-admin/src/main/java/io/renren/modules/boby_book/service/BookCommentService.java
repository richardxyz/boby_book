package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.BookCommentDTO;
import io.renren.modules.boby_book.entity.BookCommentEntity;

import java.util.List;

/**
 * 图书章节评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface BookCommentService extends CrudService<BookCommentEntity, BookCommentDTO> {
    /**
     * 添加评论
     *
     * @param bookId  图书id
     * @param fuid    评论用户id
     * @param content 评论内容
     * @param ip      评论用户ip
     */
    void addComment(Long bookId, Long fuid, String content, String ip);

    /**
     * 根据id查询图书评论信息
     *
     * @param bookId 图书id
     * @return
     */
    List<BookCommentEntity> selectCommentByBookId(Long bookId);

    /**
     * 根据评论id查询评论信息
     *
     * @param id 评论id
     * @return
     */
    BookCommentEntity selectCommentById(Long id);
}