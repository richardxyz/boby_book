package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.UserSpecificInformationDao;
import io.renren.modules.boby_book.dto.UserSpecificInformationDTO;
import io.renren.modules.boby_book.entity.UserSpecificInformationEntity;
import io.renren.modules.boby_book.service.UserSpecificInformationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 用户信息详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class UserSpecificInformationServiceImpl extends CrudServiceImpl<UserSpecificInformationDao, UserSpecificInformationEntity, UserSpecificInformationDTO> implements UserSpecificInformationService {


    @Autowired
    private UserSpecificInformationDao eUserSpecificInformationDao;

    @Override
    public QueryWrapper<UserSpecificInformationEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<UserSpecificInformationEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Override
    public void Register(Long id) {
        UserSpecificInformationEntity entity = new UserSpecificInformationEntity();
        entity.setUserId(id);//用户id（外键）
        eUserSpecificInformationDao.insert(entity);
    }

    @Override
    public UserSpecificInformationEntity selectUserSpecificInformationById(Long userId) {
        return eUserSpecificInformationDao.selectUserSpecificInformationById(userId);
    }


    @Override
    public void updateIntegralNum(Integer integralNum, Long userId) {
        eUserSpecificInformationDao.updateIntegralNum(integralNum, userId);
    }

}