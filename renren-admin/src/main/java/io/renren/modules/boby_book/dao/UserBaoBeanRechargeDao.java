package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.UserBaoBeanRechargeEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 宝豆充值
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserBaoBeanRechargeDao extends BaseDao<UserBaoBeanRechargeEntity> {
	
}