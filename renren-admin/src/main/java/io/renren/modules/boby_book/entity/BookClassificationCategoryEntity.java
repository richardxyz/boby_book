package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 书籍分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xux_book_classification_category")
public class BookClassificationCategoryEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 分类id
     */
	private Long classification_id;
    /**
     * 类别id
     */
	private Long category_id;
    /**
     * 分类信息
     */
	private String classification_val;
    /**
     * 类别信息
     */
	private String category_val;
    /**
     * 更新者
     */
	private Long updater;
    /**
     * 更新时间
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;

}