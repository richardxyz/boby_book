package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserFollowDTO;
import io.renren.modules.boby_book.entity.UserFollowEntity;

import java.util.List;

/**
 * 用户关注
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserFollowService extends CrudService<UserFollowEntity, UserFollowDTO> {

    /**
     * 查询用户所有关注信息
     *
     * @param userId 用户id
     * @return
     */
    List<UserFollowEntity> selectFollow(Long userId);
}