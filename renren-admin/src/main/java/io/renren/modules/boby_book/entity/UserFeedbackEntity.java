package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 意见反馈
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_user_feedback")
public class UserFeedbackEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 反馈内容描述
     */
    private String feedbackContent;
    /**
     * 反馈图片
     */
    private String feedbackImage;
    /**
     * 反馈时间
     */
    private Date feedbackTime;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 用户ip
     */
    private String userIp;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;
}