package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 商品品牌
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xux_commodity_brand")
public class CommodityBrandEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

    /**
     * 品牌名称
     */
	private String name;
    /**
     * 首字母
     */
	private String firstLetter;
    /**
     * 分类
     */
	private Integer sort;
    /**
     * 是否为品牌制造商：0->不是；1->是
     */
	private Integer factoryStatus;
    /**
     * 显示状态
     */
	private Integer showStatus;
    /**
     * 产品数量
     */
	private Integer productCount;
    /**
     * 产品评论数量
     */
	private Integer productCommentCount;
    /**
     * 产品logo
     */
	private String logo;
    /**
     * 专区大图
     */
	private String bigPic;
    /**
     * 品牌故事
     */
	private String brandStory;
    /**
     * 更新者
     */
	private Long updater;
    /**
     * 更新时间
     */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;
}