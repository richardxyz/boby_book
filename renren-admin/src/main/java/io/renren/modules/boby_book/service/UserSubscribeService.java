package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserSubscribeDTO;
import io.renren.modules.boby_book.entity.UserSubscribeEntity;

/**
 * 用户订阅
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserSubscribeService extends CrudService<UserSubscribeEntity, UserSubscribeDTO> {

}