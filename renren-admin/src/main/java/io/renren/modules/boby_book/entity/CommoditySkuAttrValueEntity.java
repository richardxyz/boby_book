package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * sku平台属性值关联表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_commodity_sku_attr_value")
public class CommoditySkuAttrValueEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 属性id（冗余)
     */
    private Long attrOd;
    /**
     * 属性值id
     */
    private Long valueId;
    /**
     * skuid
     */
    private Long skuId;
    /**
     * 销售属性名称
     */
    private String attrName;
    /**
     * 销售属性值名称
     */
    private String attrValueName;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;
}