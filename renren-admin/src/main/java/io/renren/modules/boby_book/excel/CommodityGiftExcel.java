package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 兑换礼品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommodityGiftExcel {
    @Excel(name = "主键id")
    private Long id;
    @Excel(name = "商品skuId")
    private Long skuId;
    @Excel(name = "用户id")
    private Long userId;
    @Excel(name = "兑换数量")
    private Integer exchangeNum;
    @Excel(name = "兑换时间")
    private Date exchangeTime;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}