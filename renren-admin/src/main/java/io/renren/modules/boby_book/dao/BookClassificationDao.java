package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.BookClassificationEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 书籍分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface BookClassificationDao extends BaseDao<BookClassificationEntity> {
    /**
     * 查询所有书分类
     *
     * @return
     */
    List<BookClassificationEntity> findAllClassification();
}
