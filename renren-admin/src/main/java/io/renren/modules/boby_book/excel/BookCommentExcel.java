package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 图书评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class BookCommentExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "书籍id")
    private String bookId;
    @Excel(name = "评论用户IP")
    private String userIp;
    @Excel(name = "评论内容")
    private String commentContent;
    @Excel(name = "评论时间")
    private Date commentTime;
    @Excel(name = "用户id")
    private Long fromUid;
    @Excel(name = "回复次数")
    private Integer replayCount;
    @Excel(name = "点赞次数")
    private Integer fabulousCount;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}