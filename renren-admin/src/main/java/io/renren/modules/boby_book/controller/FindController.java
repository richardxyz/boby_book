package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.FindDTO;
import io.renren.modules.boby_book.entity.FindEntity;
import io.renren.modules.boby_book.excel.FindExcel;
import io.renren.modules.boby_book.service.FindService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 发现数据
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/find")
@Api(tags = "发现数据")
public class FindController {

    @Autowired
    private FindService findService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:find:page")
    public Result<PageData<FindDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<FindDTO> page = findService.page(params);

        return new Result<PageData<FindDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:find:info")
    public Result<FindDTO> get(@PathVariable("id") Long id) {
        FindDTO data = findService.get(id);

        return new Result<FindDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:find:save")
    public Result save(@RequestBody FindDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        findService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:find:update")
    public Result update(@RequestBody FindDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        findService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:find:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        findService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:find:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<FindDTO> list = findService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, FindExcel.class);
    }

    /**
     * 查询所有发现数据
     *
     * @return
     */
    @PostMapping("/selectFindAll")
    public Result selectFind() {
        List<FindEntity> find2Entity = findService.selectFindAll();
        return new Result().ok(find2Entity);

    }

    /**
     * 查询当前用户视频
     *
     * @param userId 用户id
     * @return
     */
    @PostMapping("/selectFindByUserId")
    public Result selectFindByUserId(Long userId) {
        List<FindEntity> find2Entity = findService.selectFindByUserId(userId);
        return new Result().ok(find2Entity);

    }

    /**
     * 根据id查询发现数据
     *
     * @param findId 发现数据id
     * @return
     */
    @PostMapping("/selectFindById")
    public Result selectFindById(Long findId) {
        FindEntity find2Entity = findService.selectById(findId);
        return new Result().ok(find2Entity);

    }


}