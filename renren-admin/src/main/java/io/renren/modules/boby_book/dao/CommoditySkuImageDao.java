package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommoditySkuImageEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 商品图片表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface CommoditySkuImageDao extends BaseDao<CommoditySkuImageEntity> {


    /**
     * 查询商品图片
     *
     * @param id 商品库存id
     * @return
     */
    @Select("SELECT * FROM e_commodity_sku_image WHERE sku_id = #{sku_id}")
    List<CommoditySkuImageEntity> selectImageById(@Param("sku_id") Long id);

}