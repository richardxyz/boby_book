package io.renren.modules.boby_book.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 发现评论点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-20
 */
@Data
@ApiModel(value = "发现评论点赞表")
public class FindCommentFabulousDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private Long id;

	@ApiModelProperty(value = "评论id")
	private Long findCommentId;

	@ApiModelProperty(value = "评论用户IP")
	private String fromUip;

	@ApiModelProperty(value = "用户id")
	private Long fromUid;

	@ApiModelProperty(value = "更新者")
	private Long updater;

	@ApiModelProperty(value = "更新时间")
	private Date updateDate;

	@ApiModelProperty(value = "创建者")
	private Long creator;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;


}