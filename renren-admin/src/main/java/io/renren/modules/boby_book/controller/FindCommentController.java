package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.IpUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.FindCommentDTO;
import io.renren.modules.boby_book.entity.FindCommentEntity;
import io.renren.modules.boby_book.entity.FindCommentReplyEntity;
import io.renren.modules.boby_book.excel.FindCommentExcel;
import io.renren.modules.boby_book.service.FindCommentReplyService;
import io.renren.modules.boby_book.service.FindCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 发现数据评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/findcomment")
@Api(tags = "发现数据评论")
public class FindCommentController {

    @Autowired
    private FindCommentService findCommentService;
    @Autowired
    private FindCommentReplyService findCommentReplyService;

    @Autowired
    private IpUtils ipUtils;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:findcomment:page")
    public Result<PageData<FindCommentDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<FindCommentDTO> page = findCommentService.page(params);

        return new Result<PageData<FindCommentDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:findcomment:info")
    public Result<FindCommentDTO> get(@PathVariable("id") Long id) {
        FindCommentDTO data = findCommentService.get(id);

        return new Result<FindCommentDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:findcomment:save")
    public Result save(@RequestBody FindCommentDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        findCommentService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:findcomment:update")
    public Result update(@RequestBody FindCommentDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        findCommentService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:findcomment:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        findCommentService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:findcomment:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<FindCommentDTO> list = findCommentService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, FindCommentExcel.class);
    }

    /**
     * 根据id查询发现评论信息
     *
     * @param findId 发现数据id
     * @return
     */
    @PostMapping("/selectCommentByFindId")
    public Result selectCommentByFindId(Long findId) {
        System.out.println("findId：" + findId);
        List<FindCommentEntity> entityList = findCommentService.selectCommentByFindId(findId);
        return new Result().ok(entityList);
    }


    /**
     * 添加评论
     *
     * @param findId  发现数据id
     * @param fuid    评论用户id
     * @param content 评论内容
     * @param request
     * @return
     */
    @PostMapping("/addFindComment")
    public Result addFindComment(Long findId, Long fuid, String content, HttpServletRequest request) {
        String ip = ipUtils.getIpAddr(request);
        findCommentService.addFindComment(findId, fuid, content, ip);
        return new Result().ok("评论成功");
    }

    /**
     * 添加评论回复
     *
     * @param cid     目标评论id
     * @param fuid    回复用户id
     * @param tuid    目标用户id
     * @param content 评论内容
     * @param request
     * @return
     */
    @PostMapping("/addFindCommentReply")
    public Result addFindCommentReply(Long cid, Long fuid, Long tuid, String content, HttpServletRequest request) {
        String ip = ipUtils.getIpAddr(request);
        findCommentReplyService.addFindCommentReply(cid, fuid, tuid, content, ip);
        return new Result().ok("评论回复成功");
    }

    /**
     * 根据评论id查询评论信息
     *
     * @param id 评论id
     * @return
     */
    @PostMapping("/selectFindCommentById")
    public Result selectFindCommentById(Long id) {
        System.out.println("id：" + id);
        FindCommentEntity entity = findCommentService.selectFindCommentById(id);
        return new Result().ok(entity);
    }

    /**
     * 根据id查询评论回复信息
     *
     * @param id 评论id
     * @return
     */
    @PostMapping("/selectFindCommentReplyByCommentId")
    public Result selectFindCommentReplyByCommentId(Long id) {
        List<FindCommentReplyEntity> entityList = findCommentReplyService.selectFindCommentReplyByCommentId(id);
        return new Result().ok(entityList);
    }
}