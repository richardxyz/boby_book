package io.renren.modules.boby_book.dto;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


/**
 * 图书信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "图书信息")
public class BookDTO extends Model<BookDTO> {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "图书封面")
    private String bookCover;

    @ApiModelProperty(value = "图书名")
    private String bookName;

    @ApiModelProperty(value = "图书作者")
    private String bookAuthor;

    @ApiModelProperty(value = "图书简介")
    private String bookSynopsis;

    @ApiModelProperty(value = "类别")
    private Long bookCategory;

    @ApiModelProperty(value = "图书章节数量")
    private Integer bookChapterNum;

    @ApiModelProperty(value = "图书订阅数量")
    private Integer bookSubscribeNum;

    @ApiModelProperty(value = "图书播放数量")
    private Integer bookPlayNum;

    @ApiModelProperty(value = "图书收藏数量")
    private Integer bookCollectionNum;

    @ApiModelProperty(value = "图书评论数量")
    private Integer bookCommentNum;

    @ApiModelProperty(value = "图书点赞数量")
    private Integer bookFabulousNum;

    @ApiModelProperty(value = "评分")
    private Integer score;

    @ApiModelProperty(value = "上架时间")
    private Date shelfTime;

    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;


}