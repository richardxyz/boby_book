package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommodityBaseSaleAttrDTO;
import io.renren.modules.boby_book.entity.CommodityBaseSaleAttrEntity;

/**
 * 销售属性名称
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommodityBaseSaleAttrService extends CrudService<CommodityBaseSaleAttrEntity, CommodityBaseSaleAttrDTO> {

}