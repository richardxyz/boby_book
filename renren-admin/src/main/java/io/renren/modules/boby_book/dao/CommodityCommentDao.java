package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommodityCommentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 商品评价
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface CommodityCommentDao extends BaseDao<CommodityCommentEntity> {
    /**
     * 根据id查询商品评价信息
     *
     * @param id 商品id
     * @return
     */
    @Select("SELECT * FROM e_commodity_comment WHERE commodity_id = #{commodity_id}")
    List<CommodityCommentEntity> selectCommentById(@Param("commodity_id") Long id);

    /**
     * 查询最新商品评价信息
     *
     * @param id 商品id
     * @return
     */
    List<CommodityCommentEntity> selectCommentList(@Param("commodity_id") Long id);
}