package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommoditySalesAttrEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 销售属性
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface CommoditySalesAttrDao extends BaseDao<CommoditySalesAttrEntity> {

    /**
     * 销售属性列表
     *
     * @param commodityId 商品id
     * @param skuId       商品库存id
     * @return
     */
    List<CommoditySalesAttrEntity> selectSpuSaleAttrListCheckBySku(@Param("commodity_id") Long commodityId, @Param("sku_id") Long skuId);

}