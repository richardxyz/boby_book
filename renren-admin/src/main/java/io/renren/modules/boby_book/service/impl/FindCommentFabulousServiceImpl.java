package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.IpUtils;
import io.renren.modules.boby_book.dao.FindCommentFabulousDao;
import io.renren.modules.boby_book.dto.FindCommentFabulousDTO;
import io.renren.modules.boby_book.entity.FindCommentEntity;
import io.renren.modules.boby_book.entity.FindCommentFabulousEntity;
import io.renren.modules.boby_book.entity.FindFabulousEntity;
import io.renren.modules.boby_book.service.FindCommentFabulousService;
import io.renren.modules.boby_book.service.FindCommentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 发现评论点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-1-0
 */
@Service
public class FindCommentFabulousServiceImpl extends CrudServiceImpl<FindCommentFabulousDao, FindCommentFabulousEntity, FindCommentFabulousDTO> implements FindCommentFabulousService {

    @Override
    public QueryWrapper<FindCommentFabulousEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<FindCommentFabulousEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Autowired
    private FindCommentFabulousDao findCommentFabulousDao;

    @Autowired
    private IpUtils ipUtils;

    @Autowired
    private FindCommentService findCommentService;

    @Override
    public Integer judgeFindCommentFabulousStatus(Long userId, Long findCommentId) {
        return findCommentFabulousDao.judgeFindCommentFabulousStatus(userId, findCommentId);
    }

    @Override
    public void cancelFindCommentFabulous(Long userId, Long findCommentId) {
        FindFabulousEntity findFabulousEntity = findCommentFabulousDao.selectFindCommentFabulousByUserIdAndCommentId(userId, findCommentId);
        findCommentFabulousDao.deleteById(findFabulousEntity.getId());
        FindCommentEntity entity = findCommentService.selectById(findCommentId);
        Integer fabulousCount = entity.getFabulousCount();
        fabulousCount--;
        entity.setFabulousCount(fabulousCount);
        findCommentService.updateById(entity);
    }

    @Override
    public void addFindCommentFabulous(Long userId, Long findCommentId, HttpServletRequest request) {
        String ip = ipUtils.getIpAddr(request);
        FindCommentEntity entity = findCommentService.selectById(findCommentId);
        Integer fabulousCount = entity.getFabulousCount();
        fabulousCount++;
        entity.setFabulousCount(fabulousCount);
        findCommentService.updateById(entity);
        FindCommentFabulousEntity findCommentFabulousEntity = new FindCommentFabulousEntity();
        findCommentFabulousEntity.setFindCommentId(findCommentId);
        findCommentFabulousEntity.setFromUid(userId);
        findCommentFabulousEntity.setFromUip(ip);
        findCommentFabulousDao.insert(findCommentFabulousEntity);
    }
}