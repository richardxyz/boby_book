package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommoditySkuAttrValueDao;
import io.renren.modules.boby_book.dto.CommoditySkuAttrValueDTO;
import io.renren.modules.boby_book.entity.CommoditySkuAttrValueEntity;
import io.renren.modules.boby_book.service.CommoditySkuAttrValueService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * sku平台属性值关联表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-11-11
 */
@Service
@Transactional
public class CommoditySkuAttrValueServiceImpl extends CrudServiceImpl<CommoditySkuAttrValueDao, CommoditySkuAttrValueEntity, CommoditySkuAttrValueDTO> implements CommoditySkuAttrValueService {

    @Override
    public QueryWrapper<CommoditySkuAttrValueEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<CommoditySkuAttrValueEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}