package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.FindCommentReplyEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 发现评论回复
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface FindCommentReplyDao extends BaseDao<FindCommentReplyEntity> {

    /**
     * 根据id查询评论回复信息
     *
     * @param id 评论id
     * @return
     */
    List<FindCommentReplyEntity> selectFindCommentReplyByCommentId(Long id);
}
