package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 商品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommoditySkuInfoExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "商品id")
    private Long commodityId;
    @Excel(name = "sku名称")
    private String skuName;
    @Excel(name = "商品详情")
    private String skuDetails;
    @Excel(name = "积分")
    private Integer integral;
    @Excel(name = "市场参考价")
    private Double price;
    @Excel(name = "品牌id(冗余)")
    private Long brandId;
    @Excel(name = "分类id（冗余)")
    private Long catalogId;
    @Excel(name = "默认显示图片(冗余)")
    private String skuDefaultImg;
    @Excel(name = "已兑换个数")
    private Integer exchangeNum;
    @Excel(name = "库存个数")
    private Integer stockNum;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}