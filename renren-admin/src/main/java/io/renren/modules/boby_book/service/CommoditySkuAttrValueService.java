package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommoditySkuAttrValueDTO;
import io.renren.modules.boby_book.entity.CommoditySkuAttrValueEntity;

/**
 * sku平台属性值关联表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommoditySkuAttrValueService extends CrudService<CommoditySkuAttrValueEntity, CommoditySkuAttrValueDTO> {

}