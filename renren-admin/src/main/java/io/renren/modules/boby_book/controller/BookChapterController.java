package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.BookChapterDTO;
import io.renren.modules.boby_book.entity.BookChapterEntity;
import io.renren.modules.boby_book.excel.BookChapterExcel;
import io.renren.modules.boby_book.service.BookChapterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 图书章节信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/bookchapter")
@Api(tags = "图书章节信息")
public class BookChapterController {

    @Autowired
    private BookChapterService chapterService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:bookchapter:page")
    public Result<PageData<BookChapterDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<BookChapterDTO> page = chapterService.page(params);
        return new Result<PageData<BookChapterDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:bookchapter:info")
    public Result<BookChapterDTO> get(@PathVariable("id") Long id) {
        BookChapterDTO data = chapterService.get(id);
        return new Result<BookChapterDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:bookchapter:save")
    public Result save(@RequestBody BookChapterDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);
        chapterService.save(dto);
        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:bookchapter:update")
    public Result update(@RequestBody BookChapterDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);
        chapterService.update(dto);
        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:bookchapter:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");
        chapterService.delete(ids);
        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:bookchapter:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<BookChapterDTO> list = chapterService.list(params);
        ExcelUtils.exportExcelToTarget(response, null, list, BookChapterExcel.class);
    }

    /**
     * 查询所有书籍章节信息（正序）
     *
     * @param bookId 图书id
     * @return
     */
    @PostMapping("/selectBookChapterOrderByASC")
    public Result selectBookChapterOrderByASC(Long bookId) {
        List<BookChapterEntity> list = chapterService.selectBookChapterOrderByASC(bookId);
        return new Result().ok(list);
    }

    /**
     * 查询所有书籍章节信息（倒序）
     *
     * @param bookId 图书id
     * @return
     */
    @PostMapping("/selectBookChapterOrderByDESC")
    public Result selectBookChapterOrderByDESC(Long bookId) {
        List<BookChapterEntity> list = chapterService.selectBookChapterOrderByDESC(bookId);
        return new Result().ok(list);
    }
}