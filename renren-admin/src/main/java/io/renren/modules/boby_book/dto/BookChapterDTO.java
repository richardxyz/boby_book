package io.renren.modules.boby_book.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 图书章节信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@ApiModel(value = "图书章节信息")
public class BookChapterDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = " 图书名称")
    private String bookName;

    @ApiModelProperty(value = "图书章节名称")
    private String chapterName;

    @ApiModelProperty(value = "图书章节图片")
    private String chapterImg;

    @ApiModelProperty(value = "图书章节内容")
    private String chapterContent;

    @ApiModelProperty(value = "图书播放数量")
    private Integer bookPlayNums;

    @ApiModelProperty(value = "图书收藏数量")
    private Integer bookCollectionNum;

    @ApiModelProperty(value = "图书评论数量")
    private Integer bookCommentNum;

    @ApiModelProperty(value = "书籍id")
    private Long bookId;

    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;


}