package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserFeedbackDTO;
import io.renren.modules.boby_book.entity.UserFeedbackEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * 意见反馈
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserFeedbackService extends CrudService<UserFeedbackEntity, UserFeedbackDTO> {
    /**
     * 添加意见反馈
     *
     * @param userId          用户id
     * @param feedbackContent 反馈内容
     */
    void addFeedback(Long userId, String feedbackContent, HttpServletRequest request);

    /**
     * 添加意见反馈截图
     *
     * @param filePath
     * @param userId
     */
    void uploadFeedbackImg(String filePath, Long userId);
}