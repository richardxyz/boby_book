package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserPurchaseDTO;
import io.renren.modules.boby_book.entity.BookChapterEntity;
import io.renren.modules.boby_book.entity.BookEntity;
import io.renren.modules.boby_book.entity.UserPurchaseEntity;

import java.util.List;

/**
 * 用户购买
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserPurchaseService extends CrudService<UserPurchaseEntity, UserPurchaseDTO> {
    /**
     * 查询我已购买的书籍
     *
     * @param id 用户id
     * @return
     */
    List<BookEntity> selectPurchaseBook(Long id);

    /**
     * 查询我已购买的书籍的章节
     *
     * @param bookid 图书id
     * @param userid 用户id
     * @return
     */
    List<BookChapterEntity> selectPurchaseBookChapter(Long bookid, Long userid);
}