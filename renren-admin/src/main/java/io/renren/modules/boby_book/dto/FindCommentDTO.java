package io.renren.modules.boby_book.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 发现数据评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@ApiModel(value = "发现数据评论")
public class FindCommentDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private Long id;

	@ApiModelProperty(value = "发现数据id")
	private Long findId;

	@ApiModelProperty(value = "用户id(评论者)")
	private Long userId;

	@ApiModelProperty(value = "评论内容")
	private String findComment;

	@ApiModelProperty(value = "评论时间")
	private Date findCommentTime;

	@ApiModelProperty(value = "用户ip(评论者)")
	private String userIp;

	@ApiModelProperty(value = "回复次数")
	private Integer replayCount;

	@ApiModelProperty(value = "点赞次数")
	private Integer fabulousCount;

	@ApiModelProperty(value = "更新者")
	private Long updater;

	@ApiModelProperty(value = "更新时间")
	private Date updateDate;


}