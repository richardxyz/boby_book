package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommoditySkuInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 商品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface CommoditySkuInfoDao extends BaseDao<CommoditySkuInfoEntity> {
    /**
     * 根据商品id获取所有库存信息
     *
     * @param commodityId 商品id
     * @return
     */
    List<CommoditySkuInfoEntity> selectSkuSaleAttrValueListBySpu(@Param("commodity_id") Long commodityId);

    /**
     * 根据分类id查询商品
     *
     * @param commodityId 商品id
     * @return
     */
    @Select("SELECT * FROM e_commodity_sku_info WHERE commodity_id = #{commodity_id}")
    List<CommoditySkuInfoEntity> findCommodityBycommodityId(@Param("commodity_id") Long commodityId);


    /**
     * 修改库存兑换数量信息
     *
     * @param exchangeNum 以兑换数量
     * @param id          商品库存id
     */
    @Update("UPDATE e_commodity_sku_info SET exchange_num = #{exchange_num} WHERE id = #{id}")
    void updateExchangeNumById(@Param("exchange_num") Integer exchangeNum, @Param("id") Long id);
}