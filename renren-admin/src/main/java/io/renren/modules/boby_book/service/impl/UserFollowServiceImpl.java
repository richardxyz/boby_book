package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.UserFollowDao;
import io.renren.modules.boby_book.dto.UserFollowDTO;
import io.renren.modules.boby_book.entity.UserFollowEntity;
import io.renren.modules.boby_book.service.UserFollowService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 用户关注
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class UserFollowServiceImpl extends CrudServiceImpl<UserFollowDao, UserFollowEntity, UserFollowDTO> implements UserFollowService {

    @Override
    public QueryWrapper<UserFollowEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<UserFollowEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private UserFollowDao followDao;

    @Override
    public List<UserFollowEntity> selectFollow(Long userId) {
        return followDao.selectFollow(userId);
    }
}