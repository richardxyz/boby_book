package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.UserSignInEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;

/**
 * 用户签到表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserSignInDao extends BaseDao<UserSignInEntity> {
    /**
     * 获取最后一次签到的日期
     *
     * @param userid 用户id
     * @return
     */
    @Select("SELECT attendance_time FROM e_user_sign_in WHERE user_id = #{user_id} ORDER BY id DESC LIMIT 0,1;")
    Date lastSignIn(@Param("user_id") Long userid);
}