package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.UserCollectionEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户收藏
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserCollectionDao extends BaseDao<UserCollectionEntity> {

    /**
     * 查询我的收藏
     *
     * @param id 用户id
     * @return
     */
    List<UserCollectionEntity> selectCollectionBook(@Param("user_id") Long id);
}