package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 用户信息详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class UserSpecificInformationExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "金币积分")
    private Integer integral;
    @Excel(name = "宝豆")
    private Double baoBean;
    @Excel(name = "下载图书")
    private Integer downloadNum;
    @Excel(name = "购买图书")
    private Integer purchaseNum;
    @Excel(name = "收藏图书")
    private Integer collectionNum;
    @Excel(name = "订阅图书")
    private Integer subscribeNum;
    @Excel(name = "视频播放量")
    private Integer playVideoNum;
    @Excel(name = "视频数量")
    private Integer videoNum;
    @Excel(name = "粉丝(人)")
    private Integer fansNum;
    @Excel(name = "关注(人)")
    private Integer followNum;
    @Excel(name = "用户id(外键)")
    private Long userId;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}