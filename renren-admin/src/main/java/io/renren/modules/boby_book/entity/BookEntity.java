package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

/**
 * 图书信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@TableName("xux_book")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "图书信息")
public class BookEntity extends Model<BookEntity> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 图书封面
     */
    @ApiModelProperty(value = "图书封面")
    private String bookCover;
    /**
     * 图书名
     */
    @ApiModelProperty(value = "图书名")
    private String bookName;
    /**
     * 图书作者
     */
    @ApiModelProperty(value = "图书作者")
    private String bookAuthor;
    /**
     * 图书简介
     */
    @ApiModelProperty(value = "图书简介")
    private String bookSynopsis;
    /**
     * 类别
     */
    @ApiModelProperty(value = "类别")
    private Long bookCategory;
    /**
     * 图书章节数量
     */
    @ApiModelProperty(value = "图书章节数量")
    private Integer bookChapterNum;
    /**
     * 图书订阅数量
     */
    @ApiModelProperty(value = "图书订阅数量")
    private Integer bookSubscribeNum;
    /**
     * 图书播放数量
     */
    @ApiModelProperty(value = "图书播放数量")
    private Integer bookPlayNum;
    /**
     * 图书收藏数量
     */
    @ApiModelProperty(value = "图书收藏数量")
    private Integer bookCollectionNum;
    /**
     * 图书评论数量
     */
    @ApiModelProperty(value = "图书评论数量")
    private Integer bookCommentNum;
    /**
     * 图书点赞数量
     */
    @ApiModelProperty(value = "图书点赞数量")
    private Integer bookFabulousNum;
    /**
     * 评分
     */
    @ApiModelProperty(value = "评分")
    private Integer score;
    /**
     * 上架时间
     */
    @ApiModelProperty(value = "上架时间")
    private Date shelfTime;
    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    private Long updater;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;

    @ApiModelProperty(value = "创建者")
    @TableField(fill = FieldFill.INSERT)
    private Long creator;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;
    /**
     * 图书章节信息
     */
    @TableField(exist = false)
    List<BookChapterEntity> chapterList;
}