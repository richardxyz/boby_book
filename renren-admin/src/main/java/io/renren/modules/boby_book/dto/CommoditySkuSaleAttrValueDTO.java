package io.renren.modules.boby_book.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * sku销售属性值
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Data
@ApiModel(value = "sku销售属性值")
public class CommoditySkuSaleAttrValueDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "库存单元id")
    private Long skuId;

    @ApiModelProperty(value = "销售属性id（冗余)")
    private Long saleAttrId;

    @ApiModelProperty(value = "销售属性值id")
    private Long saleAttrValueId;

    @ApiModelProperty(value = "销售属性名称(冗余)")
    private String saleAttrName;

    @ApiModelProperty(value = "销售属性值名称(冗余)")
    private String saleAttrValueName;

    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;
}
