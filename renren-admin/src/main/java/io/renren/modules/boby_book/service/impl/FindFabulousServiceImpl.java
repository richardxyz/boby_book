package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.IpUtils;
import io.renren.modules.boby_book.dao.FindFabulousDao;
import io.renren.modules.boby_book.dto.FindFabulousDTO;
import io.renren.modules.boby_book.entity.FindEntity;
import io.renren.modules.boby_book.entity.FindFabulousEntity;
import io.renren.modules.boby_book.service.FindFabulousService;
import io.renren.modules.boby_book.service.FindService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 发现点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-20
 */
@Service
public class FindFabulousServiceImpl extends CrudServiceImpl<FindFabulousDao, FindFabulousEntity,FindFabulousDTO> implements FindFabulousService {

    @Override
    public QueryWrapper<FindFabulousEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<FindFabulousEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private FindFabulousDao findFabulousDao;

    @Autowired
    private IpUtils ipUtils;

    @Autowired
    private FindService findService;

    @Override
    public Integer judgeFindFabulousStatus(Long userId, Long findId) {
        return findFabulousDao.judgeFindFabulousStatus(userId, findId);
    }

    @Override
    public void cancelFindFabulous(Long userId, Long findId) {
        FindFabulousEntity findFabulous2Entity = findFabulousDao.selectFindFabulousByUserIdAndCommentId(userId, findId);
        findFabulousDao.deleteById(findFabulous2Entity.getId());
        FindEntity entity = findService.selectById(findId);
        Integer fabulousCount = entity.getFabulousNum();
        fabulousCount--;
        entity.setFabulousNum(fabulousCount);
        findService.updateById(entity);
    }

    @Override
    public void addFindFabulous(Long userId, Long findId, HttpServletRequest request) {
        String ip = ipUtils.getIpAddr(request);
        FindEntity entity = findService.selectById(findId);
        Integer fabulousCount = entity.getFabulousNum();
        fabulousCount++;
        entity.setFabulousNum(fabulousCount);
        findService.updateById(entity);
        FindFabulousEntity findFabulous2Entity = new FindFabulousEntity();
        findFabulous2Entity.setFindId(findId);
        findFabulous2Entity.setFromUid(userId);
        findFabulous2Entity.setFromUip(ip);
        findFabulousDao.insert(findFabulous2Entity);
    }
}