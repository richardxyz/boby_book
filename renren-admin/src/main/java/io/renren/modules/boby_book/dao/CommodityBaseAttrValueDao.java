package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommodityBaseAttrValueEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性值表(用作于查询)
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface CommodityBaseAttrValueDao extends BaseDao<CommodityBaseAttrValueEntity> {
	
}