package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 发现数据评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class FindCommentExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "发现数据id")
    private Long findId;
    @Excel(name = "用户id(评论者)")
    private Long userId;
    @Excel(name = "评论内容")
    private String findComment;
    @Excel(name = "评论时间")
    private String findCommentTime;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}