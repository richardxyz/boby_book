package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.UserSpecificInformationEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * 用户信息详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserSpecificInformationDao extends BaseDao<UserSpecificInformationEntity> {
    /**
     * 根据用户ID查询详细信息
     *
     * @param id 用户id
     * @return
     */
    @Select("SELECT * FROM e_user_specific_information WHERE user_id = #{user_id}")
    UserSpecificInformationEntity selectUserSpecificInformationById(@Param("user_id") Long id);


    /**
     * 修改积分数量
     *
     * @param integralNum 积分
     * @param userId      用户id
     */
    @Update("UPDATE e_user_specific_information SET integral = #{integral} WHERE user_id =  #{user_id}")
    void updateIntegralNum(@Param("integral") Integer integralNum, @Param("user_id") Long userId);

}