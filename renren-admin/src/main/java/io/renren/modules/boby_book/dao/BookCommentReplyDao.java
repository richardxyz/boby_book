package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.BookCommentReplyEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 图书章节评论回复
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface BookCommentReplyDao extends BaseDao<BookCommentReplyEntity> {

    /**
     * 根据id查询图书评论回复信息
     *
     * @param id 评论id
     * @return
     */
    List<BookCommentReplyEntity> selectCommentReplyByCommentId(@Param("comment_id") Long id);
}
