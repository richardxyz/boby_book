package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 一级分类表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommodityBaseCatalogOneExcel {
    @Excel(name = "商品分类主键id")
    private Long id;
    @Excel(name = "商品分类主键名称")
    private String name;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}