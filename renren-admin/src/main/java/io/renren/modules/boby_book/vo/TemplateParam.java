package io.renren.modules.boby_book.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 模板内容
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TemplateParam {
    private String key;
    private String value;

    public TemplateParam(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
