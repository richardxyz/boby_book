package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommodityGiftEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 兑换礼品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface CommodityGiftDao extends BaseDao<CommodityGiftEntity> {

    /**
     * 根据用户id查询已兑换商品
     *
     * @param userId      用户id
     * @param commodityId 商品id
     * @return
     */
    List<CommodityGiftEntity> selectGiftByUserId(@Param("user_id") Long userId, @Param("commodity_id") Long commodityId);
}