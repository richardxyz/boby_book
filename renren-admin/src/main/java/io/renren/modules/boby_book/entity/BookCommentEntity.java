package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 图书章节评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_book_comment")
public class BookCommentEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 书籍id
     */
    private Long bookId;
    /**
     * 评论用户IP
     */
    private String userIp;
    /**
     * 评论内容
     */
    private String commentContent;
    /**
     * 评论时间
     */
    private Date commentTime;
    /**
     * 用户id
     */
    private Long fromUid;
    /**
     * 回复次数
     */
    private Integer replayCount;
    /**
     * 点赞次数
     */
    private Integer fabulousCount;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;
    /**
     * 图书信息
     */
    @TableField(exist = false)
    BookEntity book;
    /**
     * 用户信息
     */
    @TableField(exist = false)
    UserEntity user;


}