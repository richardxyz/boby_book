package io.renren.modules.boby_book.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品信息表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Data
@ApiModel(value = "商品信息表")
public class CommoditySpuInfoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "商品名称")
    private Long commodityName;

    @ApiModelProperty(value = "商品描述")
    private String commodityDescribe;

    @ApiModelProperty(value = "品牌id")
    private String brandId;

    @ApiModelProperty(value = "商品分类id")
    private String catalogId;

    @ApiModelProperty(value = "默认积分")
    private Integer integral;

    @ApiModelProperty(value = "默认价格")
    private Double price;

    @ApiModelProperty(value = "已兑换总个数")
    private Integer exchangeNum;

    @ApiModelProperty(value = "评论数")
    private Integer commentNum;

    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;
}
