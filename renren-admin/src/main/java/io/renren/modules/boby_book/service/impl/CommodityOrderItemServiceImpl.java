package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommodityOrderItemDao;
import io.renren.modules.boby_book.dto.CommodityOrderItemDTO;
import io.renren.modules.boby_book.entity.CommodityOrderItemEntity;
import io.renren.modules.boby_book.service.CommodityOrderItemService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 订单详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-10-30
 */
@Service
@Transactional
public class CommodityOrderItemServiceImpl extends CrudServiceImpl<CommodityOrderItemDao, CommodityOrderItemEntity, CommodityOrderItemDTO> implements CommodityOrderItemService {

    @Override
    public QueryWrapper<CommodityOrderItemEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<CommodityOrderItemEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

}