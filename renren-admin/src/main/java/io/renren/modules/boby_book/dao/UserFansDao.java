package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.UserFansEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户粉丝
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserFansDao extends BaseDao<UserFansEntity> {
    /**
     * 删除粉丝数据
     *
     * @param fuId 用户id
     * @param tuId 所关注用户id
     */
    @Delete("DELETE FROM e_user_fans WHERE user_id = #{user_id} AND fans_user_id = #{fans_user_id}")
    void cancelFollowUser(@Param("fans_user_id") Long fuId, @Param("user_id") Long tuId);

    /**
     * 查询用户所有粉丝信息
     *
     * @param userId 用户id
     * @return
     */
    List<UserFansEntity> selectFans(@Param("user_id") Long userId);
}