package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.FindDTO;
import io.renren.modules.boby_book.entity.FindEntity;

import java.util.List;

/**
 * 发现数据
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface FindService extends CrudService<FindEntity, FindDTO> {

    /**
     * 查询所有发现数据
     *
     * @return
     */
    List<FindEntity> selectFindAll();


    /**
     * 查询当前用户视频
     *
     * @param userId 用户id
     * @return
     */
    List<FindEntity> selectFindByUserId(Long userId);
}