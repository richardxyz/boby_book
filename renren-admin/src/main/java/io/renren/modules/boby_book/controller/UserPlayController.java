package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.UserPlayDTO;
import io.renren.modules.boby_book.entity.UserPlayEntity;
import io.renren.modules.boby_book.excel.UserPlayExcel;
import io.renren.modules.boby_book.service.UserPlayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 用户最近播放
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/userplay")
@Api(tags = "用户最近播放")
public class UserPlayController {
    @Autowired
    private UserPlayService playService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:userplay:page")
    public Result<PageData<UserPlayDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<UserPlayDTO> page = playService.page(params);

        return new Result<PageData<UserPlayDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:userplay:info")
    public Result<UserPlayDTO> get(@PathVariable("id") Long id) {
        UserPlayDTO data = playService.get(id);

        return new Result<UserPlayDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:userplay:save")
    public Result save(@RequestBody UserPlayDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        playService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:userplay:update")
    public Result update(@RequestBody UserPlayDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        playService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:userplay:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        playService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:userplay:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<UserPlayDTO> list = playService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, UserPlayExcel.class);
    }


    /**
     * 添加入播放列表
     *
     * @param userId 用户id
     * @param bookId 图书id
     * @return
     */
    @PostMapping("/addToPlay")
    public Result addToPlay(Long userId, Long bookId) {
        Integer count = playService.lastPlay(userId, bookId);
        if (count == 1) {
            return new Result();
        } else {
            playService.addToPlay(userId, bookId);
            return new Result().ok("添加成功");
        }
    }

    /**
     * 查询今日播放
     *
     * @param id 用户id
     * @return
     */
    @PostMapping("/selectTodayBook")
    public Result selectTodayBook(Long id) {
        List<UserPlayEntity> entity = playService.selectTodayBook(id);
        return new Result().ok(entity);
    }

    /**
     * 查询昨日播放
     *
     * @param id 用户id
     * @return
     */
    @PostMapping("/selectYesterdayBook")
    public Result selectYesterdayBook(Long id) {
        List<UserPlayEntity> entity = playService.selectYesterdayBook(id);
        return new Result().ok(entity);
    }

    /**
     * 查询更早播放
     *
     * @param id 用户id
     * @return
     */
    @PostMapping("/selecEarlierBook")
    public Result selecEarlierBook(Long id) {
        List<UserPlayEntity> entity = playService.selecEarlierBook(id);
        return new Result().ok(entity);
    }
}