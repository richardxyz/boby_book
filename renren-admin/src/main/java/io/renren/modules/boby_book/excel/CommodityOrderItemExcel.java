package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 订单详情表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommodityOrderItemExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "订单id")
    private Long orderId;
    @Excel(name = "订单编号")
    private String orderSn;
    @Excel(name = "商品id(对应spu_id)")
    private Long commodityId;
    @Excel(name = "商品图片")
    private String commodityPic;
    @Excel(name = "商品名称")
    private String commodityName;
    @Excel(name = "品牌")
    private String commodityBrand;
    @Excel(name = "商品编号（仓库）")
    private String commoditySn;
    @Excel(name = "销售价格")
    private Integer commodityPrice;
    @Excel(name = "购买数量")
    private Integer commodityQuantity;
    @Excel(name = "商品sku编号")
    private Long commoditySkuId;
    @Excel(name = "商品sku条码")
    private String commoditySkuCode;
    @Excel(name = "商品分类id")
    private Long commodityCategoryId;
    @Excel(name = "商品的销售属性1")
    private String sp1;
    @Excel(name = "商品的销售属性2")
    private String sp2;
    @Excel(name = "商品的销售属性3")
    private String sp3;
    @Excel(name = "商品销售属性:[{\"key\":\"颜色\",\"value\":\"颜色\"},{\"key\":\"容量\",\"value\":\"4G\"}]")
    private String commodityAttr;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}