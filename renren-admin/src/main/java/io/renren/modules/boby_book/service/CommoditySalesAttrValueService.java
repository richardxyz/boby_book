package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommoditySalesAttrValueDTO;
import io.renren.modules.boby_book.entity.CommoditySalesAttrValueEntity;

/**
 * 销售属性值
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommoditySalesAttrValueService extends CrudService<CommoditySalesAttrValueEntity, CommoditySalesAttrValueDTO> {

}