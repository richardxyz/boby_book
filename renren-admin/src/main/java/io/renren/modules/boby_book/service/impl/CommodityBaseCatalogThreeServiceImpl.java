package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommodityBaseCatalogOneDao;
import io.renren.modules.boby_book.dao.CommodityBaseCatalogThreeDao;
import io.renren.modules.boby_book.dao.CommodityBaseCatalogTwoDao;
import io.renren.modules.boby_book.dto.CommodityBaseCatalogThreeDTO;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogOneEntity;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogThreeEntity;
import io.renren.modules.boby_book.entity.CommodityBaseCatalogTwoEntity;
import io.renren.modules.boby_book.service.CommodityBaseCatalogThreeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 商品分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-11-11
 */
@Service
@Transactional
public class CommodityBaseCatalogThreeServiceImpl extends CrudServiceImpl<CommodityBaseCatalogThreeDao, CommodityBaseCatalogThreeEntity, CommodityBaseCatalogThreeDTO> implements CommodityBaseCatalogThreeService {

    @Override
    public QueryWrapper<CommodityBaseCatalogThreeEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<CommodityBaseCatalogThreeEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private CommodityBaseCatalogOneDao catalogOneDao;

    @Autowired
    private CommodityBaseCatalogTwoDao catalogTwoDao;

    @Autowired
    private CommodityBaseCatalogThreeDao catalogThreeDao;


    @Override
    public List<CommodityBaseCatalogOneEntity> getCatalog1() {
        return catalogOneDao.selectList(null);
    }


    @Override
    public List<CommodityBaseCatalogTwoEntity> getCatalog2(Long catalog1Id) {
        return catalogTwoDao.selectListByCatalog1Id(catalog1Id);
    }

    @Override
    public List<CommodityBaseCatalogThreeEntity> getCatalog3(Long catalogId) {
        return catalogThreeDao.selectListByCatalog2Id(catalogId);
    }


}