
package io.renren.modules.boby_book.controller;

import io.renren.common.redis.RedisUtils;
import io.renren.common.utils.AesCbcUtil;
import io.renren.common.utils.Result;
import io.renren.common.utils.WeChatUtils;
import io.renren.modules.boby_book.entity.UserEntity;
import io.renren.modules.boby_book.service.UserService;
import io.renren.modules.boby_book.service.UserSpecificInformationService;
import io.swagger.annotations.Api;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 用户基本信息表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/wxlogin")
@Api(tags = "用户基本信息表")
public class WXLoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private UserSpecificInformationService specificInformationService;


    /**
     * 用户微信授权注册
     *
     * @param code      小程序传过来的code
     * @param avatarUrl 头像
     * @param city      市区
     * @param country   国家
     * @param gender    性别 0：未知	，1：男性，2：女性
     * @param language  语言
     * @param nickName  用户昵称
     * @param province  省份
     * @return
     */
    @GetMapping("/wxEmpower")
    public Result wxEmpower(String code, String avatarUrl, String city, String country, String gender, String language, String nickName, String province) {
        Map<String, Object> codeMap = WeChatUtils.selectOpenIdByCode(code);
        String openid = (String) codeMap.get("openid");
        String cnAa = WeChatUtils.changeCharacter(nickName);
        userService.wxEmpower(openid, avatarUrl, cnAa, province);
        UserEntity entity = userService.selectUserByOpenid(openid);
        specificInformationService.Register(entity.getId());
        return new Result().ok(openid);
    }

    /**
     * 用户微信登录
     *
     * @param code 临时登录凭证
     * @return
     */
    @GetMapping("/wxLogin")
    public Result wxLogin(String code) {
        Map<String, Object> codeMap = WeChatUtils.selectOpenIdByCode(code);
        String openid = (String) codeMap.get("openid");
        Integer count = userService.wxLogin(openid);
        UserEntity entity = userService.selectUserByOpenid(openid);
//        System.out.println(entity.toString());
        Map map = new HashMap<>();
        if (count == 1) {
            map.put("status", 1);
            map.put("msg", "登录成功");
            map.put("user", entity);
            return new Result().ok(map);
        } else {
            map.put("status", 0);
            map.put("msg", "登录失败");
            map.put("user", null);
            return new Result().ok(map);
        }
    }


    /**
     * 用户微信登录授权
     *
     * @param encryptedData 包括敏感数据在内的完整用户信息的加密数据
     * @param iv            加密算法的初始向量
     * @param code          临时登录凭证
     * @return
     */
    @GetMapping("/wxAuthorize")
    public Result wxAuthorize(String encryptedData, String iv, String code) {
//        System.err.println("code：" + code);
//        System.err.println("encryptedData：" + encryptedData);
//        System.err.println("iv：" + iv);
        Map map = new HashMap<>();
        if (code == null || code.length() == 0) {
            map.put("status", 0);
            map.put("msg", "code是空的");
            return new Result().ok(map);
        }
//        System.out.println("code" + code);
        Map<String, Object> codeMap = WeChatUtils.selectOpenIdByCode(code);
        String openid = (String) codeMap.get("openid");
        String session_key = (String) codeMap.get("session_key");
        String key = UUID.randomUUID().toString().replace("-", "");
        String value = openid + session_key;
        redisUtils.set(key, value);
        try {
            System.out.println("进入解密程序");
            String decrypt = AesCbcUtil.decrypt(encryptedData, session_key, iv, "utf-8");
            JSONObject jsonObject = JSONObject.fromObject(decrypt);
            System.out.println("decrypt：" + decrypt);
            System.out.println("decrypt长度：" + decrypt.length());
            if (null != decrypt && decrypt.length() > 0) {
                System.out.println("解密成功");
                map.put("status", 1);
                map.put("msg", "解密成功");
                map.put("third_Session", key);
                HashMap userinfo = new HashMap<>();
                userinfo.put("openid", openid);
                userinfo.put("session_key", session_key);
                map.put("userInfo", userinfo);
            }
        } catch (Exception e) {
            System.out.println("解密失败");
            map.put("status", 0);
            map.put("msg", "解密失败");
        }

        return new Result().ok(map);

    }


}