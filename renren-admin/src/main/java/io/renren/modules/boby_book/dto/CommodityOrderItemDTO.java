package io.renren.modules.boby_book.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 订单详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@ApiModel(value = "订单详情")
public class CommodityOrderItemDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;

    @ApiModelProperty(value = "订单id")
    private Long orderId;

    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    @ApiModelProperty(value = "商品id(对应spu_id)")
    private Long commodityId;

    @ApiModelProperty(value = "商品图片")
    private String commodityPic;

    @ApiModelProperty(value = "商品名称")
    private String commodityName;

    @ApiModelProperty(value = "品牌")
    private String commodityBrand;

    @ApiModelProperty(value = "商品编号（仓库）")
    private String commoditySn;

    @ApiModelProperty(value = "销售价格")
    private Integer commodityPrice;

    @ApiModelProperty(value = "购买数量")
    private Integer commodityQuantity;

    @ApiModelProperty(value = "商品sku编号")
    private Long commoditySkuId;

    @ApiModelProperty(value = "商品sku条码")
    private String commoditySkuCode;

    @ApiModelProperty(value = "商品分类id")
    private Long commodityCategoryId;

    @ApiModelProperty(value = "商品的销售属性1")
    private String sp1;

    @ApiModelProperty(value = "商品的销售属性2")
    private String sp2;

    @ApiModelProperty(value = "商品的销售属性3")
    private String sp3;

    @ApiModelProperty(value = "更新者")
    private String commodityAttr;

    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;


}