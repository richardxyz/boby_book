package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 图书信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class BookExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "图书封面")
    private String bookCover;
    @Excel(name = "图书名")
    private String bookName;
    @Excel(name = "图书作者")
    private String bookAuthor;
    @Excel(name = "图书简介")
    private String bookSynopsis;
    @Excel(name = "类别")
    private Long bookCategory;
    @Excel(name = "图书章节数量")
    private Integer bookChapterNum;
    @Excel(name = "图书订阅数量")
    private Integer bookSubscribeNum;
    @Excel(name = "图书播放数量")
    private Integer bookPlayNum;
    @Excel(name = "图书收藏数量")
    private Integer bookCollectionNum;
    @Excel(name = "图书评论数量")
    private Integer bookCommentNum;
    @Excel(name = "图书点赞数量")
    private Integer bookFabulousNum;
    @Excel(name = "评分")
    private Integer score;
    @Excel(name = "上架时间")
    private Date shelfTime;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}