package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.CommodityOrderDTO;
import io.renren.modules.boby_book.entity.*;
import io.renren.modules.boby_book.excel.CommodityOrderExcel;
import io.renren.modules.boby_book.service.CommodityOrderService;
import io.renren.modules.boby_book.service.CommoditySkuInfoService;
import io.renren.modules.boby_book.service.UserReceiveAddressService;
import io.renren.modules.boby_book.service.UserSpecificInformationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 商品订单
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-10-30
 */
@RestController
@RequestMapping("boby_book/commodityorder")
@Api(tags = "商品订单")
public class CommodityOrderController {


    @Autowired
    private CommodityOrderService orderService;

    @Autowired
    private UserReceiveAddressService userReceiveAddressService;

    @Autowired
    private CommoditySkuInfoService skuInfoService;

    @Autowired
    private UserSpecificInformationService userSpecificInformationService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:commodityorder:page")
    public Result<PageData<CommodityOrderDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<CommodityOrderDTO> page = orderService.page(params);

        return new Result<PageData<CommodityOrderDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:commodityorder:info")
    public Result<CommodityOrderDTO> get(@PathVariable("id") Long id) {
        CommodityOrderDTO data = orderService.get(id);

        return new Result<CommodityOrderDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:commodityorder:save")
    public Result save(@RequestBody CommodityOrderDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        orderService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:commodityorder:update")
    public Result update(@RequestBody CommodityOrderDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        orderService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:commodityorder:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        orderService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:commodityorder:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<CommodityOrderDTO> list = orderService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, CommodityOrderExcel.class);
    }


    /**
     * 生成交易码
     *
     * @param userid 用户id
     * @return
     */
    @PostMapping("/genTradeCode")
    public Result genTradeCode(Long userid) {

        // 生成交易码，为了在提交订单时做交易码的校验
        String tradeCode = orderService.genTradeCode(userid);

        return new Result().ok(tradeCode);
    }

    /**
     * 提交订单
     *
     * @param userId           用户id
     * @param skuId            商品id
     * @param receiveAddressId 地址id
     * @param purchaseQuantity 购买数量
     * @param zongJia          总价
     * @param tradeCode        交易码
     * @return
     */
    @PostMapping("/submitOrder")
    public Result submitOrder(Long userId, Long skuId, Long receiveAddressId, Integer purchaseQuantity, Integer zongJia, String tradeCode) {
        Map<String, Object> map = new HashMap<>();
        CommoditySkuInfoEntity gift = skuInfoService.selectById(skuId);
        System.out.println("用户id：" + userId);
        System.out.println(gift.toString());
        System.out.println("地址id：" + receiveAddressId);
        System.out.println("购买数量：" + purchaseQuantity);
        System.out.println("总价：" + zongJia);
        System.out.println("交易码：" + tradeCode);

        // 检查交易码
        String success = orderService.checkTradeCode(userId, tradeCode);
        System.out.println("success：" + success);
        if (success.equals("success")) {
            List<CommodityOrderItemEntity> orderItems = new ArrayList<>();
            String outTradeNo = "yidou";
            outTradeNo = outTradeNo + System.currentTimeMillis();// 将毫秒时间戳拼接到外部订单号
            SimpleDateFormat sdf = new SimpleDateFormat("YYYYMMDDHHmmss");
            outTradeNo = outTradeNo + sdf.format(new Date());// 将时间字符串拼接到外部订单号
            // 订单对象
            CommodityOrderEntity order = new CommodityOrderEntity();
            order.setUserId(userId);//用户id
            order.setOrderSn(outTradeNo);// 外部订单号
            order.setCreateTime(new Date());// 提交时间
            order.setTotalAmount(zongJia);// 订单总金额
            order.setPayAmount(zongJia);// 应付金额（实际支付金额）
            order.setPayType(0);// 支付方式：0->未支付；1->支付宝；->微信
            order.setSourceType(1);// 订单来源：0->PC订单；1->app订单
            order.setStatus(0);// 订单状态：0->待付款；1->待发货；
            order.setAutoConfirmDay(7);// 自动确认时间（天）
            order.setBillType(0);// 发票类型：0->不开发票；1->电子发票；->纸质发票
            order.setBillHeader(null);// 发票抬头
            order.setBillContent(null);// 发票内容
            order.setBillReceiverPhone(null);// 收票人电话
            order.setBillReceiverEmail(null);// 收票人邮箱
            // order.setDiscountAmount(null);
            // order.setFreightAmount(); 运费，支付后，在生成物流信息时
            //order.setMemberUsername(nickname);
            // order.setOrderType(1);
            UserReceiveAddressEntity userReceiveAddress = userReceiveAddressService.selectById(receiveAddressId);//查询收货地址信息
            order.setReceiverName(userReceiveAddress.getName());// 收货人姓名
            order.setReceiverPhone(userReceiveAddress.getPhoneNumber());// 收货人电话
            order.setReceiverPostCode(userReceiveAddress.getPostCode());// 收货人邮编
            order.setReceiverProvince(userReceiveAddress.getProvince());// 省份/直辖市
            order.setReceiverCity(userReceiveAddress.getCity());// 城市
            order.setReceiverRegion(userReceiveAddress.getRegion());// 区
            order.setReceiverDetailAddress(userReceiveAddress.getDetailAddress());// 详细地址
            order.setNote(null);// 订单备注

            // 当前日期加一天，一天后配送
            // Calendar c = Calendar.getInstance();
            // c.add(Calendar.DATE, 1);
            // Date time = c.getTime();
            // order.setReceiveTime(time);
            // 获得订单详情列表
            CommodityOrderItemEntity omsOrderItem = new CommodityOrderItemEntity();
            // 检价
            boolean b = skuInfoService.checkPrice(gift.getId(), gift.getPrice());
            if (b == false) {
                map.put("code", "606");
                map.put("msg", "当前商品价格已调整，请您重新确认订单");
                return new Result().ok(map);
            }
            // 验库存,远程调用库存系统
            omsOrderItem.setCommodityPic(gift.getSkuDefaultImg());
            omsOrderItem.setCommodityName(gift.getSkuName());
            omsOrderItem.setOrderSn(outTradeNo);// 外部订单号，用来和其他系统进行交互，防止重复
            omsOrderItem.setCommodityCategoryId(gift.getCatalogId());
            omsOrderItem.setCommodityPrice(zongJia);
            // omsOrderItem.setRealAmount();
            omsOrderItem.setCommodityQuantity(purchaseQuantity);
            omsOrderItem.setCommoditySkuCode("111111111111");
            omsOrderItem.setCommoditySkuId(gift.getId());
            omsOrderItem.setCommodityId(gift.getCommodityId());
            omsOrderItem.setCommoditySn("仓库对应的商品编号");// 在仓库中的skuId

            orderItems.add(omsOrderItem);//所有商品

            order.setOrderItems(orderItems);//设置商品信息信息

            // 将订单和订单详情写入数据库
            // 删除购物车的对应商品***
            orderService.saveOrder(order);
            map.put("code", "0");
            map.put("msg", "订单提交成功");
            return new Result().ok(map);
//            return new Result().ok("订单提交成功");
        } else {
            map.put("code", "500");
            map.put("msg", "订单已失效");
            return new Result().ok(map);
        }
    }

    /**
     * 查询登录用户所有订单
     *
     * @param userId 用户id
     * @return
     */
    @PostMapping("/findAllOrder")
    public Result findAllOrder(Long userId) {
        List<CommodityOrderEntity> list = orderService.findAllOrder(userId);
        return new Result().ok(list);
    }

    /**
     * 取消/删除订单
     *
     * @param orderId 订单id
     * @return
     */
    @PostMapping("/cancelOrder")
    public Result cancelOrder(Long orderId) {
        orderService.cancelOrder(orderId);
        return new Result();
    }

    /**
     * 付款
     *
     * @param userId  用户id
     * @param orderId 订单id
     * @return
     */
    @PostMapping("/payment")
    public Result payment(Long userId, Long orderId) {
        Map<String, Object> map = new HashMap<>();
        try {
            UserSpecificInformationEntity userSpecificInformation = userSpecificInformationService.selectUserSpecificInformationById(userId);
            CommodityOrderEntity order = orderService.selectById(orderId);
            Integer integral = userSpecificInformation.getIntegral();
            Integer payAmount = order.getPayAmount();
            if (payAmount > integral) {
                map.put("code", "606");
                map.put("msg", "您的积分不足，您可做任务赚取积分");
                return new Result().ok(map);
//                return new Result().error(606, "您的积分不足，您可做任务赚取积分");
            } else {
                integral = integral - payAmount;
                userSpecificInformationService.updateIntegralNum(integral, userId);
                order.setPayType(2);
                order.setStatus(1);
                orderService.updateById(order);
                map.put("code", "0");
                map.put("msg", "支付完成");
                return new Result().ok(map);
//                return new Result().ok("支付完成");
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("code", "500");
            map.put("msg", "网络异常,支付失败");
            return new Result().ok(map);
//            return new Result().error("支付失败");
        }
    }
}