package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommoditySkuImageDTO;
import io.renren.modules.boby_book.entity.CommoditySkuImageEntity;

/**
 * 商品图片表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommoditySkuImageService extends CrudService<CommoditySkuImageEntity, CommoditySkuImageDTO> {

}