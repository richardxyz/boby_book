package io.renren.modules.boby_book.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * sku平台属性值关联表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Data
@ApiModel(value = "sku平台属性值关联表")
public class CommoditySkuAttrValueDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "属性id（冗余)")
    private Long attrOd;

    @ApiModelProperty(value = "属性值id")
    private Long valueId;

    @ApiModelProperty(value = "skuid")
    private Long skuId;

    @ApiModelProperty(value = "销售属性名称")
    private String attrName;

    @ApiModelProperty(value = "销售属性值名称")
    private String attrValueName;

    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;
}
