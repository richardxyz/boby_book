package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户基本信息表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_user")
public class UserEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 微信(用户唯一标识 OpenID)
     */
    private String openId;
    /**
     * 昵称
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 性别
     */
    private String gender;
    /**
     * 头像
     */
    private String headPortrait;
    /**
     * 地区
     */
    private String regional;
    /**
     * 详细地址
     */
    private String detailedAddress;

    /**
     * 语录
     */
    private String quotations;

    /**
     * 本周签到状态
     */
    private String signIn;

    /**
     * 签到总天数
     */
    private Integer signInNum;

    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;
}