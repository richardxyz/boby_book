package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommodityGiftDTO;
import io.renren.modules.boby_book.entity.CommodityGiftEntity;

import java.util.List;

/**
 * 兑换礼品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface CommodityGiftService extends CrudService<CommodityGiftEntity, CommodityGiftDTO> {

    /**
     * 根据用户id查询已兑换商品
     *
     * @param userid      用户id
     * @param commodityId 商品id
     * @return
     */
    List<CommodityGiftEntity> selectGiftByUserId(Long userid, Long commodityId);

    /**
     * 添加兑换数据
     *
     * @param giftid           商品id
     * @param purchaseQuantity 兑换数量
     * @param userid           用户id
     */
    void saveGift(Long giftid, Integer purchaseQuantity, Long userid);
}