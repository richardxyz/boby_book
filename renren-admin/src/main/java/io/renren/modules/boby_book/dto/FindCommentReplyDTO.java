package io.renren.modules.boby_book.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 发现评论回复
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@ApiModel(value = "发现评论回复")
public class FindCommentReplyDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "评论回复id")
    private Long id;

    @ApiModelProperty(value = "目标评论id")
    private Long findCommentId;

    @ApiModelProperty(value = "回复内容")
    private String content;

    @ApiModelProperty(value = "回复时间")
    private Date replyTime;

    @ApiModelProperty(value = "回复用户id")
    private Long fromUid;

    @ApiModelProperty(value = "回复用户ip")
    private String fromUip;

    @ApiModelProperty(value = "目标用户id")
    private Long toUid;

    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;


}