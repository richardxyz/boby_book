package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommoditySkuInfoDTO;
import io.renren.modules.boby_book.entity.CommoditySkuInfoEntity;
import io.renren.modules.boby_book.vo.SkuVo;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 商品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommoditySkuInfoService extends CrudService<CommoditySkuInfoEntity, CommoditySkuInfoDTO> {
    /**
     * 根据商品id获取所有库存信息
     *
     * @param commodityId 商品id
     * @return
     */
    List<CommoditySkuInfoEntity> getSkuSaleAttrValueListBySpu(Long commodityId);

    /**
     * 查询所有商品
     *
     * @return
     */
    List<SkuVo> findCommoditySkuInfoAll();

    /**
     * 根据分类id查询商品
     *
     * @param commodityId 商品id
     * @return
     */
    List<SkuVo> findCommodityBycommodityId(Long commodityId);

    /**
     * 根据id查询商品详细信息
     *
     * @param skuid   商品库存id
     * @param request
     * @return
     */
    ModelMap selectSkuById(Long skuid, HttpServletRequest request);

    /**
     * 修改库存兑换数量信息
     *
     * @param exchangeNum 以兑换数量
     * @param id          商品库存id
     */
    void updateExchangeNumById(Integer exchangeNum, Long id);

    /**
     * 校验价格是否与数据库一致
     *
     * @param skuId    库存id
     * @param skuMoney 商品价格
     * @return
     */
    boolean checkPrice(Long skuId, Double skuMoney);
}