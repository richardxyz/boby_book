package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommoditySalesAttrDao;
import io.renren.modules.boby_book.dto.CommoditySalesAttrDTO;
import io.renren.modules.boby_book.entity.CommoditySalesAttrEntity;
import io.renren.modules.boby_book.service.CommoditySalesAttrService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 销售属性
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-11-11
 */
@Service
@Transactional
public class CommoditySalesAttrServiceImpl extends CrudServiceImpl<CommoditySalesAttrDao, CommoditySalesAttrEntity, CommoditySalesAttrDTO> implements CommoditySalesAttrService {

    @Override
    public QueryWrapper<CommoditySalesAttrEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<CommoditySalesAttrEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    /**
     * sku销售属性信息
     */
    @Autowired
    private CommoditySalesAttrDao salesAttrDao;

    @Override
    public List<CommoditySalesAttrEntity> spuSaleAttrListCheckBySku(Long commodityId, Long skuId) {

        List<CommoditySalesAttrEntity> salesAttrEntityList = salesAttrDao.selectSpuSaleAttrListCheckBySku(commodityId,skuId);
        return salesAttrEntityList;
    }
}