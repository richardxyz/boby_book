package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.FindFabulousDTO;
import io.renren.modules.boby_book.excel.FindFabulousExcel;
import io.renren.modules.boby_book.service.FindFabulousService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 发现点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-20
 */
@RestController
@RequestMapping("boby_book/findfabulous")
@Api(tags = "发现点赞表")
public class FindFabulousController {

    @Autowired
    private FindFabulousService findFabulousService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:findfabulous:page")
    public Result<PageData<FindFabulousDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<FindFabulousDTO> page = findFabulousService.page(params);

        return new Result<PageData<FindFabulousDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:findfabulous:info")
    public Result<FindFabulousDTO> get(@PathVariable("id") Long id) {
        FindFabulousDTO data = findFabulousService.get(id);

        return new Result<FindFabulousDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:findfabulous:save")
    public Result save(@RequestBody FindFabulousDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        findFabulousService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:findfabulous:update")
    public Result update(@RequestBody FindFabulousDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        findFabulousService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:findfabulous:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        findFabulousService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:findfabulous:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<FindFabulousDTO> list = findFabulousService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, FindFabulousExcel.class);
    }

    /**
     * 根据发现id判断点赞状态，根据状态进行不同的操作
     *
     * @param userId  用户id
     * @param findId  发现id
     * @param request
     * @return
     */
    @PostMapping("/judgeFindFabulousStatus")
    public Result judgeFindFabulousStatus(Long userId, Long findId, HttpServletRequest request) {
        System.out.println("userId：" + userId);
        System.out.println("findId：" + findId);
        Integer count = findFabulousService.judgeFindFabulousStatus(userId, findId);
        if (count >= 1) {
            findFabulousService.cancelFindFabulous(userId, findId);
            return new Result().ok("取消点赞成功");
        } else {
            findFabulousService.addFindFabulous(userId, findId, request);
            return new Result().ok("点赞成功");
        }
    }
}