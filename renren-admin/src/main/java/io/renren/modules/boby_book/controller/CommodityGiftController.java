package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.CommodityGiftDTO;
import io.renren.modules.boby_book.entity.CommodityGiftEntity;
import io.renren.modules.boby_book.excel.CommodityGiftExcel;
import io.renren.modules.boby_book.service.CommodityGiftService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 兑换礼品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/commoditygift")
@Api(tags = "兑换礼品详情")
public class CommodityGiftController {
    @Autowired
    private CommodityGiftService giftService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:commoditygift:page")
    public Result<PageData<CommodityGiftDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<CommodityGiftDTO> page = giftService.page(params);

        return new Result<PageData<CommodityGiftDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:commoditygift:info")
    public Result<CommodityGiftDTO> get(@PathVariable("id") Long id) {
        CommodityGiftDTO data = giftService.get(id);

        return new Result<CommodityGiftDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:commoditygift:save")
    public Result save(@RequestBody CommodityGiftDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        giftService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:commoditygift:update")
    public Result update(@RequestBody CommodityGiftDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        giftService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:commoditygift:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        giftService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:commoditygift:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<CommodityGiftDTO> list = giftService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, CommodityGiftExcel.class);
    }


    /**
     * 根据用户id查询已兑换商品
     *
     * @param userId      用户id
     * @param commodityId 商品id
     * @return
     */
    @PostMapping("/selectGiftByUserId")
    public Result selectGiftByUserId(Long userId, Long commodityId) {
        List<CommodityGiftEntity> list = giftService.selectGiftByUserId(userId, commodityId);
        return new Result().ok(list);
    }


}