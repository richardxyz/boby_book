package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.UserSubscribeDao;
import io.renren.modules.boby_book.dto.UserSubscribeDTO;
import io.renren.modules.boby_book.entity.UserSubscribeEntity;
import io.renren.modules.boby_book.service.UserSubscribeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 用户订阅
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class UserSubscribeServiceImpl extends CrudServiceImpl<UserSubscribeDao, UserSubscribeEntity, UserSubscribeDTO> implements UserSubscribeService {

    @Override
    public QueryWrapper<UserSubscribeEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<UserSubscribeEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}