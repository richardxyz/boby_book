package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.IpUtils;
import io.renren.modules.boby_book.dao.BookCommentFabulousDao;
import io.renren.modules.boby_book.dto.BookCommentFabulousDTO;
import io.renren.modules.boby_book.entity.BookCommentEntity;
import io.renren.modules.boby_book.entity.BookCommentFabulousEntity;
import io.renren.modules.boby_book.service.BookCommentFabulousService;
import io.renren.modules.boby_book.service.BookCommentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 图书评论点赞
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-10-30
 */
@Service
@Transactional
public class BookCommentFabulousServiceImpl extends CrudServiceImpl<BookCommentFabulousDao, BookCommentFabulousEntity, BookCommentFabulousDTO> implements BookCommentFabulousService {

    @Override
    public QueryWrapper<BookCommentFabulousEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<BookCommentFabulousEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Autowired
    private BookCommentFabulousDao fabulousDao;

    @Autowired
    private BookCommentService commentService;

    @Autowired
    private IpUtils ipUtils;

    @Override
    public Integer judgeCommentFabulousStatus(Long userId, Long commentId) {
        return fabulousDao.judgeCommentFabulousStatus(userId, commentId);
    }


    @Override
    public void addFabulous(Long userId, Long commentId, HttpServletRequest request) {
        String ip = ipUtils.getIpAddr(request);
        BookCommentEntity entity = commentService.selectById(commentId);
        Integer fabulousCount = entity.getFabulousCount();
        fabulousCount++;
        entity.setFabulousCount(fabulousCount);
        commentService.updateById(entity);
        BookCommentFabulousEntity bookCommentFabulousEntity = new BookCommentFabulousEntity();
        bookCommentFabulousEntity.setCommentId(commentId);
        bookCommentFabulousEntity.setFromUid(userId);
        bookCommentFabulousEntity.setUserIp(ip);
        fabulousDao.insert(bookCommentFabulousEntity);
    }

    @Override
    public void cancelFabulous(Long userId, Long commentId) {
        BookCommentFabulousEntity fabulousEntity  = fabulousDao.selectFabulousByUserIdAndCommentId(userId,commentId);
        fabulousDao.deleteById(fabulousEntity.getId());
        BookCommentEntity entity = commentService.selectById(commentId);
        Integer fabulousCount = entity.getFabulousCount();
        fabulousCount--;
        entity.setFabulousCount(fabulousCount);
        commentService.updateById(entity);
    }
}