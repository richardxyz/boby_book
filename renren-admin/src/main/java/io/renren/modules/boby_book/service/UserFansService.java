package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserFansDTO;
import io.renren.modules.boby_book.entity.UserFansEntity;

import java.util.List;

/**
 * 用户粉丝
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserFansService extends CrudService<UserFansEntity, UserFansDTO> {

    /**
     * 查询用户所有关注信息
     *
     * @param userId 用户id
     * @return
     */
    List<UserFansEntity> selectFans(Long userId);
}