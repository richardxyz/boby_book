package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.BookCategoryDao;
import io.renren.modules.boby_book.dto.BookCategoryDTO;
import io.renren.modules.boby_book.entity.BookCategoryEntity;
import io.renren.modules.boby_book.service.BookCategoryService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 图书类别
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class BookCategoryServiceImpl extends CrudServiceImpl<BookCategoryDao, BookCategoryEntity, BookCategoryDTO> implements BookCategoryService {

    @Override
    public QueryWrapper<BookCategoryEntity> getWrapper(Map<String, Object> params) {
        QueryWrapper<BookCategoryEntity> wrapper = new QueryWrapper<>();
        if (params != null) {
            String id = (String) params.get("id");
            wrapper.eq(StringUtils.isNotBlank(id), "id", id);
        }
        return wrapper;
    }


}