package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommoditySkuSaleAttrValueDao;
import io.renren.modules.boby_book.dto.CommoditySkuSaleAttrValueDTO;
import io.renren.modules.boby_book.entity.CommoditySkuSaleAttrValueEntity;
import io.renren.modules.boby_book.service.CommoditySkuSaleAttrValueService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * sku销售属性值
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-11-11
 */
@Service
@Transactional
public class CommoditySkuSaleAttrValueServiceImpl extends CrudServiceImpl<CommoditySkuSaleAttrValueDao, CommoditySkuSaleAttrValueEntity, CommoditySkuSaleAttrValueDTO> implements CommoditySkuSaleAttrValueService {

    @Override
    public QueryWrapper<CommoditySkuSaleAttrValueEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<CommoditySkuSaleAttrValueEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}