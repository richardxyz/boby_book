package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.BookCommentFabulousDTO;
import io.renren.modules.boby_book.entity.BookCommentFabulousEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * 图书评论点赞
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface BookCommentFabulousService extends CrudService<BookCommentFabulousEntity, BookCommentFabulousDTO> {

    /**
     * 根据评论id判断点赞状态
     *
     * @param userId    用户id
     * @param commentId 评论id
     * @return
     */
    Integer judgeCommentFabulousStatus(Long userId, Long commentId);

    /**
     * 给评论点赞
     *
     * @param userId    用户id
     * @param commentId 评论id
     * @param request
     */
    void addFabulous(Long userId, Long commentId, HttpServletRequest request);

    /**
     * 评论取消点赞
     * @param userId 用户id
     * @param commentId 评论id
     */
    void cancelFabulous(Long userId, Long commentId);
}