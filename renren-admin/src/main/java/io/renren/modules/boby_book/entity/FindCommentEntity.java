package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 发现数据评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_find_comment")
public class FindCommentEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 发现数据id
     */
    private Long findId;
    /**
     * 用户id(评论者)
     */
    private Long userId;
    /**
     * 评论内容
     */
    private String findComment;
    /**
     * 评论时间
     */
    private Date findCommentTime;
    /**
     * 用户ip(评论者)
     */
    private String userIp;
    /**
     * 回复次数
     */
    private Integer replayCount;
    /**
     * 点赞次数
     */
    private Integer fabulousCount;

    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;

    /**
     * 发现信息
     */
    @TableField(exist = false)
    FindEntity find;
    /**
     * 用户信息
     */
    @TableField(exist = false)
    UserEntity user;
}