package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommodityBaseAttrInfoDTO;
import io.renren.modules.boby_book.entity.CommodityBaseAttrInfoEntity;

/**
 * 属性表(用作于查询)
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommodityBaseAttrInfoService extends CrudService<CommodityBaseAttrInfoEntity, CommodityBaseAttrInfoDTO> {

}