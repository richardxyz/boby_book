package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.FindCommentDTO;
import io.renren.modules.boby_book.entity.FindCommentEntity;

import java.util.List;

/**
 * 发现数据评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface FindCommentService extends CrudService<FindCommentEntity, FindCommentDTO> {
    /**
     * 根据id查询发现评论信息
     *
     * @param findId 发现数据id
     * @return
     */
    List<FindCommentEntity> selectCommentByFindId(Long findId);

    /**
     * 添加评论
     *
     * @param findId  发现数据id
     * @param fuid    评论用户id
     * @param content 评论内容
     * @param ip      评论用户ip
     * @return
     */
    void addFindComment(Long findId, Long fuid, String content, String ip);

    /**
     * 根据评论id查询评论信息
     *
     * @param id 评论id
     * @return
     */
    FindCommentEntity selectFindCommentById(Long id);
}