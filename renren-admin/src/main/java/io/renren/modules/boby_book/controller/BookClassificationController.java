package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.BookClassificationDTO;
import io.renren.modules.boby_book.entity.BookClassificationEntity;
import io.renren.modules.boby_book.excel.BookClassificationExcel;
import io.renren.modules.boby_book.service.BookClassificationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 书籍分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/bookclassification")
@Api(tags = "书籍分类")
public class BookClassificationController {

    @Autowired
    private BookClassificationService classificationService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:bookclassification:page")
    public Result<PageData<BookClassificationDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<BookClassificationDTO> page = classificationService.page(params);

        return new Result<PageData<BookClassificationDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:bookclassification:info")
    public Result<BookClassificationDTO> get(@PathVariable("id") Long id) {
        BookClassificationDTO data = classificationService.get(id);

        return new Result<BookClassificationDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:bookclassification:save")
    public Result save(@RequestBody BookClassificationDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        classificationService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:bookclassification:update")
    public Result update(@RequestBody BookClassificationDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        classificationService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:bookclassification:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        classificationService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:bookclassification:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<BookClassificationDTO> list = classificationService.list(params);
        ExcelUtils.exportExcelToTarget(response, null, list, BookClassificationExcel.class);
    }

    /**
     * 查询所有书分类
     *
     * @return
     */
    @PostMapping("/findAllClassification")
    public Result findAllClassification() {
        List<BookClassificationEntity> list = classificationService.findAllClassification();
        return new Result().ok(list);
    }

}
