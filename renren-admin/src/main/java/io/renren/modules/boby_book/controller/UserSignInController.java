package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.IpUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.UserSignInDTO;
import io.renren.modules.boby_book.entity.UserEntity;
import io.renren.modules.boby_book.entity.UserSignInEntity;
import io.renren.modules.boby_book.entity.UserSpecificInformationEntity;
import io.renren.modules.boby_book.excel.UserSignInExcel;
import io.renren.modules.boby_book.service.UserService;
import io.renren.modules.boby_book.service.UserSignInService;
import io.renren.modules.boby_book.service.UserSpecificInformationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 用户签到表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/usersignin")
@Api(tags = "用户订阅")
public class UserSignInController {
    @Autowired
    private UserSignInService signInService;

    @Autowired
    private UserService userService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:usersignin:page")
    public Result<PageData<UserSignInDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<UserSignInDTO> page = signInService.page(params);

        return new Result<PageData<UserSignInDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:usersignin:info")
    public Result<UserSignInDTO> get(@PathVariable("id") Long id) {
        UserSignInDTO data = signInService.get(id);

        return new Result<UserSignInDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:usersignin:save")
    public Result save(@RequestBody UserSignInDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        signInService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:usersignin:update")
    public Result update(@RequestBody UserSignInDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        signInService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:usersignin:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        signInService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:usersignin:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<UserSignInDTO> list = signInService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, UserSignInExcel.class);
    }

    /**
     * 用户打开签到页面
     *
     * @param userid 用户id
     * @return
     */
    @PostMapping("openSignIn")
    public Result openSignIn(Long userid) {
        System.err.println("openSignIn:用户id：" + userid);
        UserEntity entity = userService.selectById(userid);
        Calendar calendar = Calendar.getInstance();
        Date date = signInService.lastSignIn(userid);
        int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (day == 1) {
            userService.updateSignIn();// 修改签到状态
            return new Result().ok(entity);
        } else {
            return new Result().ok(entity);
        }
    }

    /**
     * 获取今天星期几
     *
     * @return
     */
    @PostMapping("getWeekDay")
    public Result getWeekDay() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        return new Result().ok(day);
    }


    @Autowired
    private UserSpecificInformationService specificInformationService;

    /**
     * 用户签到,添加签到天数
     *
     * @param userid   用户id
     * @param day      星期几
     * @param integral 积分
     * @param request
     * @return
     */
    @PostMapping("updateSignInNum")
    public Result updateSignInNum(Long userid, Integer day, Integer integral, HttpServletRequest request) {
        UserEntity entity = userService.selectById(userid);
        System.err.println("用户id：" + userid);
        System.err.println("星期：" + day);
        String signIn = entity.getSignIn();
        String[] split = signIn.split(",");
        Integer index = day - 1;
        split[index] = "1";
        String signInState = StringUtils.join(split, ",");// 数组转字符串(逗号分隔)(推荐)
        System.err.println(signInState);
        entity.setSignIn(signInState);
        userService.updateSignInState(signInState, entity.getId());// 修改签到状态
        String ip = IpUtils.getIpAddr(request); //ip地址
        UserSignInEntity signIn2Entity = new UserSignInEntity();
        signIn2Entity.setUserId(entity.getId());
        signIn2Entity.setUserIp(ip);
        signIn2Entity.setAttendanceTime(new Date());
        signInService.insert(signIn2Entity);//添加签到记录
        Integer signInNum = entity.getSignInNum();
        UserSpecificInformationEntity userSpecificInformation2Entity = specificInformationService.selectUserSpecificInformationById((entity.getId()));
        Integer integral2 = userSpecificInformation2Entity.getIntegral();
        Integer num = signInNum + 1;//签到次数
        Integer integralNum = integral2 + integral;//签到积分
        userService.updateSignInNum(num, entity.getId());//添加签到次数
        specificInformationService.updateIntegralNum(integralNum, userSpecificInformation2Entity.getUserId());//添加签到积分
        return new Result();
    }


}