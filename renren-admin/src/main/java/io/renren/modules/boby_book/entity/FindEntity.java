package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 发现数据
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_find")
public class FindEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 视频名称
     */
    private String videoName;
    /**
     * 视频封面
     */
    private String videoImg;
    /**
     * 视频URL
     */
    private String videoUrl;
    /**
     * 播放数量
     */
    private Integer playNum;
    /**
     * 评论数量
     */
    private Integer commentNum;
    /**
     * 点赞数量
     */
    private Integer fabulousNum;
    /**
     * 分享次数
     */
    private Integer shareNum;
    /**
     * 用户id(发布者)
     */
    private Long userId;
    /**
     * 上传时间
     */
    private Date uploadTime;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;

    /**
     * 用户信息
     */
    @TableField(exist = false)
    UserEntity user;
}