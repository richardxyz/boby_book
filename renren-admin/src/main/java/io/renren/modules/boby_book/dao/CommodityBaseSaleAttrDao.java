package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.CommodityBaseSaleAttrEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 销售属性名称
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Mapper
public interface CommodityBaseSaleAttrDao extends BaseDao<CommodityBaseSaleAttrEntity> {
	
}