package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.IpUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.CommodityCommentDTO;
import io.renren.modules.boby_book.entity.CommodityCommentEntity;
import io.renren.modules.boby_book.entity.CommodityCommentReplyEntity;
import io.renren.modules.boby_book.excel.CommodityCommentExcel;
import io.renren.modules.boby_book.service.CommodityCommentReplyService;
import io.renren.modules.boby_book.service.CommodityCommentService;
import io.renren.modules.boby_book.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 商品评价
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@RestController
@RequestMapping("boby_book/commoditycomment")
@Api(tags = "商品评价")
public class CommodityCommentController {

    @Autowired
    private CommodityCommentService commentService;
    @Autowired
    private CommodityCommentReplyService commentReplyService;
    @Autowired
    private IpUtils ipUtils;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:commoditycomment:page")
    public Result<PageData<CommodityCommentDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<CommodityCommentDTO> page = commentService.page(params);

        return new Result<PageData<CommodityCommentDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:commoditycomment:info")
    public Result<CommodityCommentDTO> get(@PathVariable("id") Long id) {
        CommodityCommentDTO data = commentService.get(id);

        return new Result<CommodityCommentDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:commoditycomment:save")
    public Result save(@RequestBody CommodityCommentDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        commentService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:commoditycomment:update")
    public Result update(@RequestBody CommodityCommentDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        commentService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:commoditycomment:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        commentService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:commoditycomment:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<CommodityCommentDTO> list = commentService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, CommodityCommentExcel.class);
    }

    /**
     * 添加评论
     *
     * @param cid     商品id
     * @param fuid    评论用户id
     * @param content 评论内容
     * @param request
     * @return
     */
    @PostMapping("/saveComment")
    public Result saveComment(Long cid, Long fuid, String content, HttpServletRequest request) {
        String ip = ipUtils.getIpAddr(request);
        commentService.saveComment(cid, fuid, content, ip);
        return new Result().ok("评论成功");
    }

    /**
     * 添加评论回复
     *
     * @param cid     目标评论id
     * @param fuid    回复用户id
     * @param tuid    目标用户id
     * @param content 评论内容
     * @param request
     * @return
     */
    @PostMapping("/saveCommentReply")
    public Result saveCommentReply(Long cid, Long fuid, Long tuid, String content, HttpServletRequest request) {
        String ip = ipUtils.getIpAddr(request);
        commentReplyService.saveCommentReply(cid, fuid, tuid, content, ip);
        return new Result().ok("评论回复成功");
    }


    /**
     * 查询最新商品评价信息
     *
     * @param id 商品id
     * @return
     */
    @PostMapping("/selectCommentList")
    public Result selectCommentList(Long id) {
        List<CommodityCommentEntity> list = commentService.selectCommentList(id);
        return new Result().ok(list);
    }


    /**
     * 根据id查询商品评价信息
     *
     * @param id 商品id
     * @return
     */
    @PostMapping("/selectCommentById")
    public Result selectCommentById(Long id) {
        List<CommodityCommentEntity> entityList = commentService.selectCommentById(id);
        return new Result().ok(entityList);
    }

    /**
     * 根据id查询商品评价回复信息
     *
     * @param id 商品id
     * @return
     */
    @PostMapping("/selectCommentReplyById")
    public Result selectCommentReplyById(Long id) {
        List<CommodityCommentReplyEntity> entityList = commentReplyService.selectCommentReplyById(id);
        return new Result().ok(entityList);
    }

}