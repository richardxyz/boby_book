package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户下载
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper=false)
@TableName("xux_user_download")
public class UserDownloadEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 书籍章节id
	 */
	private Long bookChapterId;
	/**
	 * 书籍id
	 */
	private Long bookId;
	/**
	 * 下载时间
	 */
	private Date downlopadTime;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 更新者
	 */
	private Long updater;
	/**
	 * 更新时间
	 */
	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date updateDate;

	/**
	 * 图书信息
	 */
	@TableField(exist = false)
	private BookEntity book;
	/**
	 * 图书章节
	 */
	@TableField(exist = false)
	private BookChapterEntity chapter;
}