package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 图书章节信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class BookChapterExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "图书名称")
    private String bookName;
    @Excel(name = "图书章节名称")
    private String chapterName;
    @Excel(name = "图书章节内容")
    private String chapterContent;
    @Excel(name = "图书章节图片")
    private String chapterImg;
    @Excel(name = "图书播放数量")
    private Integer bookPlayNums;
    @Excel(name = "图书收藏数量")
    private Integer bookCollectionNum;
    @Excel(name = "图书评论数量")
    private Integer bookCommentNum;
    @Excel(name = "书籍id")
    private Long bookId;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}