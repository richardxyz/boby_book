package io.renren.modules.boby_book.service;

import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.BookClassificationDTO;
import io.renren.modules.boby_book.entity.BookClassificationEntity;

import java.util.List;

/**
 * 书籍分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface BookClassificationService extends CrudService<BookClassificationEntity, BookClassificationDTO> {

    /**
     * 查询所有书分类
     *
     * @return
     */
    List<BookClassificationEntity> findAllClassification();
}
