package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.WeChatUtils;
import io.renren.modules.boby_book.dao.FindCommentDao;
import io.renren.modules.boby_book.dao.FindCommentReplyDao;
import io.renren.modules.boby_book.dto.FindCommentReplyDTO;
import io.renren.modules.boby_book.entity.FindCommentEntity;
import io.renren.modules.boby_book.entity.FindCommentReplyEntity;
import io.renren.modules.boby_book.service.FindCommentReplyService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 发现评论回复
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@Service
@Transactional
public class FindCommentReplyServiceImpl extends CrudServiceImpl<FindCommentReplyDao, FindCommentReplyEntity, FindCommentReplyDTO> implements FindCommentReplyService {

    @Override
    public QueryWrapper<FindCommentReplyEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<FindCommentReplyEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private FindCommentDao findCommentDao;
    @Autowired
    private FindCommentReplyDao findCommentReplyDao;

    @Override
    public void addFindCommentReply(Long cid, Long fuid, Long tuid, String content, String ip) {
        String cnAa = WeChatUtils.changeCharacter(content);
        FindCommentReplyEntity entity = new FindCommentReplyEntity();
        entity.setFindCommentId(cid);
        entity.setContent(cnAa);
        entity.setReplyTime(new Date());
        entity.setFromUid(fuid);
        entity.setToUid(tuid);
        entity.setFromUip(ip);
        findCommentReplyDao.insert(entity);
        FindCommentEntity findComment2Entity = findCommentDao.selectById(cid);
        Integer replayCount = findComment2Entity.getReplayCount();
        replayCount++;
        findComment2Entity.setReplayCount(replayCount);
        findCommentDao.updateById(findComment2Entity);
    }

    @Override
    public List<FindCommentReplyEntity> selectFindCommentReplyByCommentId(Long id) {
        return findCommentReplyDao.selectFindCommentReplyByCommentId(id);
    }
}