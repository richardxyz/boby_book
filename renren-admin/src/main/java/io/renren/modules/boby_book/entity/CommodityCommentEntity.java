package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户评论
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_commodity_comment")
public class CommodityCommentEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 商品id
     */
    private Long commodityId;
    /**
     * 评价的ip
     */
    private String memberIp;
    /**
     * 评价的时间
     */
    private Date createTime;
    /**
     * 评价内容
     */
    private String content;
    /**
     * 评论用户id
     */
    private Long fromUid;
    /**
     * 回复次数
     */
    private Integer replayCount;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;


    /**
     * 商品信息
     */
    @TableField(exist = false)
    CommoditySpuInfoEntity commoditySpuInfo;
    /**
     * 用户信息
     */
    @TableField(exist = false)
    UserEntity user;

}