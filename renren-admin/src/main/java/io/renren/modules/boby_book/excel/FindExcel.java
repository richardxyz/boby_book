package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 发现数据
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class FindExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "视频名称")
    private String videoName;
    @Excel(name = "视频封面")
    private String videoImg;
    @Excel(name = "视频URL")
    private String videoUrl;
    @Excel(name = "播放数量")
    private Integer playNum;
    @Excel(name = "评论数量")
    private Integer commentNum;
    @Excel(name = "点赞数量")
    private Integer fabulousNum;
    @Excel(name = "分享次数")
    private Integer shareNum;
    @Excel(name = "用户id(发布者)")
    private Long userId;
    @Excel(name = "上传时间")
    private Date uploadTime;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}