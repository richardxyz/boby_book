package io.renren.modules.boby_book.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 兑换礼品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@ApiModel(value = " 兑换礼品详情")
public class CommodityGiftDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty(value = "商品skuId")
    private Long skuId;

    @ApiModelProperty(value = "用户id")
    private Long userId;

    @ApiModelProperty(value = "兑换数量")
    private Integer exchangeNum;

    @ApiModelProperty(value = "兑换时间")
    private Date exchangeTime;

    @ApiModelProperty(value = "更新者")
    private Long updater;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;


}