package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.BookCategoryDTO;
import io.renren.modules.boby_book.entity.BookCategoryEntity;

/**
 * 图书类别
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface BookCategoryService extends CrudService<BookCategoryEntity, BookCategoryDTO> {

}