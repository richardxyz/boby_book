package io.renren.modules.boby_book.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.redis.RedisUtil;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.IpUtils;
import io.renren.modules.boby_book.dao.CommodityBrandDao;
import io.renren.modules.boby_book.dao.CommoditySkuAttrValueDao;
import io.renren.modules.boby_book.dao.CommoditySkuImageDao;
import io.renren.modules.boby_book.dao.CommoditySkuInfoDao;
import io.renren.modules.boby_book.dto.CommoditySkuInfoDTO;
import io.renren.modules.boby_book.entity.*;
import io.renren.modules.boby_book.service.CommoditySalesAttrService;
import io.renren.modules.boby_book.service.CommoditySkuInfoService;
import io.renren.modules.boby_book.service.CommoditySpuInfoService;
import io.renren.modules.boby_book.vo.SkuVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import redis.clients.jedis.Jedis;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * 商品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-11-11
 */
@Service
@Transactional
public class CommoditySkuInfoServiceImpl extends CrudServiceImpl<CommoditySkuInfoDao, CommoditySkuInfoEntity, CommoditySkuInfoDTO> implements CommoditySkuInfoService {

    @Override
    public QueryWrapper<CommoditySkuInfoEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<CommoditySkuInfoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    /**
     * sku库存信息
     */
    @Autowired
    private CommoditySkuInfoDao skuInfoDao;
    /**
     * spu商品信息
     */
    @Autowired
    private CommoditySpuInfoService spuInfoService;

    /**
     * 商品品牌
     */
    @Autowired
    private CommodityBrandDao eCommodityBrandDao;
    /**
     * 商品图片
     */
    @Autowired
    private CommoditySkuImageDao skuImageDao;
    /**
     * 商品sku平台属性值关联表
     */
    @Autowired
    private CommoditySkuAttrValueDao skuAttrValueDao;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private IpUtils ipUtils;

    /**
     * sku详细信息
     *
     * @param skuInfoEntity
     * @return
     */
    private SkuVo applyDb(CommoditySkuInfoEntity skuInfoEntity) {
        SkuVo skuVo = new SkuVo();
        BeanUtils.copyProperties(skuInfoEntity, skuVo);
        //查询评论数
        CommoditySpuInfoEntity spuInfoEntity = spuInfoService.selectSpuInfoById(skuInfoEntity.getCommodityId());
        //查询品牌名称
        CommodityBrandEntity brandEntity = eCommodityBrandDao.selectById(skuInfoEntity.getBrandId());
        skuVo.setBrandName(brandEntity.getName());
        skuVo.setCommentNum(spuInfoEntity.getCommentNum());
        //查询商品图片
        List<CommoditySkuImageEntity> skuImageEntityList = skuImageDao.selectImageById(skuInfoEntity.getId());
        skuVo.setSkuImageList(skuImageEntityList);
        //查询商品销售属性
        List<CommoditySkuAttrValueEntity> skuAttrValueEntityList = skuAttrValueDao.selectSkuAttrValue(skuInfoEntity.getId());
        skuVo.setSkuAttrValueList(skuAttrValueEntityList);
        return skuVo;
    }

    private SkuVo apply(CommoditySkuInfoEntity skuInfoEntity, String ip) {
        System.out.println("ip为" + ip + "的用户:" + Thread.currentThread().getName() + "进入的商品详情的请求");
        Long skuId = skuInfoEntity.getId();
        SkuVo skuVo = new SkuVo();
        // 链接缓存
        Jedis jedis = redisUtil.getJedis();
        // 查询缓存
        String skuKey = "sku:" + skuId + ":info";
        String skuJson = jedis.get(skuKey);

        if (StringUtils.isNotBlank(skuJson)) {//if(skuJson!=null&&!skuJson.equals(""))
            System.out.println("ip为" + ip + "的用户:" + Thread.currentThread().getName() + "从缓存中获取商品详情");
            skuVo = JSON.parseObject(skuJson, SkuVo.class);
        } else {
            // 如果缓存中没有，查询mysql
            System.out.println("ip为" + ip + "的用户:" + Thread.currentThread().getName() + "发现缓存中没有，申请缓存的分布式锁：" + "sku:" + skuId + ":lock");

            // 设置分布式锁
            String token = UUID.randomUUID().toString();
            String OK = jedis.set("sku:" + skuId + ":lock", token, "nx", "px", 10 * 1000);// 拿到锁的线程有10秒的过期时间
            System.err.println(OK);
            if (StringUtils.isNotBlank(OK) && OK.equals("OK")) {
                // 设置成功，有权在10秒的过期时间内访问数据库
                System.out.println("ip为" + ip + "的用户:" + Thread.currentThread().getName() + "有权在10秒的过期时间内访问数据库：" + "sku:" + skuId + ":lock");
                skuVo = applyDb(skuInfoEntity);
                if (skuVo != null) {
                    // mysql查询结果存入redis
                    jedis.set("sku:" + skuId + ":info", JSON.toJSONString(skuVo));
                } else {
                    // 数据库中不存在该sku
                    // 为了防止缓存穿透将，null或者空字符串值设置给redis
                    jedis.setex("sku:" + skuId + ":info", 60 * 3, JSON.toJSONString(""));
                }
                // 在访问mysql后，将mysql的分布锁释放
                System.out.println("ip为" + ip + "的用户:" + Thread.currentThread().getName() + "使用完毕，将锁归还：" + "sku:" + skuId + ":lock");
                String lockToken = jedis.get("sku:" + skuId + ":lock");
                if (StringUtils.isNotBlank(lockToken) && lockToken.equals(token)) {
                    //jedis.eval("lua");可与用lua脚本，在查询到key的同时删除该key，防止高并发下的意外的发生
                    jedis.del("sku:" + skuId + ":lock");// 用token确认删除的是自己的sku的锁
                }
            } else {
                // 设置失败，自旋（该线程在睡眠几秒后，重新尝试访问本方法）
                System.out.println("ip为" + ip + "的用户:" + Thread.currentThread().getName() + "没有拿到锁，开始自旋");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return apply(skuVo, ip);
            }
        }
        jedis.close();
        return skuVo;
    }

    @Override
    public List<SkuVo> findCommoditySkuInfoAll() {
        List<CommoditySkuInfoEntity> spuInfoList = skuInfoDao.selectList(null);
        List<SkuVo> skuVoList = spuInfoList.stream().map(this::applyDb).collect(Collectors.toList());
        return skuVoList;
    }

    @Override
    public List<SkuVo> findCommodityBycommodityId(Long commodityId) {
        List<CommoditySkuInfoEntity> spuInfoList = skuInfoDao.findCommodityBycommodityId(commodityId);
        List<SkuVo> skuVoList = spuInfoList.stream().map(this::applyDb).collect(Collectors.toList());
        return skuVoList;
    }


    @Override
    public List<CommoditySkuInfoEntity> getSkuSaleAttrValueListBySpu(Long commodityId) {
        List<CommoditySkuInfoEntity> pmsSkuInfos = skuInfoDao.selectSkuSaleAttrValueListBySpu(commodityId);

        return pmsSkuInfos;
    }


    /**
     * 商品销售属性信息
     */
    @Autowired
    private CommoditySalesAttrService salesAttrService;
    /**
     * 商品库存属性信息
     */
    @Autowired
    private CommoditySkuInfoService skuInfoService;

    @Override
    public ModelMap selectSkuById(Long skuid, HttpServletRequest request) {
        String ip = ipUtils.getIpAddr(request);
        ModelMap map = new ModelMap();
        //商品信息
        CommoditySkuInfoEntity skuInfoEntity = skuInfoDao.selectById(skuid);
        SkuVo skuVo = this.apply(skuInfoEntity, ip);
//        SkuVo skuVo = this.applyDb(skuInfoEntity);
        //sku对象
        map.put("skuInfoEntity", skuVo);
        //销售属性列表
        List<CommoditySalesAttrEntity> salesAttrEntityList = salesAttrService.spuSaleAttrListCheckBySku(skuVo.getCommodityId(), skuVo.getId());// 1，1
        map.put("spuSaleAttrListCheckBySku", salesAttrEntityList);
        // 查询当前sku的spu的其他sku的集合的hash表
        Map<String, Long> skuSaleAttrHash = new HashMap<>();
        List<CommoditySkuInfoEntity> pmsSkuInfos = skuInfoService.getSkuSaleAttrValueListBySpu(skuVo.getCommodityId());
        for (CommoditySkuInfoEntity skuInfo : pmsSkuInfos) {
            String k = "";
            Long v = skuInfo.getId();
            List<CommoditySkuSaleAttrValueEntity> skuSaleAttrValueList = skuInfo.getSkuSaleAttrValueList();
            for (CommoditySkuSaleAttrValueEntity pmsSkuSaleAttrValue : skuSaleAttrValueList) {
                k += pmsSkuSaleAttrValue.getSaleAttrValueId() + "|";// "39|45"
            }
            skuSaleAttrHash.put(k, v);
        }
        // 将sku的销售属性hash表放到页面
        String skuSaleAttrHashJsonStr = JSON.toJSONString(skuSaleAttrHash);
        map.put("skuSaleAttrHashJsonStr", skuSaleAttrHashJsonStr);
//        System.err.println(skuSaleAttrHashJsonStr);
        return map;

    }

    @Override
    public void updateExchangeNumById(Integer exchangeNum, Long id) {
        skuInfoDao.updateExchangeNumById(exchangeNum, id);
    }

    @Override
    public boolean checkPrice(Long skuId, Double skuMoney) {
        boolean b = false;
        CommoditySkuInfoEntity skuInfoEntity = skuInfoService.selectById(skuId);

        Double price = skuInfoEntity.getPrice();

        if (price.compareTo(skuMoney) == 0) {
            b = true;
        }

        return b;
    }
}