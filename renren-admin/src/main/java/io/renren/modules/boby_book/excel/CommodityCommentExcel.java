package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 商品评价
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommodityCommentExcel {
    @Excel(name = "评价id")
    private Long id;
    @Excel(name = "商品id")
    private Long commodityId;
    @Excel(name = "评价的ip")
    private String memberIp;
    @Excel(name = "评价的时间")
    private Date createTime;
    @Excel(name = "评价内容")
    private String content;
    @Excel(name = "评论用户id")
    private Long fromUid;
    @Excel(name = "回复次数")
    private Integer replayCount;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}