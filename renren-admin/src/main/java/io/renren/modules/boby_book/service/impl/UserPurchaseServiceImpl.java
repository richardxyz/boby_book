package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.UserPurchaseDao;
import io.renren.modules.boby_book.dto.UserPurchaseDTO;
import io.renren.modules.boby_book.entity.BookChapterEntity;
import io.renren.modules.boby_book.entity.BookEntity;
import io.renren.modules.boby_book.entity.UserPurchaseEntity;
import io.renren.modules.boby_book.service.UserPurchaseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 用户购买
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class UserPurchaseServiceImpl extends CrudServiceImpl<UserPurchaseDao, UserPurchaseEntity, UserPurchaseDTO> implements UserPurchaseService {

    @Override
    public QueryWrapper<UserPurchaseEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<UserPurchaseEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private UserPurchaseDao purchaseDao;

    @Override
    public List<BookEntity> selectPurchaseBook(Long id) {
        return purchaseDao.selectPurchaseBook(id);
    }

    @Override
    public List<BookChapterEntity> selectPurchaseBookChapter(Long bookid, Long userid) {
        return purchaseDao.selectPurchaseBookChapter(bookid, userid);
    }

}