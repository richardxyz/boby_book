package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.CommoditySkuInfoDTO;
import io.renren.modules.boby_book.excel.CommoditySkuInfoExcel;
import io.renren.modules.boby_book.service.CommoditySkuInfoService;
import io.renren.modules.boby_book.vo.SkuVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 商品详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
@RestController
@RequestMapping("boby_book/commodityskuinfo")
@Api(tags = "商品详情")
public class CommoditySkuInfoController {

    @Autowired
    private CommoditySkuInfoService skuInfoService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:commodityskuinfo:page")
    public Result<PageData<CommoditySkuInfoDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<CommoditySkuInfoDTO> page = skuInfoService.page(params);

        return new Result<PageData<CommoditySkuInfoDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:commodityskuinfo:info")
    public Result<CommoditySkuInfoDTO> get(@PathVariable("id") Long id) {
        CommoditySkuInfoDTO data = skuInfoService.get(id);

        return new Result<CommoditySkuInfoDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:commodityskuinfo:save")
    public Result save(@RequestBody CommoditySkuInfoDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        skuInfoService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:commodityskuinfo:update")
    public Result update(@RequestBody CommoditySkuInfoDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        skuInfoService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:commodityskuinfo:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        skuInfoService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:commodityskuinfo:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<CommoditySkuInfoDTO> list = skuInfoService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, CommoditySkuInfoExcel.class);
    }

    /**
     * 查询所有商品
     *
     * @return
     */
    @PostMapping("/findCommoditySkuInfoAll")
    public Result findCommoditySkuInfoAll() {
        List<SkuVo> list = skuInfoService.findCommoditySkuInfoAll();

        return new Result().ok(list);
    }

    /**
     * 根据分类id查询商品
     *
     * @param commodityId 商品id
     * @return
     */
    @PostMapping("/findCommodityBycommodityId")
    public Result findCommodityBycommodityId(Long commodityId) {
        List<SkuVo> CommodityList = skuInfoService.findCommodityBycommodityId(commodityId);
        return new Result().ok(CommodityList);
    }

    /**
     * 根据id查询商品详细信息
     *
     * @param skuid   商品库存id
     * @param request
     * @return
     */
    @PostMapping("/selectCommodityById")
    public Result selectCommodityById(Long skuid, HttpServletRequest request) {
        ModelMap map = skuInfoService.selectSkuById(skuid, request);
        return new Result().ok(map);
    }


}