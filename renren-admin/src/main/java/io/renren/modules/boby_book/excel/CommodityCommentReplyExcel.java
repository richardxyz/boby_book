package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 评价回复
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommodityCommentReplyExcel {
    @Excel(name = "评论回复id")
    private Long id;
    @Excel(name = "目标评论id")
    private Long commentId;
    @Excel(name = "回复内容")
    private String content;
    @Excel(name = "回复时间")
    private Date replyTime;
    @Excel(name = "回复用户id")
    private Long fromUid;
    @Excel(name = "目标用户id")
    private Long toUid;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}