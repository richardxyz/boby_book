package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserCollectionDTO;
import io.renren.modules.boby_book.entity.UserCollectionEntity;

import java.util.List;

/**
 * 用户收藏
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserCollectionService extends CrudService<UserCollectionEntity, UserCollectionDTO> {

    /**
     * 查询我的收藏
     *
     * @param id 用户id
     * @return
     */
    List<UserCollectionEntity> selectCollectionBook(Long id);
}