package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.UserPlayDTO;
import io.renren.modules.boby_book.entity.UserPlayEntity;

import java.util.List;

/**
 * 用户最近播放
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface UserPlayService extends CrudService<UserPlayEntity, UserPlayDTO> {
    /**
     * 查询今日播放
     *
     * @param id 用户id
     * @return
     */
    List<UserPlayEntity> selectTodayBook(Long id);

    /**
     * 查询昨日播放
     *
     * @param id 用户id
     * @return
     */
    List<UserPlayEntity> selectYesterdayBook(Long id);

    /**
     * 查询更早播放
     *
     * @param id 用户id
     * @return
     */
    List<UserPlayEntity> selecEarlierBook(Long id);

    /**
     * 添加入播放列表
     *
     * @param userId 用户id
     * @param bookId 图书id
     */
    void addToPlay(Long userId, Long bookId);

    /**
     * 查询当前图书是否已加入播放列表
     *
     * @param userId 用户id
     * @param bookId 图书id
     * @return
     */
    Integer lastPlay(Long userId, Long bookId);
}