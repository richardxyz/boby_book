package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.UserFollowEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户关注
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserFollowDao extends BaseDao<UserFollowEntity> {
    /**
     * 删除关注数据
     *
     * @param fuId 用户id
     * @param tuId 所关注用户id
     */
    @Delete("DELETE FROM e_user_follow WHERE user_id = #{user_id} AND follow_user_id = #{follow_user_id}")
    void cancelFollowUser(@Param("user_id") Long fuId, @Param("follow_user_id") Long tuId);

    /**
     * 查询用户所有关注信息
     *
     * @param userId 用户id
     * @return
     */
    List<UserFollowEntity> selectFollow(@Param("user_id") Long userId);
}