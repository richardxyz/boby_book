package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 用户充值宝豆记录表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class UserBaoBeanRechargeExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "用户id")
    private Long userId;
    @Excel(name = "充值金额")
    private Integer rechargeNum;
    @Excel(name = "充值时间")
    private Date rechargeTime;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}