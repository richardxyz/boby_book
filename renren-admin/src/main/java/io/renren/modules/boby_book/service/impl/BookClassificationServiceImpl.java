package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.BookClassificationDao;
import io.renren.modules.boby_book.dto.BookClassificationDTO;
import io.renren.modules.boby_book.entity.BookClassificationEntity;
import io.renren.modules.boby_book.service.BookClassificationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 书籍分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class BookClassificationServiceImpl extends CrudServiceImpl<BookClassificationDao, BookClassificationEntity, BookClassificationDTO> implements BookClassificationService {

    @Override
    public QueryWrapper<BookClassificationEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<BookClassificationEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private BookClassificationDao eClassificationDao;

    @Override
    public List<BookClassificationEntity> findAllClassification() {
        return eClassificationDao.findAllClassification();
    }
}