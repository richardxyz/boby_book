package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.FindFabulousEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 发现点赞表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-20
 */
@Mapper
public interface FindFabulousDao extends BaseDao<FindFabulousEntity> {

    /**
     * 根据评论id判断点赞状态
     *
     * @param userId
     * @param findId
     * @return
     */
    @Select("SELECT COUNT(*) FROM e_find_fabulous  WHERE  from_uid = #{from_uid} AND find_id = #{find_id}")
    Integer judgeFindFabulousStatus(@Param("from_uid") Long userId, @Param("find_id") Long findId);

    /**
     * 根据userId和findId查询点赞数据
     *
     * @param userId
     * @param findId
     * @return
     */
    @Select("SELECT * FROM e_find_fabulous  WHERE  from_uid = #{from_uid} AND find_id = #{find_id}")
    FindFabulousEntity selectFindFabulousByUserIdAndCommentId(@Param("from_uid") Long userId, @Param("find_id") Long findId);
}