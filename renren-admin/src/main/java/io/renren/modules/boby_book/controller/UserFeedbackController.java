package io.renren.modules.boby_book.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.boby_book.dto.UserFeedbackDTO;
import io.renren.modules.boby_book.excel.UserFeedbackExcel;
import io.renren.modules.boby_book.service.UserFeedbackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 意见反馈
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/userfeedback")
@Api(tags = "意见反馈")
public class UserFeedbackController {

    @Autowired
    private UserFeedbackService feedbackService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query", required = true, dataType = "int"),
            @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType = "String")
    })
    @RequiresPermissions("boby_book:userfeedback:page")
    public Result<PageData<UserFeedbackDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params) {
        PageData<UserFeedbackDTO> page = feedbackService.page(params);

        return new Result<PageData<UserFeedbackDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("boby_book:userfeedback:info")
    public Result<UserFeedbackDTO> get(@PathVariable("id") Long id) {
        UserFeedbackDTO data = feedbackService.get(id);

        return new Result<UserFeedbackDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("boby_book:userfeedback:save")
    public Result save(@RequestBody UserFeedbackDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        feedbackService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("boby_book:userfeedback:update")
    public Result update(@RequestBody UserFeedbackDTO dto) {
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        feedbackService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("boby_book:userfeedback:delete")
    public Result delete(@RequestBody Long[] ids) {
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        feedbackService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("boby_book:userfeedback:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<UserFeedbackDTO> list = feedbackService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, UserFeedbackExcel.class);
    }

    /**
     * 添加意见反馈
     *
     * @param userId          用户id
     * @param feedbackContent 反馈内容
     * @param request
     * @return
     */
    @PostMapping("/addFeedback")
    public Result addFeedback(Long userId, String feedbackContent, HttpServletRequest request) {
        feedbackService.addFeedback(userId, feedbackContent, request);
        return new Result().ok("反馈成功");
    }

    /**
     * 添加意见反馈截图
     *
     * @param userId  用户id
     * @param request
     * @return
     */
    @PostMapping("/uploadFeedbackImg")
    public Result uploadFeedbackImg(Long userId, HttpServletRequest request) {
        MultipartHttpServletRequest req = (MultipartHttpServletRequest) request;
        MultipartFile multipartFile = req.getFile("file");
//        String filePath = UploadUtil.uploadImage(multipartFile);
        String filePath = "http://www.yejs.com.cn/upload/2005413135717717.jpg";
        System.out.println("uploadFeedbackImg用户id：" + userId);
        feedbackService.uploadFeedbackImg(filePath, userId);
        return new Result().ok("图片上传成功");
    }
}