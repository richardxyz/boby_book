package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 书籍分类中间表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class BookClassificationCategoryExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "分类id")
    private Long classificationId;
    @Excel(name = "类别id")
    private Long categoryId;
    @Excel(name = "分类信息")
    private String classificationVal;
    @Excel(name = "类别信息")
    private String categoryVal;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}