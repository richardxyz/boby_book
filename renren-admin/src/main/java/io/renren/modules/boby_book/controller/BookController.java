package io.renren.modules.boby_book.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.common.annotation.LogOperation;
import io.renren.common.utils.Result;
import io.renren.modules.boby_book.entity.BookEntity;
import io.renren.modules.boby_book.service.BookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 图书信息
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@AllArgsConstructor
@RequestMapping("boby_book/book")
@Api(value = "boby_book", tags = "图书信息")
public class BookController {
    private final BookService bookService;

    /**
     * 分页列表
     *
     * @param page       分页对象
     * @param bookEntity 序列
     * @return
     */
    @ApiOperation("图书信息分页列表")
    @LogOperation("图书信息分页列表")
    @GetMapping("/page")
    public Result getPage(Page page, BookEntity bookEntity) {
        return new Result().ok(bookService.page(page, Wrappers.query(bookEntity)));
    }

    /**
     * 图书信息查询
     *
     * @param id
     * @return R
     */
    @ApiOperation("图书信息查询")
    @LogOperation("图书信息查询")
    @GetMapping("/{id}")
    public R getById(@PathVariable("id") String id) {
        return R.ok(bookService.getById(id));
    }

    /**
     * 图书信息新增
     *
     * @param bookEntity 图书信息
     * @return R
     */
    @ApiOperation("图书信息新增")
    @LogOperation("图书信息新增")
    @PostMapping
    public R save(@RequestBody BookEntity bookEntity) {
        return R.ok(bookService.save(bookEntity));
    }

    /**
     * 图书信息修改
     *
     * @param bookEntity 图书信息
     * @return R
     */
    @ApiOperation("图书信息修改")
    @LogOperation("图书信息修改")
    @PutMapping
    public R updateById(@RequestBody BookEntity bookEntity) {
        return R.ok(bookService.updateById(bookEntity));
    }

    /**
     * 图书信息删除
     *
     * @param id
     * @return R
     */
    @ApiOperation("图书信息删除")
    @LogOperation("图书信息删除")
    @DeleteMapping("/{id}")
    public R removeById(@PathVariable String id) {
        return R.ok(bookService.removeById(id));
    }


}