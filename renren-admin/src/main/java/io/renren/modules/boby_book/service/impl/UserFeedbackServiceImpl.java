package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.IpUtils;
import io.renren.modules.boby_book.dao.UserFeedbackDao;
import io.renren.modules.boby_book.dto.UserFeedbackDTO;
import io.renren.modules.boby_book.entity.UserFeedbackEntity;
import io.renren.modules.boby_book.service.UserFeedbackService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * 意见反馈
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Service
@Transactional
public class UserFeedbackServiceImpl extends CrudServiceImpl<UserFeedbackDao, UserFeedbackEntity, UserFeedbackDTO> implements UserFeedbackService {

    @Override
    public QueryWrapper<UserFeedbackEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<UserFeedbackEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private UserFeedbackDao feedbackDao;

    @Autowired
    private IpUtils ipUtils;

    @Override
    public void addFeedback(Long userId, String feedbackContent, HttpServletRequest request) {
        String ip = ipUtils.getIpAddr(request);
        UserFeedbackEntity entity = new UserFeedbackEntity();
        entity.setUserId(userId);
        entity.setUserIp(ip);
        entity.setFeedbackImage("");
        entity.setFeedbackContent(feedbackContent);
        entity.setFeedbackTime(new Date());
        feedbackDao.insert(entity);
    }

    @Override
    public void uploadFeedbackImg(String filePath, Long userId) {
        System.out.println("filePath：" + filePath);
        UserFeedbackEntity entity = feedbackDao.findLastFeedBack(userId);
        if (entity.equals("") || entity == null) {
            feedbackDao.uploadFeedbackImg(filePath, userId);
        }
        String feedbackImage = entity.getFeedbackImage();
        if (feedbackImage == "" || feedbackImage.equals("")) {
            feedbackDao.uploadFeedbackImg(filePath, userId);
        } else {
            String filePaths = feedbackImage + ";" + filePath;
            feedbackDao.uploadFeedbackImg(filePaths, userId);
        }


    }
}