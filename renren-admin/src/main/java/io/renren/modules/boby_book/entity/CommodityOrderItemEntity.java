package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 订单详情
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_commodity_order_item")
public class CommodityOrderItemEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 订单id
     */
    private Long orderId;
    /**
     * 订单编号
     */
    private String orderSn;
    /**
     * 商品id(对应spu_id)
     */
    private Long commodityId;
    /**
     * 商品图片
     */
    private String commodityPic;
    /**
     * 商品名称
     */
    private String commodityName;
    /**
     * 品牌
     */
    private String commodityBrand;
    /**
     * 商品编号（仓库）
     */
    private String commoditySn;
    /**
     * 销售价格(所需积分)
     */
    private Integer commodityPrice;
    /**
     * 购买数量
     */
    private Integer commodityQuantity;
    /**
     * 商品sku编号
     */
    private Long commoditySkuId;
    /**
     * 商品sku条码
     */
    private String commoditySkuCode;
    /**
     * 商品分类id
     */
    private Long commodityCategoryId;
    /**
     * 商品的销售属性1
     */
    private String sp1;
    /**
     * 商品的销售属性2
     */
    private String sp2;
    /**
     * 商品的销售属性3
     */
    private String sp3;
    /**
     * 商品销售属性:[{"key":"颜色","value":"颜色"},{"key":"容量","value":"4G"}]
     */
    private String commodityAttr;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;

    /**
     * 商品信息
     */
    @TableField(exist = false)
    CommoditySkuInfoEntity commoditySkuInfo;


}