package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 用户收藏
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_user_collection")
public class UserCollectionEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 书籍id
     */
    private Long bookId;
    /**
     * 收藏时间
     */
    private String collectionTime;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;

    /**
     * 图书信息
     */
    @TableField(exist = false)
    private BookEntity book;
}