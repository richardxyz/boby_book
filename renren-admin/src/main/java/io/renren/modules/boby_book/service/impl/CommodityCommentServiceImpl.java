package io.renren.modules.boby_book.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.boby_book.dao.CommodityCommentDao;
import io.renren.modules.boby_book.dto.CommodityCommentDTO;
import io.renren.modules.boby_book.entity.CommodityCommentEntity;
import io.renren.modules.boby_book.service.CommodityCommentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 商品评价
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-11-11
 */
@Service
@Transactional
public class CommodityCommentServiceImpl extends CrudServiceImpl<CommodityCommentDao, CommodityCommentEntity, CommodityCommentDTO> implements CommodityCommentService {

    @Override
    public QueryWrapper<CommodityCommentEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<CommodityCommentEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }

    @Autowired
    private CommodityCommentDao commentDao;

    @Override
    public void saveComment(Long cid, Long fuid, String content, String ip) {
        CommodityCommentEntity entity = new CommodityCommentEntity();
        entity.setCommodityId(cid);
        entity.setMemberIp(ip);
        entity.setCreateTime(new Date());
        entity.setContent(content);
        entity.setFromUid(fuid);
        entity.setReplayCount(0);
        commentDao.insert(entity);
    }

    @Override
    public List<CommodityCommentEntity> selectCommentById(Long id) {
        return commentDao.selectCommentById(id);
    }

    @Override
    public  List<CommodityCommentEntity> selectCommentList(Long id) {
        return commentDao.selectCommentList(id);
    }

}
