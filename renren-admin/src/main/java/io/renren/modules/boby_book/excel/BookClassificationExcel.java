package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 书籍分类
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class BookClassificationExcel {
    @Excel(name = "分类id")
    private Long id;
    @Excel(name = "分类名称")
    private String classificationName;
    @Excel(name = "更新者")
    private Long update;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date creatorDate;

}