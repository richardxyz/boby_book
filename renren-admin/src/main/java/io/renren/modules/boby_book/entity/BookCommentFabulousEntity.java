package io.renren.modules.boby_book.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 图书评论点赞
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("xux_book_comment_fabulous")
public class BookCommentFabulousEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 书籍id
     */
    private Long commentId;
    /**
     * 评论用户IP
     */
    private String userIp;
    /**
     * 用户id
     */
    private Long fromUid;
    /**
     * 更新者
     */
    private Long updater;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateDate;



}