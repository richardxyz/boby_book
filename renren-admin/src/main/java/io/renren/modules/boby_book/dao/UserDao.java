package io.renren.modules.boby_book.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.boby_book.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * 用户基本信息表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@Mapper
public interface UserDao extends BaseDao<UserEntity> {
    /**
     * 根据手机号查询用户id
     *
     * @param phone 手机号
     * @return
     */
    @Select("SELECT id FROM e_user WHERE phone = #{phone}")
    Long selectIdByPhone(@Param("phone") String phone);

    /**
     * 根据手机号查询用户
     *
     * @param phone 手机号
     * @return
     */
    @Select("SELECT * FROM e_user WHERE phone = #{phone}")
    UserEntity selectUserByPhone(@Param("phone") String phone);

    /**
     * 修改昵称
     *
     * @param username 用户名
     * @param id       用户id
     */
    @Update("UPDATE e_user SET username = #{username} WHERE id =  #{id}")
    void changeUserName(@Param("username") String username, @Param("id") Long id);

    /**
     * 修改地区
     *
     * @param regional 地区
     * @param id       用户id
     */
    @Update("UPDATE e_user SET regional = #{regional} WHERE id =  #{id}")
    void changeRegional(@Param("regional") String regional, @Param("id") Long id);

    /**
     * 修改详细地址
     *
     * @param detailedAddress 详细地址
     * @param id              用户id
     */
    @Update("UPDATE e_user SET detailed_address = #{detailedAddress} WHERE id =  #{id}")
    void changedeTailedAddress(@Param("detailedAddress") String detailedAddress, @Param("id") Long id);

    /**
     * 查询当前手机号是否注册
     *
     * @param phone 手机号
     * @return
     */
    @Select("SELECT COUNT(*) FROM e_user WHERE phone = #{phone}")
    Integer selectByPhone(@Param("phone") String phone);

    /**
     * 用户修改语录
     *
     * @param quotations 语录
     * @param userid     用户id
     */
    @Update("UPDATE e_user SET quotations = #{quotations} WHERE id =  #{id}")
    void changeQuotations(@Param("quotations") String quotations, @Param("id") Long userid);

    /**
     * 修改头像
     *
     * @param filePath 头像信息
     * @param id       用户id
     */
    @Update("UPDATE e_user SET head_portrait = #{filePath} WHERE id =  #{id}")
    void changeHeadPortrait(@Param("filePath") String filePath, @Param("id") Long id);

    /**
     * 修改性别
     *
     * @param gender 性别
     * @param userid 用户id
     */
    @Update("UPDATE e_user SET gender = #{gender} WHERE id =  #{id}")
    void changedeGender(@Param("gender") String gender, @Param("id") Long userid);

    /**
     * 修改签到状态
     *
     * @param signInState 签到状态
     * @param id          用户id
     */
    @Update("UPDATE e_user SET sign_in = #{sign_in} WHERE id =  #{id}")
    void updateSignInState(@Param("sign_in") String signInState, @Param("id") Long id);

    /**
     * 修改签到天数
     *
     * @param num 天数
     * @param id  用户id
     */
    @Update("UPDATE e_user SET sign_in_num = #{sign_in_num} WHERE id =  #{id}")
    void updateSignInNum(@Param("sign_in_num") Integer num, @Param("id") Long id);

    /**
     * 清空签到状态
     */
    @Update("UPDATE e_user SET sign_in = '0,0,0,0,0,0,0'")
    void updateSignIn();

    /**
     * 微信登录
     *
     * @param openid 微信用户唯一标识
     * @return
     */
    @Select("SELECT COUNT(*) FROM e_user WHERE open_id =  #{open_id}")
    Integer wxLogin(@Param("open_id") String openid);

    /**
     * 根据openid查询用户
     *
     * @param openid 微信用户唯一标识
     * @return
     */
    @Select("SELECT * FROM e_user WHERE open_id =  #{open_id}")
    UserEntity selectUserByOpenid(@Param("open_id")String openid);

    /**
     * 修改密码
     *
     * @param newPassword 新密码
     * @param userid      用户id
     */
    @Update("UPDATE e_user SET password = #{password} WHERE id =  #{id}")
    void changedePwd(@Param("password") String newPassword, @Param("id") Long userid);

    /**
     * 查询当前用户是否关注
     *
     * @param fuId 用户id
     * @param tuId 所关注用户id
     * @return
     */
    @Select("SELECT COUNT(*) FROM e_user_follow WHERE user_id = #{user_id} AND follow_user_id = #{follow_user_id}")
    Integer selectFollowStyle(@Param("user_id") Long fuId, @Param("follow_user_id") Long tuId);

    /**
     * 绑定手机号
     *
     * @param phone 手机号
     * @param userid 用户id
     */
    @Update("UPDATE e_user SET phone = #{phone} WHERE id =  #{id}")
    void bindPhone(@Param("phone")String phone, @Param("id") Long userid);

    /**
     * 判断手机号是否已绑定注册
     *
     * @param phone 手机号
     * @return
     */
    @Select("SELECT COUNT(*) FROM e_user WHERE phone = #{phone}")
    Integer judgePhineExist(@Param("phone")String phone);
}