package io.renren.modules.boby_book.controller;

import io.renren.common.exception.ErrorCode;
import io.renren.common.exception.RenException;
import io.renren.common.redis.RedisUtils;
import io.renren.common.utils.Result;
import io.renren.modules.boby_book.entity.UserEntity;
import io.renren.modules.boby_book.service.UserService;
import io.renren.modules.boby_book.service.UserSpecificInformationService;
import io.renren.modules.security.password.PasswordUtils;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static io.renren.common.redis.RedisUtils.Minutes_Five_EXPIRE;


/**
 * 用户登录/注册操作
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
@RestController
@RequestMapping("boby_book/login")
@Api(tags = "用户登录/注册操作")
public class UserLoginController {

    @Autowired
    private UserService userService;

//    @Autowired
//    private StringRedisTemplate redisTemplate;

    @Autowired
    private UserSpecificInformationService specificInformationService;

    @Autowired
    private RedisUtils redisUtils;


    /**
     * 用户注册时发送短信验证码
     *
     * @param phone 手机号
     * @return
     */
    @PostMapping("/sendSms")
    public Result sendSms(String phone) {
        try {
            System.err.println("手机号：" + phone);
            //1.生成短信验证码
            String vcode = RandomStringUtils.randomNumeric(6);
            System.err.println("验证码：" + vcode);
            //1.2.把生成的短信验证码存放到redis中
//            redisTemplate.opsForValue().set(phone, vcode, 1, TimeUnit.HOURS);
            redisUtils.set(phone, vcode, Minutes_Five_EXPIRE);
            //2.发送短信
            Map<String, Object> map = new HashMap<>();
            map.put("message", "短信发送成功");
            map.put("vcode", vcode);
            return new Result().ok(map);

//            2.发送短信
//            SendSmsResponse smsResponse = SmsUtil.sendSms(phone, vcode);
//            System.out.println("++++:" + smsResponse.getCode());
//            if ("OK".equalsIgnoreCase(smsResponse.getCode())) {
//                System.out.println("----------------");
//                System.out.println("发送成功");
//                HashMap<String, Object> map = new HashMap<>();
//                map.put("message", "短信发送成功");
//                map.put("vcode", vcode);
//                return new Result().ok(map);
//            } else {
//                System.out.println("----------------");
//                System.out.println(smsResponse.getMessage());
//                HashMap<String, Object> map = new HashMap<>();
//                map.put("message", smsResponse.getMessage());
//                map.put("vcode", 200);
//                return new Result().ok(map);
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Result().error(500, "短信发送失败");
        }

// } catch (ClientException e) {
//     e.printStackTrace();
//     return new Result().error(500, "短信发送失败");
// }
    }

    /***
     * 用户的注册
     * @param phone 手机号
     * @param vcode 验证码
     * @param password 密码
     * @return
     */
    @PostMapping("/regist")
    public Result<Object> regist(String phone, String vcode, String password) {
        Integer count = userService.judgePhineExist(phone);
        Map<String, Object> map = new HashMap<>();
        if (count >= 1) {
            map.put("code", 606);
            map.put("msg", "此手机号已被注册过");
            return new Result().ok(map);
        } else {
            System.out.println("*****开始注册*****");
            //.获得前台传递过来的验证码
            String verificationCode = vcode;
            System.out.println("前台传递过来的验证码:" + verificationCode);
            //.获得Redis数据库中存储的数据
//        String redisVerificationCode = redisTemplate.opsForValue().get(phone);
            String redisVerificationCode = (String) redisUtils.get(phone);
            System.out.println("Redis数据库中存储的验证码:" + redisVerificationCode);
            if (verificationCode.equalsIgnoreCase(redisVerificationCode)) {
                userService.Register(phone, password);
                UserEntity entity = userService.selectUserByPhone(phone);
                specificInformationService.Register(entity.getId());
                map.put("code", 0);
                map.put("msg", "注册成功");
                return new Result().ok(map);
            } else {
                map.put("code", 500);
                map.put("msg", "注册失败");
                return new Result().ok(map);
            }
        }
    }


    /**
     * 用户验证码登录
     *
     * @param phone 手机号
     * @param vcode 验证码
     * @return
     */
    @PostMapping("/vLogin")
    public Result vLogin(String phone, String vcode) {
        System.err.print("手机号：" + phone);
        Integer count = userService.selectByPhone(phone);
        if (count <= 0) {
            return new Result().error(500, "当前手机号未注册");
        } else {
            String verificationCode = vcode;
            System.out.println("前台传递过来的验证码:" + verificationCode);
            //.获得Redis数据库中存储的数据
            String redisVerificationCode = (String) redisUtils.get(phone);
            System.out.println("Redis数据库中存储的验证码:" + redisVerificationCode);
            if (verificationCode.equalsIgnoreCase(redisVerificationCode)) {
                UserEntity entity = userService.selectUserByPhone(phone);
                return new Result().ok(entity);
            } else {
                return new Result().error(500, "登录失败");
            }
        }

    }

    /**
     * 用户密码登录
     *
     * @param phone    手机号
     * @param password 密码
     * @return
     */
    @PostMapping("/mLogin")
    public Result mLogin(String phone, String password) {
        System.err.print("手机号：" + phone);
        System.err.println("密码：" + password);
        Long id = userService.selectIdByPhone(phone);
        UserEntity entity = userService.selectById(id);
        //用户不存在
        if (entity == null) {
            throw new RenException(ErrorCode.ACCOUNT_PASSWORD_ERROR);
        }
        //密码错误
        if (!PasswordUtils.matches(password, entity.getPassword())) {
            throw new RenException(ErrorCode.ACCOUNT_PASSWORD_ERROR);
        }
//        return sysUserTokenService.createToken(id);
        return new Result().ok(entity);
    }

}