package io.renren.modules.boby_book.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * sku平台属性值关联表(用户查看商品详细信息数据)
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-12-13
 */
@Data
public class CommoditySkuAttrValueExcel {
    @Excel(name = "编号")
    private Long id;
    @Excel(name = "属性id（冗余)")
    private Long attrId;
    @Excel(name = "属性值id")
    private Long valueId;
    @Excel(name = "skuid")
    private Long skuId;
    @Excel(name = "销售属性名称")
    private String attrName;
    @Excel(name = "销售属性值名称")
    private String attrValueName;
    @Excel(name = "更新者")
    private Long updater;
    @Excel(name = "更新时间")
    private Date updateDate;
    @Excel(name = "创建者")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;

}