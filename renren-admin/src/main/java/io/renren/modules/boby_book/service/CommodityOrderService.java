package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommodityOrderDTO;
import io.renren.modules.boby_book.entity.CommodityOrderEntity;

import java.util.List;

/**
 * 订单
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-10-30
 */
public interface CommodityOrderService extends CrudService<CommodityOrderEntity, CommodityOrderDTO> {
    /**
     * 生成交易码
     *
     * @param userid 用户id
     * @return
     */
    String genTradeCode(Long userid);

    /**
     * 检查交易码
     *
     * @param userid    用户id
     * @param tradeCode 交易码
     * @return
     */
    String checkTradeCode(Long userid, String tradeCode);

    /**
     * 保存订单消息
     *
     * @param order 订单id
     */
    void saveOrder(CommodityOrderEntity order);

    /**
     * 查询登录用户所有订单
     *
     * @param userId 用户id
     * @return
     */
    List<CommodityOrderEntity> findAllOrder(Long userId);

    /**
     * 取消/删除订单
     *
     * @param orderId 订单id
     */
    void cancelOrder(Long orderId);
}