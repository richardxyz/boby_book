package io.renren.modules.boby_book.service;


import io.renren.common.service.CrudService;
import io.renren.modules.boby_book.dto.CommodityCommentDTO;
import io.renren.modules.boby_book.entity.CommodityCommentEntity;

import java.util.List;

/**
 * 商品评价
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2019-11-11
 */
public interface CommodityCommentService extends CrudService<CommodityCommentEntity, CommodityCommentDTO> {
    /**
     * 商品添加评论
     *
     * @param cid     商品id
     * @param fuid    评论用户id
     * @param content 评论内容
     * @param ip      评论用户ip
     */
    void saveComment(Long cid, Long fuid, String content, String ip);

    /**
     * 根据id查询商品评价信息
     *
     * @param id 商品id
     * @return
     */
    List<CommodityCommentEntity> selectCommentById(Long id);

    /**
     * 查询最新商品评价信息
     *
     * @param id 商品id
     * @return
     */
    List<CommodityCommentEntity> selectCommentList(Long id);
}