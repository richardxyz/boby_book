package io.renren.modules.boby_book.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.constant.SystemConstant;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.RandomName;
import io.renren.common.utils.Result;
import io.renren.modules.boby_book.dao.UserDao;
import io.renren.modules.boby_book.dao.UserFansDao;
import io.renren.modules.boby_book.dao.UserFollowDao;
import io.renren.modules.boby_book.dao.UserSpecificInformationDao;
import io.renren.modules.boby_book.dto.UserDTO;
import io.renren.modules.boby_book.entity.UserEntity;
import io.renren.modules.boby_book.entity.UserFansEntity;
import io.renren.modules.boby_book.entity.UserFollowEntity;
import io.renren.modules.boby_book.entity.UserSpecificInformationEntity;
import io.renren.modules.boby_book.service.UserService;
import io.renren.modules.security.password.PasswordUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Date;
import java.util.Map;

/**
 * 用户基本信息表
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 019-10-30
 */
@Service
@Transactional
public class UserServiceImpl extends CrudServiceImpl<UserDao, UserEntity, UserDTO> implements UserService {

    @Autowired
    private UserDao eUserDao;

    @Override
    public QueryWrapper<UserEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<UserEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    /**
     * 注册用户
     *
     * @param phone
     * @param password
     */
    @Override
    public void Register(String phone, String password) {
        //密码加密
        String pwd = PasswordUtils.encode(password);

        //随机昵称
        String randomJianHan = RandomName.getRandomJianHan(5);
        UserEntity entity = new UserEntity();
        entity.setPhone(phone);
        entity.setPassword(password);
        entity.setUsername(randomJianHan);
        entity.setGender("男");
        entity.setDetailedAddress("");
        entity.setPassword(pwd);
        entity.setQuotations("这人很懒，啥也没写。。。");
        eUserDao.insert(entity);
    }

    @Override
    public Long selectIdByPhone(String phone) {
        return eUserDao.selectIdByPhone(phone);
    }

    @Override
    public UserEntity selectUserByPhone(String phone) {
        return eUserDao.selectUserByPhone(phone);
    }

    @Override
    public void changeUserName(String username, Long id) {
        eUserDao.changeUserName(username, id);
    }

    @Override
    public void changeRegional(String regional, Long id) {
        eUserDao.changeRegional(regional, id);

    }

    @Override
    public void changedeTailedAddress(String detailedAddress, Long id) {
        eUserDao.changedeTailedAddress(detailedAddress, id);
    }

    @Override
    public Integer selectByPhone(String phone) {
        return eUserDao.selectByPhone(phone);
    }

    @Override
    public void changedeGender(String gender, Long userid) {
        eUserDao.changedeGender(gender, userid);
    }

    @Override
    public void changeQuotations(String quotations, Long userid) {
        eUserDao.changeQuotations(quotations, userid);
    }

    @Override
    public void changedePwd(String newPassword, Long userid) {
        newPassword = PasswordUtils.encode(newPassword);
        eUserDao.changedePwd(newPassword, userid);
    }

    @Override
    public void bindPhone(String phone, Long userid) {
        eUserDao.bindPhone(phone, userid);
    }

    @Override
    public Integer judgePhineExist(String phone) {
        return eUserDao.judgePhineExist(phone);
    }


    @Autowired
    private UserFollowDao followDao;
    @Autowired
    private UserFansDao fansDao;
    @Autowired
    private UserSpecificInformationDao userSpecificInformationDao;

    @Override
    public Integer selectFollowStyle(Long fuId, Long tuId) {
        return eUserDao.selectFollowStyle(fuId, tuId);
    }

    @Override
    public void followUser(Long fuId, Long tuId) {
        UserFollowEntity followEntity = new UserFollowEntity();
        followEntity.setUserId(fuId);
        followEntity.setFollowTime(new Date());
        followEntity.setFollowUserId(tuId);
        followDao.insert(followEntity);
        //用户转为关注用户粉丝
        UserFansEntity fansEntity = new UserFansEntity();
        fansEntity.setUserId(tuId);//用户id
        fansEntity.setFansFollowTime(new Date());
        fansEntity.setFansUserId(fuId);//粉丝id
        fansDao.insert(fansEntity);
        //添加用户关注数量
        UserSpecificInformationEntity specificInformationEntity = userSpecificInformationDao.selectUserSpecificInformationById(fuId);
        Integer followNum = specificInformationEntity.getFollowNum();
        followNum++;
        specificInformationEntity.setFollowNum(followNum);
        userSpecificInformationDao.updateById(specificInformationEntity);
        //添加关注用户粉丝数量
        UserSpecificInformationEntity specificInformationEntity2 = userSpecificInformationDao.selectUserSpecificInformationById(tuId);
        Integer fansNum = specificInformationEntity2.getFansNum();
        fansNum++;
        specificInformationEntity2.setFansNum(fansNum);
        userSpecificInformationDao.updateById(specificInformationEntity2);
    }

    @Override
    public void cancelFollowUser(Long fuId, Long tuId) {
        followDao.cancelFollowUser(fuId, tuId);//删除关注数据
        fansDao.cancelFollowUser(fuId, tuId);//删除粉丝数据
        //减少用户关注数量
        UserSpecificInformationEntity specificInformationEntity = userSpecificInformationDao.selectUserSpecificInformationById(fuId);
        Integer followNum = specificInformationEntity.getFollowNum();
        followNum--;
        specificInformationEntity.setFollowNum(followNum);
        userSpecificInformationDao.updateById(specificInformationEntity);
        //减少关注用户粉丝数量
        UserSpecificInformationEntity specificInformationEntity2 = userSpecificInformationDao.selectUserSpecificInformationById(tuId);
        Integer fansNum = specificInformationEntity2.getFansNum();
        fansNum--;
        specificInformationEntity2.setFansNum(fansNum);
        userSpecificInformationDao.updateById(specificInformationEntity2);
    }

    @Override
    public void changeHeadPortrait(String filePath, Long id) {
        eUserDao.changeHeadPortrait(filePath, id);
    }

    @Override
    public void updateSignInState(String signInState, Long id) {
        eUserDao.updateSignInState(signInState, id);
    }

    @Override
    public void updateSignInNum(Integer num, Long id) {
        eUserDao.updateSignInNum(num, id);
    }

    @Override
    public void updateSignIn() {
        eUserDao.updateSignIn();
    }

    @Override
    public Integer wxLogin(String openid) {
        return eUserDao.wxLogin(openid);
    }

    @Override
    public void wxEmpower(String openid, String avatarUrl, String nickName, String province) {

        UserEntity entity = new UserEntity();

        entity.setOpenId(openid);
        entity.setUsername(nickName);
        entity.setHeadPortrait(avatarUrl);
        entity.setGender("男");
        entity.setRegional(province);
        entity.setDetailedAddress("");
        entity.setQuotations("这人很懒，啥也没写。。。");
        eUserDao.insert(entity);
    }

    @Override
    public UserEntity selectUserByOpenid(String openid) {
        return eUserDao.selectUserByOpenid(openid);
    }


    @Override
    public Result updUserProfile(MultipartFile newProfile, Long userid) {
        // 根据Windows和Linux配置不同的头像保存路径
//        String OSName = System.getProperty("os.name");
//        String profilesPath = OSName.toLowerCase().startsWith("win") ? SystemConstant.WINDOWS_PROFILES_PATH
//                : SystemConstant.LINUX_PROFILES_PATH;
        String profilesPathDB = SystemConstant.WINDOWS_PROFILES_PATH_DB;//数据库所需路径
        String profilesPath = SystemConstant.WINDOWS_PROFILES_PATH + SystemConstant.WINDOWS_PROFILES_PATH_DB;//保存图片所需路径
        if (!newProfile.isEmpty()) {
            // 当前用户
            UserEntity currentUser = eUserDao.selectById(userid);
            //用户头像
            String profilePathAndNameDB = currentUser.getHeadPortrait();
            // 默认以原来的头像名称为新头像的名称，这样可以直接替换掉文件夹中对应的旧头像
            String newProfileNameDB = null;//数据库头像路径
            String newProfileName = profilePathAndNameDB;//本地保存头像路径
            // 若头像名称不存在
            if (profilePathAndNameDB == null || "".equals(profilePathAndNameDB)) {
                newProfileNameDB = profilesPathDB + System.currentTimeMillis() + newProfile.getOriginalFilename();
                // 路径存库
                currentUser.setHeadPortrait(newProfileNameDB);
                eUserDao.updateById(currentUser);
            }
            // 磁盘保存
            BufferedOutputStream out = null;
            try {
                File folder = new File(profilesPath);
                if (!folder.exists())
                    folder.mkdirs();
                try {
                    out = new BufferedOutputStream(new FileOutputStream(SystemConstant.WINDOWS_PROFILES_PATH + newProfileName));
                } catch (Exception e) {
//                    e.printStackTrace();
                    newProfileNameDB = profilesPathDB + System.currentTimeMillis() + newProfile.getOriginalFilename();//数据库头像路径
                    newProfileName = profilesPath + System.currentTimeMillis() + newProfile.getOriginalFilename();//本地保存头像路径
                    currentUser.setHeadPortrait(newProfileNameDB);
                    eUserDao.updateById(currentUser);
                    out = new BufferedOutputStream(new FileOutputStream(newProfileName));
                }
                // 写入新文件
                out.write(newProfile.getBytes());
                out.flush();
            } catch (Exception e) {
                e.printStackTrace();
                return new Result().error(606, "设置头像失败");
            } finally {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return new Result().ok("设置头像成功");
        } else {
            return new Result().error(606, "设置头像失败");
        }

    }
}