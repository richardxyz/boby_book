package io.renren.common.constant;

public interface SystemConstant {

    /**
     * 头像保存路径
     */
    //数据库保存地址
    String WINDOWS_PROFILES_PATH_DB = "/image/headPortrait/";
    //本地保存地址
    String WINDOWS_PROFILES_PATH = "D:/environment/wx_pro/boby_book";
    //服务器保存地址
    String LINUX_PROFILES_PATH = "";
}
