package io.renren.common.constant;

/**
 * 微信所需相关数据
 */
public interface WeiXinData {
    /**
     * app_id 小程序ID
     */
    String APP_ID = "wxea52ca39c47ac722";

    /**
     * app_secret 小程序密钥
     */
    String APP_SECRET = "68593e0ee972fe48ed83b2e8e833b525";

    /**
     * grant_type 登录凭证校验，授权类型，此处只需填写 authorization_code
     */
    String AUTHORIZATION_CODE = "authorization_code";

    /**
     * grant_type 获取小程序全局唯一后台接口调用凭据，授权类型，此处只需填写 client_credential
     */
    String CLIENT_CREDENTIAL = "client_credential";
}
